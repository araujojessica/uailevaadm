(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-home-login-home-module"],{

/***/ "./node_modules/jwt-decode/build/jwt-decode.esm.js":
/*!*********************************************************!*\
  !*** ./node_modules/jwt-decode/build/jwt-decode.esm.js ***!
  \*********************************************************/
/*! exports provided: default, InvalidTokenError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvalidTokenError", function() { return n; });
function e(e){this.message=e}e.prototype=new Error,e.prototype.name="InvalidCharacterError";var r="undefined"!=typeof window&&window.atob&&window.atob.bind(window)||function(r){var t=String(r).replace(/=+$/,"");if(t.length%4==1)throw new e("'atob' failed: The string to be decoded is not correctly encoded.");for(var n,o,a=0,i=0,c="";o=t.charAt(i++);~o&&(n=a%4?64*n+o:o,a++%4)?c+=String.fromCharCode(255&n>>(-2*a&6)):0)o="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(o);return c};function t(e){var t=e.replace(/-/g,"+").replace(/_/g,"/");switch(t.length%4){case 0:break;case 2:t+="==";break;case 3:t+="=";break;default:throw"Illegal base64url string!"}try{return function(e){return decodeURIComponent(r(e).replace(/(.)/g,(function(e,r){var t=r.charCodeAt(0).toString(16).toUpperCase();return t.length<2&&(t="0"+t),"%"+t})))}(t)}catch(e){return r(t)}}function n(e){this.message=e}function o(e,r){if("string"!=typeof e)throw new n("Invalid token specified");var o=!0===(r=r||{}).header?0:1;try{return JSON.parse(t(e.split(".")[o]))}catch(e){throw new n("Invalid token specified: "+e.message)}}n.prototype=new Error,n.prototype.name="InvalidTokenError";/* harmony default export */ __webpack_exports__["default"] = (o);
//# sourceMappingURL=jwt-decode.esm.js.map


/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/login-home/login-home.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login-home/login-home.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <!--<ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"login-adm\" text=\"\"></ion-back-button>\r\n    </ion-buttons>-->\r\n    <ion-buttons slot=\"end\" size=\"x-small\">\r\n      <ion-button color=\"light\" (click) = \"logout();\" tappable>SAIR&nbsp;<ion-icon name=\"log-out-outline\"></ion-icon></ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <br>\r\n  <ion-title>Olá, <b>{{nome}}</b></ion-title><br>\r\n  <br>\r\n  <ion-grid>\r\n    <ion-row class=\"ex\">\r\n      <b>Acesso Motorista</b>\r\n      <br>\r\n      <ion-row>\r\n        <ion-col class=\"gridCol1\" (click)=\"listaMotoristas()\" tappable>LIBERAÇÃO<br><ion-icon name=\"lock-open-outline\"></ion-icon></ion-col>\r\n        <ion-col class=\"gridCol1\" (click)=\"motorista()\" tappable>MOTORISTAS<br><ion-icon name=\"people-outline\"></ion-icon></ion-col>\r\n        <!--<ion-col class=\"gridCol1\" (click)=\"cadastro()\" tappable>CADASTRO<br><ion-icon name=\"clipboard-outline\"></ion-icon></ion-col>-->\r\n        <ion-col class=\"gridCol1\" (click)=\"bloqueados()\" tappable>BLOQUEADO<br><ion-icon name=\"lock-closed-outline\"></ion-icon></ion-col>\r\n      </ion-row>\r\n    </ion-row>\r\n  </ion-grid>\r\n  <br>\r\n\r\n  <ion-grid>\r\n    <ion-row class=\"ex\">\r\n      <b>Acesso Cliente</b>\r\n        <br>\r\n        <ion-row>\r\n          <ion-col class=\"gridCol1 col-md-12\" (click)=\"bloqueioCliente()\" tappable>BLOQUEIO<br><ion-icon name=\"lock-closed-outline\"></ion-icon></ion-col>\r\n         <!-- <ion-col class=\"gridCol1 col-md-12\" (click)=\"editarCliente()\" tappable>EDITAR<br><ion-icon name=\"create-outline\"></ion-icon></ion-col>-->\r\n        </ion-row>\r\n    </ion-row>\r\n  </ion-grid>\r\n  <br>\r\n  <ng-container *ngIf=\"adm === 'ADM' || adm === 'TRUE'\">\r\n  <ion-grid>\r\n    <ion-row class=\"ex\">\r\n      <b>Viagens</b>\r\n      <br>\r\n      <ion-row>\r\n        <ion-col class=\"gridCol1 col-md-12\" (click)=\"viagens()\"><u>ABERTAS</u><br><ion-icon name=\"speedometer-outline\"></ion-icon></ion-col>\r\n      </ion-row>\r\n    </ion-row>\r\n  </ion-grid>\r\n  <br>\r\n\r\n  \r\n    <ion-grid fixed=\"true\" mode=\"md\">\r\n      <br>\r\n        <ion-row>\r\n          <ion-col class=\"gridCol2 col-md-12\" (click)=\"carteira()\" tappable><u>CARTEIRA</u><br><ion-icon name=\"wallet-outline\"></ion-icon></ion-col>\r\n          <ion-col class=\"gridCol2 col-md-12\" (click)=\"corridas()\" tappable><u>CORRIDAS</u><br><ion-icon name=\"car-outline\"></ion-icon></ion-col>\r\n          <ion-col class=\"gridCol2 col-md-12\" style=\"opacity: 0.5;\"><u>BANCO</u><br><ion-icon name=\"layers-outline\"></ion-icon></ion-col>\r\n          <ion-col class=\"gridCol2 col-md-12\" style=\"opacity: 0.5;\" ><u>GERAR BOLETO</u><br><ion-icon name=\"reader-outline\"></ion-icon></ion-col>\r\n        </ion-row>\r\n    </ion-grid>\r\n  </ng-container>\r\n  \r\n  <br>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/login-home/login-home-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/login-home/login-home-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: LoginHomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginHomePageRoutingModule", function() { return LoginHomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _login_home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login-home.page */ "./src/app/login-home/login-home.page.ts");




const routes = [
    {
        path: '',
        component: _login_home_page__WEBPACK_IMPORTED_MODULE_3__["LoginHomePage"]
    }
];
let LoginHomePageRoutingModule = class LoginHomePageRoutingModule {
};
LoginHomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], LoginHomePageRoutingModule);



/***/ }),

/***/ "./src/app/login-home/login-home.module.ts":
/*!*************************************************!*\
  !*** ./src/app/login-home/login-home.module.ts ***!
  \*************************************************/
/*! exports provided: LoginHomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginHomePageModule", function() { return LoginHomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _login_home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login-home-routing.module */ "./src/app/login-home/login-home-routing.module.ts");
/* harmony import */ var _login_home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login-home.page */ "./src/app/login-home/login-home.page.ts");







let LoginHomePageModule = class LoginHomePageModule {
};
LoginHomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _login_home_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginHomePageRoutingModule"]
        ],
        declarations: [_login_home_page__WEBPACK_IMPORTED_MODULE_6__["LoginHomePage"]]
    })
], LoginHomePageModule);



/***/ }),

/***/ "./src/app/login-home/login-home.page.scss":
/*!*************************************************!*\
  !*** ./src/app/login-home/login-home.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-card, img {\n  border-radius: 5%;\n  border: #001c4e 0.5px solid;\n}\n\nion-toolbar {\n  --background: #001c4e;\n}\n\nion-card-title, ion-card-subtitle, ion-card-content {\n  text-align: center;\n}\n\n/*ion-content {\n    //--background: url(../../assets/img/bg.jpg);\n    //--background-size:var(--background-size);\n    --background: aliceblue;\n}*/\n\nion-grid {\n  margin: auto;\n  display: block;\n  width: 90%;\n  border: 1px solid;\n  border-left: none;\n  border-right: none;\n}\n\nion-title {\n  text-align: center;\n  color: #001c4e;\n  font-size: 1.7em;\n  display: block;\n}\n\nu {\n  text-decoration: overline;\n}\n\n.ex {\n  text-align: center;\n  margin: auto;\n  display: block;\n  padding: 20px;\n  font-size: 1.5em;\n}\n\n.gridCol1 {\n  text-align: center;\n  padding: 45px;\n  padding-bottom: 5px;\n  font-size: 0.9em;\n  text-decoration: underline;\n}\n\n.gridCol2 {\n  text-align: center;\n  padding: 43px;\n  font-size: 1.5em;\n  text-decoration: underline;\n}\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 20px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n\nbody {\n  background: url('bg.jpg');\n  background-size: cover;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4taG9tZS9sb2dpbi1ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EsMkJBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtBQUNKOztBQUVBOzs7O0VBQUE7O0FBTUE7RUFDSSxZQUFBO0VBQ0EsY0FBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFBSjs7QUFHQTtFQUNJLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQUFKOztBQUdBO0VBQ0kseUJBQUE7QUFBSjs7QUFHQTtFQUVJLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7QUFESjs7QUFNQTtFQUNJLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtBQUhKOztBQVFBO0VBRUksa0JBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtBQU5KOztBQVlBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQVRKOztBQWFBO0VBQ0kseUJBQUE7RUFDQSxzQkFBQTtBQVZKIiwiZmlsZSI6InNyYy9hcHAvbG9naW4taG9tZS9sb2dpbi1ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkLCBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNSU7XHJcbiAgICBib3JkZXI6ICMwMDFjNGUgMC41cHggc29saWQ7XHJcbn1cclxuXHJcbmlvbi10b29sYmFyIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzAwMWM0ZTtcclxufVxyXG5cclxuaW9uLWNhcmQtdGl0bGUsIGlvbi1jYXJkLXN1YnRpdGxlLCBpb24tY2FyZC1jb250ZW50IHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLyppb24tY29udGVudCB7XHJcbiAgICAvLy0tYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9pbWcvYmcuanBnKTtcclxuICAgIC8vLS1iYWNrZ3JvdW5kLXNpemU6dmFyKC0tYmFja2dyb3VuZC1zaXplKTtcclxuICAgIC0tYmFja2dyb3VuZDogYWxpY2VibHVlO1xyXG59Ki9cclxuXHJcbmlvbi1ncmlkIHtcclxuICAgIG1hcmdpbjogYXV0bzsgXHJcbiAgICBkaXNwbGF5OiBibG9jazsgXHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICBib3JkZXItbGVmdDogbm9uZTtcclxuICAgIGJvcmRlci1yaWdodDogbm9uZTtcclxufVxyXG5cclxuaW9uLXRpdGxlIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjsgXHJcbiAgICBjb2xvcjogIzAwMWM0ZTtcclxuICAgIGZvbnQtc2l6ZTogMS43ZW07XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxufVxyXG5cclxudSB7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG92ZXJsaW5lO1xyXG59XHJcblxyXG4uZXgge1xyXG4gICAvLyBib3JkZXI6IDFweCBzb2xpZDsgXHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW46IGF1dG87IFxyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBwYWRkaW5nOiAyMHB4OyBcclxuICAgIGZvbnQtc2l6ZTogMS41ZW07XHJcbiAgICAvL2JvcmRlci1sZWZ0OiBub25lO1xyXG4gICAgLy9ib3JkZXItcmlnaHQ6IG5vbmU7XHJcbn1cclxuXHJcbi5ncmlkQ29sMSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBwYWRkaW5nOiA0NXB4O1xyXG4gICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgIGZvbnQtc2l6ZTogLjllbTtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xyXG4gICAgLy9ib3JkZXItbGVmdDogbm9uZTtcclxuICAgIC8vYm9yZGVyLXJpZ2h0OiBub25lO1xyXG59XHJcblxyXG4uZ3JpZENvbDIge1xyXG4gICAgLy9ib3JkZXI6IDFweCBzb2xpZDsgXHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7IFxyXG4gICAgcGFkZGluZzogNDNweDsgXHJcbiAgICBmb250LXNpemU6IDEuNWVtO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XHJcbiAgICAvL2ZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAvLyBib3JkZXItbGVmdDogbm9uZTtcclxuICAgIC8vYm9yZGVyLXJpZ2h0OiBub25lO1xyXG59XHJcblxyXG5oMSB7XHJcbiAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBtYXJnaW46IDIwcHggMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbiAgICB9XHJcblxyXG4gICAgXHJcbmJvZHkge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9pbWcvYmcuanBnKTtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/login-home/login-home.page.ts":
/*!***********************************************!*\
  !*** ./src/app/login-home/login-home.page.ts ***!
  \***********************************************/
/*! exports provided: LoginHomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginHomePage", function() { return LoginHomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/core/user/user.service */ "./src/core/user/user.service.ts");
/* harmony import */ var src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/servico/motorista_liberacao */ "./src/servico/motorista_liberacao.ts");






let LoginHomePage = class LoginHomePage {
    constructor(router, motoristaService, mortoristaLiberacaoService, userService, menuCtrl) {
        this.router = router;
        this.motoristaService = motoristaService;
        this.mortoristaLiberacaoService = mortoristaLiberacaoService;
        this.userService = userService;
        this.menuCtrl = menuCtrl;
        this.user$ = motoristaService.getUser();
        this.user$.subscribe(usuario => {
            this.usuarioLogado = usuario;
            this.user_id = this.usuarioLogado.id;
            var a = this.usuarioLogado.full_name.split(' ');
            if (this.usuarioLogado.apelido == null) {
                this.apelido = '';
            }
            else {
                this.apelido = '(' + this.usuarioLogado.apelido + ')';
            }
            this.nome = a[0] + ' ' + this.apelido;
            this.adm = this.usuarioLogado.adm;
        });
        this.mortoristaLiberacaoService.liberar().subscribe(motoristaLiberado => {
            this.m = motoristaLiberado;
        });
    }
    ngOnInit() {
    }
    liberacao() {
        this.router.navigate(['/liberacao']);
    }
    motorista() {
        this.router.navigate(['/bloqueio']);
    }
    cadastro() {
        this.router.navigate(['/cadastro']);
    }
    bloqueados() {
        this.router.navigate(['motorista-bloqueado']);
    }
    carteira() {
        this.router.navigate(['/carteira']);
    }
    banco() {
        this.router.navigate(['/banco']);
    }
    gerarBoleto() {
        this.router.navigate(['/boleto']);
    }
    bloqueioCliente() {
        this.router.navigate(['/bloqueio-cliente']);
    }
    editarCliente() {
        this.router.navigate(['/editar-passageiro']);
    }
    corridas() {
        this.router.navigate(['/corridas']);
    }
    viagens() {
        this.router.navigate(['/viagens']);
    }
    listaMotoristas() {
        this.router.navigate(['/lista-motoristas']);
    }
    logout() {
        this.userService.logout();
        localStorage.removeItem('currentUser');
        this.router.navigate(['']);
        this.menuCtrl.close();
        navigator['app'].exitApp();
    }
};
LoginHomePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: src_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_5__["MotoristaLiberacaoService"] },
    { type: src_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] }
];
LoginHomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./login-home.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/login-home/login-home.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./login-home.page.scss */ "./src/app/login-home/login-home.page.scss")).default]
    })
], LoginHomePage);



/***/ }),

/***/ "./src/core/token/token.service.ts":
/*!*****************************************!*\
  !*** ./src/core/token/token.service.ts ***!
  \*****************************************/
/*! exports provided: TokenService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenService", function() { return TokenService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


const KEY = 'authToken';
let TokenService = class TokenService {
    hasToken() {
        return !!this.getToken();
    }
    setToken(token) {
        window.localStorage.setItem(KEY, token);
    }
    getToken() {
        return window.localStorage.getItem(KEY);
    }
    removeToken() {
        window.localStorage.removeItem(KEY);
    }
};
TokenService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], TokenService);



/***/ }),

/***/ "./src/core/user/user.service.ts":
/*!***************************************!*\
  !*** ./src/core/user/user.service.ts ***!
  \***************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _token_token_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../token/token.service */ "./src/core/token/token.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/build/jwt-decode.esm.js");




//import * as jwt_decode from 'jwt-decode';

let UserService = class UserService {
    constructor(tokenService) {
        this.tokenService = tokenService;
        this.userSubject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](null);
        this.tokenService.hasToken() && this.decodeAndNotify();
    }
    setToken(token) {
        this.tokenService.setToken(token);
        this.decodeAndNotify();
    }
    getUser() {
        return this.userSubject.asObservable();
    }
    decodeAndNotify() {
        const token = this.tokenService.getToken();
        const user = Object(jwt_decode__WEBPACK_IMPORTED_MODULE_4__["default"])(token);
        this.fullName = user.full_name;
        this.userSubject.next(user);
    }
    logout() {
        this.tokenService.removeToken();
        this.userSubject.next(null);
    }
    isLogged() {
        return this.tokenService.hasToken();
    }
    getUserName() {
        return this.fullName;
    }
};
UserService.ctorParameters = () => [
    { type: _token_token_service__WEBPACK_IMPORTED_MODULE_2__["TokenService"] }
];
UserService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], UserService);



/***/ }),

/***/ "./src/servico/motorista_liberacao.ts":
/*!********************************************!*\
  !*** ./src/servico/motorista_liberacao.ts ***!
  \********************************************/
/*! exports provided: MotoristaLiberacaoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MotoristaLiberacaoService", function() { return MotoristaLiberacaoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



const API_URL = "https://uaileva.com.br/api";
let MotoristaLiberacaoService = class MotoristaLiberacaoService {
    constructor(http) {
        this.http = http;
    }
    liberar() {
        return this.http.get(API_URL + `/motorista/liberacao`);
    }
    motoristaUnico(motorista_id) {
        return this.http.get(API_URL + `/motorista/unico/` + motorista_id);
    }
    fotoMotorista(mot_phone) {
        return this.http.get(API_URL + `/photos/motorista/` + mot_phone);
    }
    fotoCrlv(mot_phone) {
        return this.http.get(API_URL + `/photos/motorista/crlv/` + mot_phone);
    }
    fotoCnh(mot_phone) {
        return this.http.get(API_URL + `/photos/motorista/cnh/` + mot_phone);
    }
    carro(id_motorista, placa, modelo) {
        return this.http.get(API_URL + `/carro/signup/` + id_motorista + `/` + placa + `/` + modelo + ``);
    }
    updateNome(user_id, nome, secondName) {
        return this.http.post(`${API_URL}/motorista/updateNome`, { user_id, nome, secondName });
    }
    ;
    liberarMotorista(phone, email) {
        return this.http.get(API_URL + `/motorista/status/` + phone + `/` + email + `/LIBERADO`);
    }
    motoristaLiberado() {
        return this.http.get(API_URL + `/motoristaLiberado`);
    }
    bloqueio(phone, email) {
        return this.http.get(API_URL + `/motorista/status/` + phone + `/` + email + `/BLOQUEADO`);
    }
    bloqueioPassageiro(phone) {
        return this.http.get(API_URL + `/user/bloqueio/` + phone + `/BLOQUEADO`);
    }
    corridaCM(data) {
        console.log(data);
        return this.http.get(API_URL + `/corrida/diariacm/` + data + ``);
    }
    corridaCP(data) {
        return this.http.get(API_URL + `/corrida/diariacp/` + data + ``);
    }
    corridaF(data) {
        return this.http.get(API_URL + `/corrida/diariaf/` + data + ``);
    }
    corridaTotal() {
        return this.http.get(API_URL + `/totalCorridas`);
    }
    motoristaLiberadoUnico(motorista_id) {
        return this.http.get(API_URL + `/motorista/liberadoUnico/` + motorista_id);
    }
    arrecadacao(motorista_id) {
        return this.http.get(API_URL + `/totalarrecadado/` + motorista_id);
    }
    desbloqueio(phone, email) {
        return this.http.get(API_URL + `/motorista/status/` + phone + `/` + email + `/LIBERADO`);
    }
    motbloqueado() {
        return this.http.get(API_URL + `/motorista/bloqueado`);
    }
};
MotoristaLiberacaoService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
MotoristaLiberacaoService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], MotoristaLiberacaoService);



/***/ })

}]);
//# sourceMappingURL=login-home-login-home-module-es2015.js.map
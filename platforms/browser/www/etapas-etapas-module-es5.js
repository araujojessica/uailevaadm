(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["etapas-etapas-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/etapas/etapas.page.html":
    /*!*******************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/etapas/etapas.page.html ***!
      \*******************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppEtapasEtapasPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"cadastro\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <br>\r\n  <ion-title>Etapas Obrigatórias</ion-title>\r\n  <div style=\"text-align: center;\">\r\n    <p class=\"usuario\">Olá, {{nome}}</p>\r\n    <p class=\"comecar\">Veja aqui o que você precisa fazer para criar sua conta.</p>\r\n  </div>\r\n\r\n  <ion-list>\r\n    <ion-item no-padding (click)=\"toAcordos()\">\r\n      <ion-icon name=\"reader-outline\" slot=\"start\"></ion-icon>\r\n      <ion-label>\r\n        Acordos Legais<br>\r\n        <p>Próxima etapa recomendada</p>\r\n      </ion-label>\r\n      <ion-icon name=\"chevron-forward-outline\" slot=\"end\" size=\"small\" style=\"color: #001c4e;\"></ion-icon>\r\n    </ion-item>\r\n\r\n    <ion-item no-padding (click)=\"toFotoPerfil()\">\r\n      <ion-icon name=\"reader-outline\" slot=\"start\"></ion-icon>\r\n      <ion-label>\r\n        Foto de Perfil<br>\r\n        <p>Tudo pronto para começar</p>\r\n      </ion-label>\r\n      <ion-icon name=\"chevron-forward-outline\" slot=\"end\" size=\"small\" style=\"color: #001c4e;\"></ion-icon>\r\n    </ion-item>\r\n\r\n    <ion-item no-padding (click)=\"toCNH()\">\r\n      <ion-icon name=\"reader-outline\" slot=\"start\"></ion-icon>\r\n      <ion-label>\r\n        Carteira Nacional de Habilitação com EAR - CNH <br>\r\n        <p>Tudo pronto para começar</p>\r\n      </ion-label>\r\n      <ion-icon name=\"chevron-forward-outline\" slot=\"end\" size=\"small\" style=\"color: #001c4e;\"></ion-icon>\r\n    </ion-item>\r\n\r\n    <ion-item no-padding (click)=\"toCRLV()\">\r\n      <ion-icon name=\"reader-outline\" slot=\"start\"></ion-icon>\r\n      <ion-label>\r\n        Certificado de Registro e Licenciamento de Veículo - CRLV<br>\r\n        <p>Tudo pronto para começar</p>\r\n      </ion-label>\r\n      <ion-icon name=\"chevron-forward-outline\" slot=\"end\" size=\"small\" style=\"color: #001c4e;\"></ion-icon>\r\n    </ion-item>\r\n  </ion-list>\r\n\r\n  <ion-button class=\"botao\" (click)=\"login()\">\r\n    <ion-icon name=\"checkmark-outline\" slot=\"end\" style=\"color: #001c4e;\"></ion-icon>Etapas Concluídas\r\n  </ion-button>\r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "./src/app/etapas/etapas-routing.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/etapas/etapas-routing.module.ts ***!
      \*************************************************/

    /*! exports provided: EtapasPageRoutingModule */

    /***/
    function srcAppEtapasEtapasRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EtapasPageRoutingModule", function () {
        return EtapasPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _etapas_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./etapas.page */
      "./src/app/etapas/etapas.page.ts");

      var routes = [{
        path: '',
        component: _etapas_page__WEBPACK_IMPORTED_MODULE_3__["EtapasPage"]
      }];

      var EtapasPageRoutingModule = function EtapasPageRoutingModule() {
        _classCallCheck(this, EtapasPageRoutingModule);
      };

      EtapasPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], EtapasPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/etapas/etapas.module.ts":
    /*!*****************************************!*\
      !*** ./src/app/etapas/etapas.module.ts ***!
      \*****************************************/

    /*! exports provided: EtapasPageModule */

    /***/
    function srcAppEtapasEtapasModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EtapasPageModule", function () {
        return EtapasPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _etapas_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./etapas-routing.module */
      "./src/app/etapas/etapas-routing.module.ts");
      /* harmony import */


      var _etapas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./etapas.page */
      "./src/app/etapas/etapas.page.ts");

      var EtapasPageModule = function EtapasPageModule() {
        _classCallCheck(this, EtapasPageModule);
      };

      EtapasPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _etapas_routing_module__WEBPACK_IMPORTED_MODULE_5__["EtapasPageRoutingModule"]],
        declarations: [_etapas_page__WEBPACK_IMPORTED_MODULE_6__["EtapasPage"]]
      })], EtapasPageModule);
      /***/
    },

    /***/
    "./src/app/etapas/etapas.page.scss":
    /*!*****************************************!*\
      !*** ./src/app/etapas/etapas.page.scss ***!
      \*****************************************/

    /*! exports provided: default */

    /***/
    function srcAppEtapasEtapasPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --background: var(--ion-color-light);\n}\n\nion-toolbar {\n  --background: #001c4e !important;\n}\n\nion-header {\n  --background: #001c4e !important;\n}\n\nion-list {\n  background: transparent;\n  padding: 10px;\n  width: 95%;\n  display: block;\n  margin: auto;\n}\n\nion-label {\n  color: #001c4e;\n}\n\nion-title {\n  text-align: center;\n  color: #001c4e;\n  font-size: 1.5em;\n  display: block;\n}\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 15px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n\n.usuario {\n  font-size: 1.4rem;\n  font-weight: bold;\n  padding: 10px;\n  padding-bottom: 0px;\n  color: #123b7d;\n}\n\n.comecar {\n  padding: 10px;\n  padding-top: 0px;\n  color: #123b7d;\n  margin-top: -20px;\n}\n\n.botao {\n  --background: #b1ff49;\n  --color: #001c4e;\n  font-weight: bold;\n  display: block;\n  height: 48px;\n  width: 95%;\n  margin: auto;\n  --border-radius: 50px;\n  --background-activated: #001c4e;\n  --color-activated: white;\n  --background-hover: #001c4e;\n  --color-hover: white;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZXRhcGFzL2V0YXBhcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQ0FBQTtBQUNKOztBQUdBO0VBQ0ksZ0NBQUE7QUFBSjs7QUFHQTtFQUNJLGdDQUFBO0FBQUo7O0FBR0E7RUFDSSx1QkFBQTtFQUNBLGFBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7QUFBSjs7QUFHQTtFQUNJLGNBQUE7QUFBSjs7QUFHQTtFQUNJLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQUFKOztBQUdBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQUFKOztBQUdBO0VBQ0ksaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUFBSjs7QUFHQTtFQUNJLGFBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtBQUFKOztBQUdBO0VBQ0kscUJBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0VBQ0EsK0JBQUE7RUFDQSx3QkFBQTtFQUNBLDJCQUFBO0VBQ0Esb0JBQUE7RUFDQSxxQkFBQTtFQUNBLFVBQUE7QUFBSiIsImZpbGUiOiJzcmMvYXBwL2V0YXBhcy9ldGFwYXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG4gICAgLy8tLWJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1nL2JnLXVhaS1sZXZhMi5wbmdcIikgI2VhZWFlYSBuby1yZXBlYXQgYm90dG9tIDc1cHggbGVmdCA0NXB4O1xyXG59XHJcblxyXG5pb24tdG9vbGJhciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWhlYWRlciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGUgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWxpc3Qge1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgd2lkdGg6IDk1JTtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG59XHJcblxyXG5pb24tbGFiZWwge1xyXG4gICAgY29sb3I6ICMwMDFjNGU7XHJcbn1cclxuXHJcbmlvbi10aXRsZSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7IFxyXG4gICAgY29sb3I6ICMwMDFjNGU7XHJcbiAgICBmb250LXNpemU6IDEuNWVtO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbmgxIHtcclxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIG1hcmdpbjogMTVweCAwO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBjb2xvcjogIzEyM2I3ZDtcclxufVxyXG5cclxuLnVzdWFyaW8ge1xyXG4gICAgZm9udC1zaXplOiAxLjRyZW07XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbn1cclxuXHJcbi5jb21lY2FyIHtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbiAgICBtYXJnaW4tdG9wOiAtMjBweDtcclxufVxyXG5cclxuLmJvdGFvIHtcclxuICAgIC0tYmFja2dyb3VuZDogI2IxZmY0OTsgIC8vIzAwMWM0ZTtcclxuICAgIC0tY29sb3I6ICMwMDFjNGU7IC8vd2hpdGU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgaGVpZ2h0OiA0OHB4O1xyXG4gICAgd2lkdGg6IDk1JTtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIC0tYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6ICMwMDFjNGU7XHJcbiAgICAtLWNvbG9yLWFjdGl2YXRlZDogd2hpdGU7XHJcbiAgICAtLWJhY2tncm91bmQtaG92ZXI6ICMwMDFjNGU7XHJcbiAgICAtLWNvbG9yLWhvdmVyOiB3aGl0ZTtcclxuICAgIC0tYm94LXNoYWRvdzogMCAwIDAgMDsgXHJcbiAgICBvdXRsaW5lOiAwO1xyXG59Il19 */";
      /***/
    },

    /***/
    "./src/app/etapas/etapas.page.ts":
    /*!***************************************!*\
      !*** ./src/app/etapas/etapas.page.ts ***!
      \***************************************/

    /*! exports provided: EtapasPage */

    /***/
    function srcAppEtapasEtapasPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EtapasPage", function () {
        return EtapasPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var EtapasPage = /*#__PURE__*/function () {
        function EtapasPage(router, route) {
          _classCallCheck(this, EtapasPage);

          this.router = router;
          this.route = route;
        }

        _createClass(EtapasPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.route.params.subscribe(function (parametros) {
              _this.nome = parametros['nome'];
              _this.phone = parametros['phone'];
              console.log('parametro dentro de info : ' + parametros['phone']);
            });
          }
        }, {
          key: "toAcordos",
          value: function toAcordos() {
            this.router.navigate(['/acordos']);
          }
        }, {
          key: "toFotoPerfil",
          value: function toFotoPerfil() {
            this.router.navigate(['/foto-perfil']);
          }
        }, {
          key: "toCNH",
          value: function toCNH() {
            this.router.navigate(['/cnh']);
          }
        }, {
          key: "toCRLV",
          value: function toCRLV() {
            this.router.navigate(['/crlv']);
          }
        }, {
          key: "login",
          value: function login() {
            this.router.navigate(['/login-home']);
          }
        }]);

        return EtapasPage;
      }();

      EtapasPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }];
      };

      EtapasPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-etapas',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./etapas.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/etapas/etapas.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./etapas.page.scss */
        "./src/app/etapas/etapas.page.scss"))["default"]]
      })], EtapasPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=etapas-etapas-module-es5.js.map
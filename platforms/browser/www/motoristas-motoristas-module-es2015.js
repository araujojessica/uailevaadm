(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["motoristas-motoristas-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/motoristas/motoristas.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/motoristas/motoristas.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"home\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-card>\r\n    <img src=\"assets/img/grupo2.jpg\" style=\"border-radius: 0%;\"/>\r\n    <ion-card-header>\r\n      <ion-card-subtitle>2020</ion-card-subtitle>\r\n      <ion-card-title>UaiLeva</ion-card-title>\r\n    </ion-card-header>\r\n    <ion-card-content style=\"text-align: center; justify-content: center;\">\r\n      Somos a mais nova cooperativa de Motoristas de Extrema - MG. \r\n      <br>Estamos aqui para melhor lhe atender na sua viagem, com conforto e alegria! \r\n      <br>Estamos chegando em Extrema com o mais moderno aplicativo de mobilidade. \r\n      <br>Aqui está nossa tropa. Queremos te ver sempre! Seja bem vindo ao UaiLeva.\r\n    </ion-card-content>\r\n  </ion-card>\r\n\r\n  <br>\r\n  <h1>Nossos Motoristas:</h1>\r\n\r\n  <ion-grid fixed=\"true\" mode=\"md\">  \r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista1.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n\r\n      <ion-col> \r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista2.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n\r\n      <ion-col> \r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista3.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista4.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista5.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista6.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista7.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <ion-col> \r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista8.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n\r\n      <ion-col> \r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista9.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista10.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista12.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista16.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista17.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista18.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista19.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n      \r\n      <ion-col> \r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista15.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n\r\n      <ion-col> \r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista20.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n      \r\n      <ion-col> \r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista21.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n      \r\n\r\n      <ion-col>\r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista22.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista23.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-card>\r\n          <ion-card-header>\r\n            <img src=\"assets/img/motorista24.jpg\"/>\r\n            <ion-card-subtitle>Desde 2020</ion-card-subtitle>\r\n            <ion-card-title>Motorista</ion-card-title>\r\n          </ion-card-header>\r\n        </ion-card>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/motoristas/motoristas-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/motoristas/motoristas-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: MotoristasPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MotoristasPageRoutingModule", function() { return MotoristasPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _motoristas_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./motoristas.page */ "./src/app/motoristas/motoristas.page.ts");




const routes = [
    {
        path: '',
        component: _motoristas_page__WEBPACK_IMPORTED_MODULE_3__["MotoristasPage"]
    }
];
let MotoristasPageRoutingModule = class MotoristasPageRoutingModule {
};
MotoristasPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MotoristasPageRoutingModule);



/***/ }),

/***/ "./src/app/motoristas/motoristas.module.ts":
/*!*************************************************!*\
  !*** ./src/app/motoristas/motoristas.module.ts ***!
  \*************************************************/
/*! exports provided: MotoristasPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MotoristasPageModule", function() { return MotoristasPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _motoristas_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./motoristas-routing.module */ "./src/app/motoristas/motoristas-routing.module.ts");
/* harmony import */ var _motoristas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./motoristas.page */ "./src/app/motoristas/motoristas.page.ts");







let MotoristasPageModule = class MotoristasPageModule {
};
MotoristasPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _motoristas_routing_module__WEBPACK_IMPORTED_MODULE_5__["MotoristasPageRoutingModule"]
        ],
        declarations: [_motoristas_page__WEBPACK_IMPORTED_MODULE_6__["MotoristasPage"]]
    })
], MotoristasPageModule);



/***/ }),

/***/ "./src/app/motoristas/motoristas.page.scss":
/*!*************************************************!*\
  !*** ./src/app/motoristas/motoristas.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-card, img {\n  border-radius: 5%;\n  border: #001c4e 0.5px solid;\n}\n\nion-toolbar {\n  --background: #001c4e;\n}\n\nion-title {\n  color: #fff;\n}\n\nion-card-title, ion-card-subtitle, ion-card-content {\n  text-align: center;\n}\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 20px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbW90b3Jpc3Rhcy9tb3RvcmlzdGFzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EsMkJBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvbW90b3Jpc3Rhcy9tb3RvcmlzdGFzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkLCBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNSU7XHJcbiAgICBib3JkZXI6ICMwMDFjNGUgMC41cHggc29saWQ7XHJcbn1cclxuXHJcbmlvbi10b29sYmFyIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzAwMWM0ZTtcclxufVxyXG5cclxuaW9uLXRpdGxlIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG59XHJcblxyXG5pb24tY2FyZC10aXRsZSwgaW9uLWNhcmQtc3VidGl0bGUsIGlvbi1jYXJkLWNvbnRlbnQge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG5oMSB7XHJcbiAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBtYXJnaW46IDIwcHggMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbiAgICB9XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/motoristas/motoristas.page.ts":
/*!***********************************************!*\
  !*** ./src/app/motoristas/motoristas.page.ts ***!
  \***********************************************/
/*! exports provided: MotoristasPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MotoristasPage", function() { return MotoristasPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let MotoristasPage = class MotoristasPage {
    constructor() { }
    ngOnInit() {
    }
};
MotoristasPage.ctorParameters = () => [];
MotoristasPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-motoristas',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./motoristas.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/motoristas/motoristas.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./motoristas.page.scss */ "./src/app/motoristas/motoristas.page.scss")).default]
    })
], MotoristasPage);



/***/ })

}]);
//# sourceMappingURL=motoristas-motoristas-module-es2015.js.map
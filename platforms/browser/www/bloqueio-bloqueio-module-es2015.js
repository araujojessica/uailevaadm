(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["bloqueio-bloqueio-module"],{

/***/ "./node_modules/jwt-decode/build/jwt-decode.esm.js":
/*!*********************************************************!*\
  !*** ./node_modules/jwt-decode/build/jwt-decode.esm.js ***!
  \*********************************************************/
/*! exports provided: default, InvalidTokenError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvalidTokenError", function() { return n; });
function e(e){this.message=e}e.prototype=new Error,e.prototype.name="InvalidCharacterError";var r="undefined"!=typeof window&&window.atob&&window.atob.bind(window)||function(r){var t=String(r).replace(/=+$/,"");if(t.length%4==1)throw new e("'atob' failed: The string to be decoded is not correctly encoded.");for(var n,o,a=0,i=0,c="";o=t.charAt(i++);~o&&(n=a%4?64*n+o:o,a++%4)?c+=String.fromCharCode(255&n>>(-2*a&6)):0)o="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(o);return c};function t(e){var t=e.replace(/-/g,"+").replace(/_/g,"/");switch(t.length%4){case 0:break;case 2:t+="==";break;case 3:t+="=";break;default:throw"Illegal base64url string!"}try{return function(e){return decodeURIComponent(r(e).replace(/(.)/g,(function(e,r){var t=r.charCodeAt(0).toString(16).toUpperCase();return t.length<2&&(t="0"+t),"%"+t})))}(t)}catch(e){return r(t)}}function n(e){this.message=e}function o(e,r){if("string"!=typeof e)throw new n("Invalid token specified");var o=!0===(r=r||{}).header?0:1;try{return JSON.parse(t(e.split(".")[o]))}catch(e){throw new n("Invalid token specified: "+e.message)}}n.prototype=new Error,n.prototype.name="InvalidTokenError";/* harmony default export */ __webpack_exports__["default"] = (o);
//# sourceMappingURL=jwt-decode.esm.js.map


/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/bloqueio/bloqueio.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/bloqueio/bloqueio.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"login-home\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <br><br>\r\n  <ion-title>Bloqueio de Motoristas</ion-title><br>\r\n  <ion-grid>\r\n    <ion-list>\r\n      <ion-item-divider mode=\"md\"><ion-label>Motoristas liberados</ion-label></ion-item-divider>\r\n      <ion-item *ngFor= \"let m of m\">\r\n        <ion-label>{{m.full_name}}</ion-label>\r\n        <ion-button class=\"btn-bloquear\" slot=\"end\" (click) = \"bloqueio(m.id)\">BLOQUEAR</ion-button>\r\n        <ion-button class=\"editarButton\" slot=\"end\" (click)=\"editarMotorista(m.id)\">EDITAR</ion-button>\r\n      </ion-item>\r\n    </ion-list>\r\n    <br>\r\n  </ion-grid>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/bloqueio/bloqueio-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/bloqueio/bloqueio-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: BloqueioPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BloqueioPageRoutingModule", function() { return BloqueioPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _bloqueio_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bloqueio.page */ "./src/app/bloqueio/bloqueio.page.ts");




const routes = [
    {
        path: '',
        component: _bloqueio_page__WEBPACK_IMPORTED_MODULE_3__["BloqueioPage"]
    }
];
let BloqueioPageRoutingModule = class BloqueioPageRoutingModule {
};
BloqueioPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], BloqueioPageRoutingModule);



/***/ }),

/***/ "./src/app/bloqueio/bloqueio.module.ts":
/*!*********************************************!*\
  !*** ./src/app/bloqueio/bloqueio.module.ts ***!
  \*********************************************/
/*! exports provided: BloqueioPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BloqueioPageModule", function() { return BloqueioPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _bloqueio_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./bloqueio-routing.module */ "./src/app/bloqueio/bloqueio-routing.module.ts");
/* harmony import */ var _bloqueio_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./bloqueio.page */ "./src/app/bloqueio/bloqueio.page.ts");







let BloqueioPageModule = class BloqueioPageModule {
};
BloqueioPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _bloqueio_routing_module__WEBPACK_IMPORTED_MODULE_5__["BloqueioPageRoutingModule"]
        ],
        declarations: [_bloqueio_page__WEBPACK_IMPORTED_MODULE_6__["BloqueioPage"]]
    })
], BloqueioPageModule);



/***/ }),

/***/ "./src/app/bloqueio/bloqueio.page.scss":
/*!*********************************************!*\
  !*** ./src/app/bloqueio/bloqueio.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-card, img {\n  border-radius: 5%;\n  border: #001c4e 0.5px solid;\n}\n\nion-toolbar {\n  --background: #001c4e;\n}\n\nion-grid {\n  margin: auto;\n  display: block;\n  width: 95%;\n  border: 1px solid;\n  border-left: none;\n  border-right: none;\n  padding: 0px;\n}\n\nion-input {\n  --highlight-color-focused: var(--ion-color-primary, #001c4e);\n  --highlight-color-valid: var(--ion-color-success, #2dd36f);\n  --highlight-color-invalid: var(--ion-color-danger, #eb445a);\n  --item-highlight: var(--ion-color-primary, #001c4e);\n}\n\nion-title {\n  text-align: center;\n  color: #001c4e;\n  font-size: 1.5em;\n  display: block;\n}\n\n.botao {\n  --background: #b1ff49;\n  --color: #001c4e;\n  font-weight: bold;\n  display: block;\n  height: 48px;\n  width: 45%;\n  margin: auto;\n  text-transform: uppercase;\n  --border-radius: 50px;\n  --background-activated: #001c4e;\n  --color-activated: white;\n  --background-hover: #001c4e;\n  --color-hover: white;\n  --box-shadow: 0 0 0 0;\n  outline: none !important;\n}\n\nion-button {\n  --background:red;\n  color: #fff;\n  font-weight: bold;\n  border-radius: 50px;\n}\n\n.editarButton {\n  --background:#b1ff49;\n  color: #001c4e;\n  font-weight: bold;\n  border-radius: 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYmxvcXVlaW8vYmxvcXVlaW8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSwyQkFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7QUFDSjs7QUFHQTtFQUNJLFlBQUE7RUFDQSxjQUFBO0VBQ0EsVUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUFBSjs7QUFJQTtFQUNJLDREQUFBO0VBQ0EsMERBQUE7RUFDQSwyREFBQTtFQUNBLG1EQUFBO0FBREo7O0FBSUE7RUFDSSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUFESjs7QUFJQTtFQUNJLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLHFCQUFBO0VBQ0EsK0JBQUE7RUFDQSx3QkFBQTtFQUNBLDJCQUFBO0VBQ0Esb0JBQUE7RUFDRCxxQkFBQTtFQUNDLHdCQUFBO0FBREo7O0FBTUE7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBSEo7O0FBTUE7RUFDSSxvQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBSEoiLCJmaWxlIjoic3JjL2FwcC9ibG9xdWVpby9ibG9xdWVpby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZCwgaW1nIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUlO1xyXG4gICAgYm9yZGVyOiAjMDAxYzRlIDAuNXB4IHNvbGlkO1xyXG59XHJcblxyXG5pb24tdG9vbGJhciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGU7XHJcbn1cclxuXHJcblxyXG5pb24tZ3JpZCB7XHJcbiAgICBtYXJnaW46IGF1dG87IFxyXG4gICAgZGlzcGxheTogYmxvY2s7IFxyXG4gICAgd2lkdGg6IDk1JTtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgYm9yZGVyLWxlZnQ6IG5vbmU7XHJcbiAgICBib3JkZXItcmlnaHQ6IG5vbmU7XHJcbiAgICBwYWRkaW5nOiAwcHg7XHJcbn1cclxuXHJcblxyXG5pb24taW5wdXQge1xyXG4gICAgLS1oaWdobGlnaHQtY29sb3ItZm9jdXNlZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnksICMwMDFjNGUpO1xyXG4gICAgLS1oaWdobGlnaHQtY29sb3ItdmFsaWQ6IHZhcigtLWlvbi1jb2xvci1zdWNjZXNzLCAjMmRkMzZmKTtcclxuICAgIC0taGlnaGxpZ2h0LWNvbG9yLWludmFsaWQ6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIsICNlYjQ0NWEpO1xyXG4gICAgLS1pdGVtLWhpZ2hsaWdodDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnksICMwMDFjNGUpO1xyXG59XHJcblxyXG5pb24tdGl0bGUge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyOyBcclxuICAgIGNvbG9yOiAjMDAxYzRlO1xyXG4gICAgZm9udC1zaXplOiAxLjVlbTtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4uYm90YW8ge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjYjFmZjQ5OyAgLy8jMDAxYzRlO1xyXG4gICAgLS1jb2xvcjogIzAwMWM0ZTsgLy93aGl0ZTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBoZWlnaHQ6IDQ4cHg7XHJcbiAgICB3aWR0aDogNDUlO1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6ICMwMDFjNGU7XHJcbiAgICAtLWNvbG9yLWFjdGl2YXRlZDogd2hpdGU7XHJcbiAgICAtLWJhY2tncm91bmQtaG92ZXI6ICMwMDFjNGU7XHJcbiAgICAtLWNvbG9yLWhvdmVyOiB3aGl0ZTtcclxuICAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xyXG4gICAgb3V0bGluZTogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5cclxuXHJcbmlvbi1idXR0b24ge1xyXG4gICAgLS1iYWNrZ3JvdW5kOnJlZDtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7IFxyXG4gICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxufVxyXG5cclxuLmVkaXRhckJ1dHRvbiB7XHJcbiAgICAtLWJhY2tncm91bmQ6I2IxZmY0OTtcclxuICAgIGNvbG9yOiAjMDAxYzRlO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7IFxyXG4gICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/bloqueio/bloqueio.page.ts":
/*!*******************************************!*\
  !*** ./src/app/bloqueio/bloqueio.page.ts ***!
  \*******************************************/
/*! exports provided: BloqueioPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BloqueioPage", function() { return BloqueioPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/core/user/user.service */ "./src/core/user/user.service.ts");
/* harmony import */ var src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/servico/motorista_liberacao */ "./src/servico/motorista_liberacao.ts");






let BloqueioPage = class BloqueioPage {
    constructor(router, route, motoristaService, motoristaLiberacaoService, toastCtrl) {
        this.router = router;
        this.route = route;
        this.motoristaService = motoristaService;
        this.motoristaLiberacaoService = motoristaLiberacaoService;
        this.toastCtrl = toastCtrl;
        this.url_foto = 'assets/img/no.png';
        this.url_crlv = 'assets/img/no.png';
        this.url_cnh = 'assets/img/no.png';
        this.user$ = motoristaService.getUser();
        this.route.params.subscribe(parametros => {
            this.motorista_id = parametros['user_id'];
        });
        this.user$.subscribe(usuario => {
            this.usuarioLogado = usuario;
            this.user_id = this.usuarioLogado.id;
            var a = this.usuarioLogado.full_name.split(' ');
            if (this.usuarioLogado.apelido == null) {
                this.apelido = '';
            }
            else {
                this.apelido = '(' + this.usuarioLogado.apelido + ')';
            }
            this.nomeMotorista = a[0] + ' ' + this.apelido;
        });
        this.motoristaLiberacaoService.motoristaLiberado().subscribe(motoristaLiberado => {
            this.m = motoristaLiberado;
        });
    }
    ngOnInit() {
    }
    bloqueio(id) {
        this.motoristaLiberacaoService.motoristaUnico(id).subscribe(motorista => {
            this.mot = motorista;
            this.nome = motorista.full_name;
            this.phone = this.mot.phone;
            this.email = this.mot.email;
            this.motoristaLiberacaoService.bloqueio(this.phone, this.email).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Motorista Bloqueado',
                    duration: 4000, position: 'top',
                    color: 'success'
                })).present();
            }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                console.log(err);
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Por favor confira se a sua internet está funcionando e tente cadastrar novamente! UaiLeva agradece!',
                    duration: 4000, position: 'top',
                    color: 'danger'
                })).present();
            }));
        });
    }
    editarMotorista(user_id) {
        this.router.navigate(['/editar-motorista/' + user_id + '']);
    }
};
BloqueioPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_5__["MotoristaLiberacaoService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] }
];
BloqueioPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-bloqueio',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./bloqueio.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/bloqueio/bloqueio.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./bloqueio.page.scss */ "./src/app/bloqueio/bloqueio.page.scss")).default]
    })
], BloqueioPage);



/***/ }),

/***/ "./src/core/token/token.service.ts":
/*!*****************************************!*\
  !*** ./src/core/token/token.service.ts ***!
  \*****************************************/
/*! exports provided: TokenService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenService", function() { return TokenService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


const KEY = 'authToken';
let TokenService = class TokenService {
    hasToken() {
        return !!this.getToken();
    }
    setToken(token) {
        window.localStorage.setItem(KEY, token);
    }
    getToken() {
        return window.localStorage.getItem(KEY);
    }
    removeToken() {
        window.localStorage.removeItem(KEY);
    }
};
TokenService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], TokenService);



/***/ }),

/***/ "./src/core/user/user.service.ts":
/*!***************************************!*\
  !*** ./src/core/user/user.service.ts ***!
  \***************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _token_token_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../token/token.service */ "./src/core/token/token.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/build/jwt-decode.esm.js");




//import * as jwt_decode from 'jwt-decode';

let UserService = class UserService {
    constructor(tokenService) {
        this.tokenService = tokenService;
        this.userSubject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](null);
        this.tokenService.hasToken() && this.decodeAndNotify();
    }
    setToken(token) {
        this.tokenService.setToken(token);
        this.decodeAndNotify();
    }
    getUser() {
        return this.userSubject.asObservable();
    }
    decodeAndNotify() {
        const token = this.tokenService.getToken();
        const user = Object(jwt_decode__WEBPACK_IMPORTED_MODULE_4__["default"])(token);
        this.fullName = user.full_name;
        this.userSubject.next(user);
    }
    logout() {
        this.tokenService.removeToken();
        this.userSubject.next(null);
    }
    isLogged() {
        return this.tokenService.hasToken();
    }
    getUserName() {
        return this.fullName;
    }
};
UserService.ctorParameters = () => [
    { type: _token_token_service__WEBPACK_IMPORTED_MODULE_2__["TokenService"] }
];
UserService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], UserService);



/***/ }),

/***/ "./src/servico/motorista_liberacao.ts":
/*!********************************************!*\
  !*** ./src/servico/motorista_liberacao.ts ***!
  \********************************************/
/*! exports provided: MotoristaLiberacaoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MotoristaLiberacaoService", function() { return MotoristaLiberacaoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



const API_URL = "https://uaileva.com.br/api";
let MotoristaLiberacaoService = class MotoristaLiberacaoService {
    constructor(http) {
        this.http = http;
    }
    liberar() {
        return this.http.get(API_URL + `/motorista/liberacao`);
    }
    motoristaUnico(motorista_id) {
        return this.http.get(API_URL + `/motorista/unico/` + motorista_id);
    }
    fotoMotorista(mot_phone) {
        return this.http.get(API_URL + `/photos/motorista/` + mot_phone);
    }
    fotoCrlv(mot_phone) {
        return this.http.get(API_URL + `/photos/motorista/crlv/` + mot_phone);
    }
    fotoCnh(mot_phone) {
        return this.http.get(API_URL + `/photos/motorista/cnh/` + mot_phone);
    }
    carro(id_motorista, placa, modelo) {
        return this.http.get(API_URL + `/carro/signup/` + id_motorista + `/` + placa + `/` + modelo + ``);
    }
    updateNome(user_id, nome, secondName) {
        return this.http.post(`${API_URL}/motorista/updateNome`, { user_id, nome, secondName });
    }
    ;
    liberarMotorista(phone, email) {
        return this.http.get(API_URL + `/motorista/status/` + phone + `/` + email + `/LIBERADO`);
    }
    motoristaLiberado() {
        return this.http.get(API_URL + `/motoristaLiberado`);
    }
    bloqueio(phone, email) {
        return this.http.get(API_URL + `/motorista/status/` + phone + `/` + email + `/BLOQUEADO`);
    }
    bloqueioPassageiro(phone) {
        return this.http.get(API_URL + `/user/bloqueio/` + phone + `/BLOQUEADO`);
    }
    corridaCM(data) {
        console.log(data);
        return this.http.get(API_URL + `/corrida/diariacm/` + data + ``);
    }
    corridaCP(data) {
        return this.http.get(API_URL + `/corrida/diariacp/` + data + ``);
    }
    corridaF(data) {
        return this.http.get(API_URL + `/corrida/diariaf/` + data + ``);
    }
    corridaTotal() {
        return this.http.get(API_URL + `/totalCorridas`);
    }
    motoristaLiberadoUnico(motorista_id) {
        return this.http.get(API_URL + `/motorista/liberadoUnico/` + motorista_id);
    }
    arrecadacao(motorista_id) {
        return this.http.get(API_URL + `/totalarrecadado/` + motorista_id);
    }
    desbloqueio(phone, email) {
        return this.http.get(API_URL + `/motorista/status/` + phone + `/` + email + `/LIBERADO`);
    }
    motbloqueado() {
        return this.http.get(API_URL + `/motorista/bloqueado`);
    }
};
MotoristaLiberacaoService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
MotoristaLiberacaoService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], MotoristaLiberacaoService);



/***/ })

}]);
//# sourceMappingURL=bloqueio-bloqueio-module-es2015.js.map
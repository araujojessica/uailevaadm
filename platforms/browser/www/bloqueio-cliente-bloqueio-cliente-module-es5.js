(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["bloqueio-cliente-bloqueio-cliente-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/bloqueio-cliente/bloqueio-cliente.page.html":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/bloqueio-cliente/bloqueio-cliente.page.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppBloqueioClienteBloqueioClientePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"login-home\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <br><br>\r\n  <ion-title>Bloqueio de Cliente</ion-title><br>\r\n  <ion-grid>\r\n    <form>\r\n      <ion-row>\r\n        <ion-col>\r\n          <ion-item>\r\n            <ion-label position=\"stacked\">Telefone</ion-label>\r\n            <ion-input  [(ngModel)]=\"phone\" name = \"phone\" ></ion-input>\r\n          </ion-item>\r\n        </ion-col>\r\n      </ion-row>\r\n    </form>\r\n\r\n    <br>\r\n\r\n    <ion-button (click)=\"bloquear();\" class=\"botao\">\r\n      <ion-icon name=\"checkmark-outline\"  slot=\"end\" style=\"color: #001c4e;\"></ion-icon>Bloquear\r\n    </ion-button>\r\n    \r\n    <br>\r\n\r\n  </ion-grid>\r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "./src/app/bloqueio-cliente/bloqueio-cliente-routing.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/bloqueio-cliente/bloqueio-cliente-routing.module.ts ***!
      \*********************************************************************/

    /*! exports provided: BloqueioClientePageRoutingModule */

    /***/
    function srcAppBloqueioClienteBloqueioClienteRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BloqueioClientePageRoutingModule", function () {
        return BloqueioClientePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _bloqueio_cliente_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./bloqueio-cliente.page */
      "./src/app/bloqueio-cliente/bloqueio-cliente.page.ts");

      var routes = [{
        path: '',
        component: _bloqueio_cliente_page__WEBPACK_IMPORTED_MODULE_3__["BloqueioClientePage"]
      }];

      var BloqueioClientePageRoutingModule = function BloqueioClientePageRoutingModule() {
        _classCallCheck(this, BloqueioClientePageRoutingModule);
      };

      BloqueioClientePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], BloqueioClientePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/bloqueio-cliente/bloqueio-cliente.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/bloqueio-cliente/bloqueio-cliente.module.ts ***!
      \*************************************************************/

    /*! exports provided: BloqueioClientePageModule */

    /***/
    function srcAppBloqueioClienteBloqueioClienteModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BloqueioClientePageModule", function () {
        return BloqueioClientePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _bloqueio_cliente_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./bloqueio-cliente-routing.module */
      "./src/app/bloqueio-cliente/bloqueio-cliente-routing.module.ts");
      /* harmony import */


      var _bloqueio_cliente_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./bloqueio-cliente.page */
      "./src/app/bloqueio-cliente/bloqueio-cliente.page.ts");

      var BloqueioClientePageModule = function BloqueioClientePageModule() {
        _classCallCheck(this, BloqueioClientePageModule);
      };

      BloqueioClientePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _bloqueio_cliente_routing_module__WEBPACK_IMPORTED_MODULE_5__["BloqueioClientePageRoutingModule"]],
        declarations: [_bloqueio_cliente_page__WEBPACK_IMPORTED_MODULE_6__["BloqueioClientePage"]]
      })], BloqueioClientePageModule);
      /***/
    },

    /***/
    "./src/app/bloqueio-cliente/bloqueio-cliente.page.scss":
    /*!*************************************************************!*\
      !*** ./src/app/bloqueio-cliente/bloqueio-cliente.page.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppBloqueioClienteBloqueioClientePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-card, img {\n  border-radius: 5%;\n  border: #001c4e 0.5px solid;\n}\n\nion-toolbar {\n  --background: #001c4e;\n}\n\nion-grid {\n  margin: auto;\n  display: block;\n  width: 95%;\n  border: 1px solid;\n  border-left: none;\n  border-right: none;\n  padding: 0px;\n}\n\nion-input {\n  --highlight-color-focused: var(--ion-color-primary, #001c4e);\n  --highlight-color-valid: var(--ion-color-success, #2dd36f);\n  --highlight-color-invalid: var(--ion-color-danger, #eb445a);\n  --item-highlight: var(--ion-color-primary, #001c4e);\n}\n\nion-title {\n  text-align: center;\n  color: #001c4e;\n  font-size: 1.5em;\n  display: block;\n}\n\n.botao {\n  --background: #b1ff49;\n  --color: #001c4e;\n  font-weight: bold;\n  display: block;\n  height: 48px;\n  width: 45%;\n  margin: auto;\n  text-transform: uppercase;\n  --border-radius: 50px;\n  --background-activated: #001c4e;\n  --color-activated: white;\n  --background-hover: #001c4e;\n  --color-hover: white;\n  --box-shadow: 0 0 0 0;\n  outline: none !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYmxvcXVlaW8tY2xpZW50ZS9ibG9xdWVpby1jbGllbnRlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EsMkJBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0FBQ0o7O0FBR0E7RUFDSSxZQUFBO0VBQ0EsY0FBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FBQUo7O0FBSUE7RUFDSSw0REFBQTtFQUNBLDBEQUFBO0VBQ0EsMkRBQUE7RUFDQSxtREFBQTtBQURKOztBQUlBO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FBREo7O0FBSUE7RUFDSSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxxQkFBQTtFQUNBLCtCQUFBO0VBQ0Esd0JBQUE7RUFDQSwyQkFBQTtFQUNBLG9CQUFBO0VBQ0QscUJBQUE7RUFDQyx3QkFBQTtBQURKIiwiZmlsZSI6InNyYy9hcHAvYmxvcXVlaW8tY2xpZW50ZS9ibG9xdWVpby1jbGllbnRlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkLCBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNSU7XHJcbiAgICBib3JkZXI6ICMwMDFjNGUgMC41cHggc29saWQ7XHJcbn1cclxuXHJcbmlvbi10b29sYmFyIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzAwMWM0ZTtcclxufVxyXG5cclxuXHJcbmlvbi1ncmlkIHtcclxuICAgIG1hcmdpbjogYXV0bzsgXHJcbiAgICBkaXNwbGF5OiBibG9jazsgXHJcbiAgICB3aWR0aDogOTUlO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICBib3JkZXItbGVmdDogbm9uZTtcclxuICAgIGJvcmRlci1yaWdodDogbm9uZTtcclxuICAgIHBhZGRpbmc6IDBweDtcclxufVxyXG5cclxuXHJcbmlvbi1pbnB1dCB7XHJcbiAgICAtLWhpZ2hsaWdodC1jb2xvci1mb2N1c2VkOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSwgIzAwMWM0ZSk7XHJcbiAgICAtLWhpZ2hsaWdodC1jb2xvci12YWxpZDogdmFyKC0taW9uLWNvbG9yLXN1Y2Nlc3MsICMyZGQzNmYpO1xyXG4gICAgLS1oaWdobGlnaHQtY29sb3ItaW52YWxpZDogdmFyKC0taW9uLWNvbG9yLWRhbmdlciwgI2ViNDQ1YSk7XHJcbiAgICAtLWl0ZW0taGlnaGxpZ2h0OiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSwgIzAwMWM0ZSk7XHJcbn1cclxuXHJcbmlvbi10aXRsZSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7IFxyXG4gICAgY29sb3I6ICMwMDFjNGU7XHJcbiAgICBmb250LXNpemU6IDEuNWVtO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi5ib3RhbyB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICNiMWZmNDk7ICAvLyMwMDFjNGU7XHJcbiAgICAtLWNvbG9yOiAjMDAxYzRlOyAvL3doaXRlO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGhlaWdodDogNDhweDtcclxuICAgIHdpZHRoOiA0NSU7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgLS1ib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogIzAwMWM0ZTtcclxuICAgIC0tY29sb3ItYWN0aXZhdGVkOiB3aGl0ZTtcclxuICAgIC0tYmFja2dyb3VuZC1ob3ZlcjogIzAwMWM0ZTtcclxuICAgIC0tY29sb3ItaG92ZXI6IHdoaXRlO1xyXG4gICAtLWJveC1zaGFkb3c6IDAgMCAwIDA7XHJcbiAgICBvdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7XHJcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/bloqueio-cliente/bloqueio-cliente.page.ts":
    /*!***********************************************************!*\
      !*** ./src/app/bloqueio-cliente/bloqueio-cliente.page.ts ***!
      \***********************************************************/

    /*! exports provided: BloqueioClientePage */

    /***/
    function srcAppBloqueioClienteBloqueioClientePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BloqueioClientePage", function () {
        return BloqueioClientePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/servico/motorista_liberacao */
      "./src/servico/motorista_liberacao.ts");

      var BloqueioClientePage = /*#__PURE__*/function () {
        function BloqueioClientePage(usuarioService, toastCtrl) {
          _classCallCheck(this, BloqueioClientePage);

          this.usuarioService = usuarioService;
          this.toastCtrl = toastCtrl;
        }

        _createClass(BloqueioClientePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "bloquear",
          value: function bloquear() {
            var _this = this;

            this.usuarioService.bloqueioPassageiro(this.phone).subscribe(function () {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                var toast;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        _context.next = 2;
                        return this.toastCtrl.create({
                          message: 'Passageiro ' + this.phone + ' bloqueado com sucesso!',
                          duration: 4000,
                          position: 'top',
                          color: 'success'
                        });

                      case 2:
                        _context.next = 4;
                        return _context.sent.present();

                      case 4:
                        toast = _context.sent;

                      case 5:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));
            }, function (err) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                var toast;
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        console.log(err);
                        _context2.next = 3;
                        return this.toastCtrl.create({
                          message: 'Por favor confira se a sua internet está funcionando e tente novamente! UaiLeva agradece!',
                          duration: 4000,
                          position: 'top',
                          color: 'danger'
                        });

                      case 3:
                        _context2.next = 5;
                        return _context2.sent.present();

                      case 5:
                        toast = _context2.sent;

                      case 6:
                      case "end":
                        return _context2.stop();
                    }
                  }
                }, _callee2, this);
              }));
            });
          }
        }]);

        return BloqueioClientePage;
      }();

      BloqueioClientePage.ctorParameters = function () {
        return [{
          type: src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_3__["MotoristaLiberacaoService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }];
      };

      BloqueioClientePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-bloqueio-cliente',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./bloqueio-cliente.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/bloqueio-cliente/bloqueio-cliente.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./bloqueio-cliente.page.scss */
        "./src/app/bloqueio-cliente/bloqueio-cliente.page.scss"))["default"]]
      })], BloqueioClientePage);
      /***/
    },

    /***/
    "./src/servico/motorista_liberacao.ts":
    /*!********************************************!*\
      !*** ./src/servico/motorista_liberacao.ts ***!
      \********************************************/

    /*! exports provided: MotoristaLiberacaoService */

    /***/
    function srcServicoMotorista_liberacaoTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MotoristaLiberacaoService", function () {
        return MotoristaLiberacaoService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var API_URL = "https://uaileva.com.br/api";

      var MotoristaLiberacaoService = /*#__PURE__*/function () {
        function MotoristaLiberacaoService(http) {
          _classCallCheck(this, MotoristaLiberacaoService);

          this.http = http;
        }

        _createClass(MotoristaLiberacaoService, [{
          key: "liberar",
          value: function liberar() {
            return this.http.get(API_URL + "/motorista/liberacao");
          }
        }, {
          key: "motoristaUnico",
          value: function motoristaUnico(motorista_id) {
            return this.http.get(API_URL + "/motorista/unico/" + motorista_id);
          }
        }, {
          key: "fotoMotorista",
          value: function fotoMotorista(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/" + mot_phone);
          }
        }, {
          key: "fotoCrlv",
          value: function fotoCrlv(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/crlv/" + mot_phone);
          }
        }, {
          key: "fotoCnh",
          value: function fotoCnh(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/cnh/" + mot_phone);
          }
        }, {
          key: "carro",
          value: function carro(id_motorista, placa, modelo) {
            return this.http.get(API_URL + "/carro/signup/" + id_motorista + "/" + placa + "/" + modelo + "");
          }
        }, {
          key: "updateNome",
          value: function updateNome(user_id, nome, secondName) {
            return this.http.post("".concat(API_URL, "/motorista/updateNome"), {
              user_id: user_id,
              nome: nome,
              secondName: secondName
            });
          }
        }, {
          key: "liberarMotorista",
          value: function liberarMotorista(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/LIBERADO");
          }
        }, {
          key: "motoristaLiberado",
          value: function motoristaLiberado() {
            return this.http.get(API_URL + "/motoristaLiberado");
          }
        }, {
          key: "bloqueio",
          value: function bloqueio(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/BLOQUEADO");
          }
        }, {
          key: "bloqueioPassageiro",
          value: function bloqueioPassageiro(phone) {
            return this.http.get(API_URL + "/user/bloqueio/" + phone + "/BLOQUEADO");
          }
        }, {
          key: "corridaCM",
          value: function corridaCM(data) {
            console.log(data);
            return this.http.get(API_URL + "/corrida/diariacm/" + data + "");
          }
        }, {
          key: "corridaCP",
          value: function corridaCP(data) {
            return this.http.get(API_URL + "/corrida/diariacp/" + data + "");
          }
        }, {
          key: "corridaF",
          value: function corridaF(data) {
            return this.http.get(API_URL + "/corrida/diariaf/" + data + "");
          }
        }, {
          key: "corridaTotal",
          value: function corridaTotal() {
            return this.http.get(API_URL + "/totalCorridas");
          }
        }, {
          key: "motoristaLiberadoUnico",
          value: function motoristaLiberadoUnico(motorista_id) {
            return this.http.get(API_URL + "/motorista/liberadoUnico/" + motorista_id);
          }
        }, {
          key: "arrecadacao",
          value: function arrecadacao(motorista_id) {
            return this.http.get(API_URL + "/totalarrecadado/" + motorista_id);
          }
        }, {
          key: "desbloqueio",
          value: function desbloqueio(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/LIBERADO");
          }
        }, {
          key: "motbloqueado",
          value: function motbloqueado() {
            return this.http.get(API_URL + "/motorista/bloqueado");
          }
        }]);

        return MotoristaLiberacaoService;
      }();

      MotoristaLiberacaoService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      MotoristaLiberacaoService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], MotoristaLiberacaoService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=bloqueio-cliente-bloqueio-cliente-module-es5.js.map
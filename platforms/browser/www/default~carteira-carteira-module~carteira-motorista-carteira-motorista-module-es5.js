(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~carteira-carteira-module~carteira-motorista-carteira-motorista-module"], {
    /***/
    "./node_modules/jwt-decode/build/jwt-decode.esm.js":
    /*!*********************************************************!*\
      !*** ./node_modules/jwt-decode/build/jwt-decode.esm.js ***!
      \*********************************************************/

    /*! exports provided: default, InvalidTokenError */

    /***/
    function node_modulesJwtDecodeBuildJwtDecodeEsmJs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "InvalidTokenError", function () {
        return n;
      });

      function e(e) {
        this.message = e;
      }

      e.prototype = new Error(), e.prototype.name = "InvalidCharacterError";

      var r = "undefined" != typeof window && window.atob && window.atob.bind(window) || function (r) {
        var t = String(r).replace(/=+$/, "");
        if (t.length % 4 == 1) throw new e("'atob' failed: The string to be decoded is not correctly encoded.");

        for (var n, o, a = 0, i = 0, c = ""; o = t.charAt(i++); ~o && (n = a % 4 ? 64 * n + o : o, a++ % 4) ? c += String.fromCharCode(255 & n >> (-2 * a & 6)) : 0) {
          o = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(o);
        }

        return c;
      };

      function t(e) {
        var t = e.replace(/-/g, "+").replace(/_/g, "/");

        switch (t.length % 4) {
          case 0:
            break;

          case 2:
            t += "==";
            break;

          case 3:
            t += "=";
            break;

          default:
            throw "Illegal base64url string!";
        }

        try {
          return function (e) {
            return decodeURIComponent(r(e).replace(/(.)/g, function (e, r) {
              var t = r.charCodeAt(0).toString(16).toUpperCase();
              return t.length < 2 && (t = "0" + t), "%" + t;
            }));
          }(t);
        } catch (e) {
          return r(t);
        }
      }

      function n(e) {
        this.message = e;
      }

      function o(e, r) {
        if ("string" != typeof e) throw new n("Invalid token specified");
        var o = !0 === (r = r || {}).header ? 0 : 1;

        try {
          return JSON.parse(t(e.split(".")[o]));
        } catch (e) {
          throw new n("Invalid token specified: " + e.message);
        }
      }

      n.prototype = new Error(), n.prototype.name = "InvalidTokenError";
      /* harmony default export */

      __webpack_exports__["default"] = o; //# sourceMappingURL=jwt-decode.esm.js.map

      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/carteira-motorista/carteira-motorista.page.html":
    /*!*******************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/carteira-motorista/carteira-motorista.page.html ***!
      \*******************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppCarteiraMotoristaCarteiraMotoristaPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header no-border>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismiss()\"><ion-icon name=\"close-outline\" style=\"color: white;\" size=\"large\"></ion-icon></ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-col class=\"col-md-12\">\r\n        <ion-item>\r\n          <ion-label><h1>{{nome}}</h1></ion-label>&nbsp;&nbsp;&nbsp;\r\n          <img src=\"{{url_foto}}\"/>\r\n        </ion-item>\r\n      </ion-col>\r\n    </ion-row>\r\n    <br>\r\n\r\n    <ion-row>\r\n      <ion-col class=\"col-md-12\">\r\n        <ion-item>\r\n          <ion-label position=\"stacked\">Corridas dos últimos 7 dias</ion-label>\r\n          <ion-input placeholder=\"--\" readonly></ion-input>\r\n        </ion-item>\r\n      </ion-col>\r\n    </ion-row>\r\n   \r\n    <ion-row>\r\n      <ion-col class=\"col-md-12\">\r\n        <ion-item>\r\n          <ion-label position=\"stacked\">Total a pagar</ion-label>\r\n          <ion-input placeholder=\"R$ {{totalUai}}\" readonly></ion-input>\r\n          <ion-button slot=\"end\" class=\"quitarButton\">Quitar</ion-button>\r\n        </ion-item>\r\n        \r\n      </ion-col>\r\n    </ion-row>\r\n  \r\n    <ion-row>\r\n      <ion-col class=\"col-md-12\">\r\n        <ion-item>\r\n          <ion-label position=\"stacked\">Total arrecadado</ion-label>\r\n          <ion-input placeholder=\"R$ {{totalArrecadado}}\" readonly></ion-input>\r\n        </ion-item>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/carteira-motorista/carteira-motorista.page.scss":
    /*!*****************************************************************!*\
      !*** ./src/app/carteira-motorista/carteira-motorista.page.scss ***!
      \*****************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppCarteiraMotoristaCarteiraMotoristaPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-card, img {\n  border-radius: 5%;\n  border: #001c4e 0.5px solid;\n  width: 25%;\n}\n\nion-toolbar {\n  --background: #001c4e;\n}\n\nion-title {\n  color: #fff;\n}\n\nion-item {\n  --ion-item-background: transparent;\n  --border-style: var(--border-style);\n  --border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n\n.quitarButton {\n  --background:#b1ff49;\n  color: #001c4e;\n  font-weight: bold;\n  border-radius: 50px;\n  width: 35%;\n  height: 30px;\n  margin-top: 23px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FydGVpcmEtbW90b3Jpc3RhL2NhcnRlaXJhLW1vdG9yaXN0YS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtFQUNBLDJCQUFBO0VBQ0EsVUFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7QUFDSjs7QUFHQTtFQUdJLGtDQUFBO0VBQ0EsbUNBQUE7RUFDQSxnQkFBQTtFQUNELG1CQUFBO0VBQ0MsVUFBQTtBQUZKOztBQUtBO0VBQ0ksb0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUFGSiIsImZpbGUiOiJzcmMvYXBwL2NhcnRlaXJhLW1vdG9yaXN0YS9jYXJ0ZWlyYS1tb3RvcmlzdGEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNhcmQsIGltZyB7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1JTtcclxuICAgIGJvcmRlcjogIzAwMWM0ZSAwLjVweCBzb2xpZDtcclxuICAgIHdpZHRoOiAyNSU7XHJcbn1cclxuXHJcbmlvbi10b29sYmFyIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzAwMWM0ZTtcclxufVxyXG5cclxuaW9uLXRpdGxlIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG59XHJcblxyXG4gXHJcbmlvbi1pdGVtIHtcclxuICAgIC8vLS1jb2xvcjogYmxhY2s7XHJcbiAgICAvL2RlaXhhciBtZW51IHNlbSBsaW5oYXNcclxuICAgIC0taW9uLWl0ZW0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAtLWJvcmRlci1zdHlsZTogdmFyKC0tYm9yZGVyLXN0eWxlKTtcclxuICAgIC0tYm9yZGVyOiAwIG5vbmU7IFxyXG4gICBib3gtc2hhZG93OiAwIDAgMCAwOyBcclxuICAgIG91dGxpbmU6IDA7XHJcbn1cclxuXHJcbi5xdWl0YXJCdXR0b24ge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiNiMWZmNDk7XHJcbiAgICBjb2xvcjogIzAwMWM0ZTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkOyBcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICB3aWR0aDogMzUlO1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMjNweDtcclxufVxyXG4iXX0= */";
      /***/
    },

    /***/
    "./src/app/carteira-motorista/carteira-motorista.page.ts":
    /*!***************************************************************!*\
      !*** ./src/app/carteira-motorista/carteira-motorista.page.ts ***!
      \***************************************************************/

    /*! exports provided: CarteiraMotoristaPage */

    /***/
    function srcAppCarteiraMotoristaCarteiraMotoristaPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CarteiraMotoristaPage", function () {
        return CarteiraMotoristaPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/core/user/user.service */
      "./src/core/user/user.service.ts");
      /* harmony import */


      var src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/servico/motorista_liberacao */
      "./src/servico/motorista_liberacao.ts");

      var CarteiraMotoristaPage = /*#__PURE__*/function () {
        function CarteiraMotoristaPage(router, route, motoristaService, motoristaLiberacaoService, toastCtrl, modalCtrl, params) {
          var _this = this;

          _classCallCheck(this, CarteiraMotoristaPage);

          this.router = router;
          this.route = route;
          this.motoristaService = motoristaService;
          this.motoristaLiberacaoService = motoristaLiberacaoService;
          this.toastCtrl = toastCtrl;
          this.modalCtrl = modalCtrl;
          this.params = params;
          this.apelido = ' - ';
          this.url_foto = 'assets/img/no.png';
          this.url_crlv = 'assets/img/no.png';
          this.url_cnh = 'assets/img/no.png';
          this.route.params.subscribe(function (parametros) {
            //this.motorista_id = parametros['user_id'];
            _this.motorista_id = _this.params.get('telMot');

            _this.motoristaLiberacaoService.motoristaUnico(_this.motorista_id).subscribe(function (motorista) {
              _this.mot = motorista;
              console.log(_this.mot);
              _this.nome = motorista.full_name; //this.phone = this.mot.phone;

              _this.email = _this.mot.email;
              _this.password = _this.mot.password;
              _this.cidade = _this.mot.cidade;
              _this.apelido = _this.mot.apelido;

              _this.motoristaLiberacaoService.fotoMotorista(_this.mot.phone).subscribe(function (foto_motorista) {
                _this.photo = foto_motorista;
                _this.url_foto = "https://uaileva.com.br/api/imgs/" + _this.photo.url;
              });

              _this.motoristaLiberacaoService.fotoCrlv(_this.mot.phone).subscribe(function (foto_crlv) {
                _this.photo_crlv = foto_crlv;
                _this.url_crlv = "https://uaileva.com.br/api/imgs/" + _this.photo_crlv.url;
              });

              _this.motoristaLiberacaoService.fotoCnh(_this.mot.phone).subscribe(function (foto_cnh) {
                _this.photo_cnh = foto_cnh;
                _this.url_cnh = "https://uaileva.com.br/api/imgs/" + _this.photo_cnh.url;
              });
            });

            _this.motoristaLiberacaoService.arrecadacao(_this.motorista_id).subscribe(function (arrecadacao) {
              _this.totalArrecadado = arrecadacao.total;
              _this.totalUai = arrecadacao.uaileva;
            });
          });
        }

        _createClass(CarteiraMotoristaPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "dismiss",
          value: function dismiss() {
            this.modalCtrl.dismiss({
              'dismissed': true
            });
          }
        }]);

        return CarteiraMotoristaPage;
      }();

      CarteiraMotoristaPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: src_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]
        }, {
          type: src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_5__["MotoristaLiberacaoService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavParams"]
        }];
      };

      CarteiraMotoristaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-carteira-motorista',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./carteira-motorista.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/carteira-motorista/carteira-motorista.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./carteira-motorista.page.scss */
        "./src/app/carteira-motorista/carteira-motorista.page.scss"))["default"]]
      })], CarteiraMotoristaPage);
      /***/
    },

    /***/
    "./src/core/token/token.service.ts":
    /*!*****************************************!*\
      !*** ./src/core/token/token.service.ts ***!
      \*****************************************/

    /*! exports provided: TokenService */

    /***/
    function srcCoreTokenTokenServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TokenService", function () {
        return TokenService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var KEY = 'authToken';

      var TokenService = /*#__PURE__*/function () {
        function TokenService() {
          _classCallCheck(this, TokenService);
        }

        _createClass(TokenService, [{
          key: "hasToken",
          value: function hasToken() {
            return !!this.getToken();
          }
        }, {
          key: "setToken",
          value: function setToken(token) {
            window.localStorage.setItem(KEY, token);
          }
        }, {
          key: "getToken",
          value: function getToken() {
            return window.localStorage.getItem(KEY);
          }
        }, {
          key: "removeToken",
          value: function removeToken() {
            window.localStorage.removeItem(KEY);
          }
        }]);

        return TokenService;
      }();

      TokenService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], TokenService);
      /***/
    },

    /***/
    "./src/core/user/user.service.ts":
    /*!***************************************!*\
      !*** ./src/core/user/user.service.ts ***!
      \***************************************/

    /*! exports provided: UserService */

    /***/
    function srcCoreUserUserServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserService", function () {
        return UserService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _token_token_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../token/token.service */
      "./src/core/token/token.service.ts");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var jwt_decode__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! jwt-decode */
      "./node_modules/jwt-decode/build/jwt-decode.esm.js"); //import * as jwt_decode from 'jwt-decode';


      var UserService = /*#__PURE__*/function () {
        function UserService(tokenService) {
          _classCallCheck(this, UserService);

          this.tokenService = tokenService;
          this.userSubject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](null);
          this.tokenService.hasToken() && this.decodeAndNotify();
        }

        _createClass(UserService, [{
          key: "setToken",
          value: function setToken(token) {
            this.tokenService.setToken(token);
            this.decodeAndNotify();
          }
        }, {
          key: "getUser",
          value: function getUser() {
            return this.userSubject.asObservable();
          }
        }, {
          key: "decodeAndNotify",
          value: function decodeAndNotify() {
            var token = this.tokenService.getToken();
            var user = Object(jwt_decode__WEBPACK_IMPORTED_MODULE_4__["default"])(token);
            this.fullName = user.full_name;
            this.userSubject.next(user);
          }
        }, {
          key: "logout",
          value: function logout() {
            this.tokenService.removeToken();
            this.userSubject.next(null);
          }
        }, {
          key: "isLogged",
          value: function isLogged() {
            return this.tokenService.hasToken();
          }
        }, {
          key: "getUserName",
          value: function getUserName() {
            return this.fullName;
          }
        }]);

        return UserService;
      }();

      UserService.ctorParameters = function () {
        return [{
          type: _token_token_service__WEBPACK_IMPORTED_MODULE_2__["TokenService"]
        }];
      };

      UserService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], UserService);
      /***/
    },

    /***/
    "./src/servico/motorista_liberacao.ts":
    /*!********************************************!*\
      !*** ./src/servico/motorista_liberacao.ts ***!
      \********************************************/

    /*! exports provided: MotoristaLiberacaoService */

    /***/
    function srcServicoMotorista_liberacaoTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MotoristaLiberacaoService", function () {
        return MotoristaLiberacaoService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var API_URL = "https://uaileva.com.br/api";

      var MotoristaLiberacaoService = /*#__PURE__*/function () {
        function MotoristaLiberacaoService(http) {
          _classCallCheck(this, MotoristaLiberacaoService);

          this.http = http;
        }

        _createClass(MotoristaLiberacaoService, [{
          key: "liberar",
          value: function liberar() {
            return this.http.get(API_URL + "/motorista/liberacao");
          }
        }, {
          key: "motoristaUnico",
          value: function motoristaUnico(motorista_id) {
            return this.http.get(API_URL + "/motorista/unico/" + motorista_id);
          }
        }, {
          key: "fotoMotorista",
          value: function fotoMotorista(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/" + mot_phone);
          }
        }, {
          key: "fotoCrlv",
          value: function fotoCrlv(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/crlv/" + mot_phone);
          }
        }, {
          key: "fotoCnh",
          value: function fotoCnh(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/cnh/" + mot_phone);
          }
        }, {
          key: "carro",
          value: function carro(id_motorista, placa, modelo) {
            return this.http.get(API_URL + "/carro/signup/" + id_motorista + "/" + placa + "/" + modelo + "");
          }
        }, {
          key: "updateNome",
          value: function updateNome(user_id, nome, secondName) {
            return this.http.post("".concat(API_URL, "/motorista/updateNome"), {
              user_id: user_id,
              nome: nome,
              secondName: secondName
            });
          }
        }, {
          key: "liberarMotorista",
          value: function liberarMotorista(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/LIBERADO");
          }
        }, {
          key: "motoristaLiberado",
          value: function motoristaLiberado() {
            return this.http.get(API_URL + "/motoristaLiberado");
          }
        }, {
          key: "bloqueio",
          value: function bloqueio(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/BLOQUEADO");
          }
        }, {
          key: "bloqueioPassageiro",
          value: function bloqueioPassageiro(phone) {
            return this.http.get(API_URL + "/user/bloqueio/" + phone + "/BLOQUEADO");
          }
        }, {
          key: "corridaCM",
          value: function corridaCM(data) {
            console.log(data);
            return this.http.get(API_URL + "/corrida/diariacm/" + data + "");
          }
        }, {
          key: "corridaCP",
          value: function corridaCP(data) {
            return this.http.get(API_URL + "/corrida/diariacp/" + data + "");
          }
        }, {
          key: "corridaF",
          value: function corridaF(data) {
            return this.http.get(API_URL + "/corrida/diariaf/" + data + "");
          }
        }, {
          key: "corridaTotal",
          value: function corridaTotal() {
            return this.http.get(API_URL + "/totalCorridas");
          }
        }, {
          key: "motoristaLiberadoUnico",
          value: function motoristaLiberadoUnico(motorista_id) {
            return this.http.get(API_URL + "/motorista/liberadoUnico/" + motorista_id);
          }
        }, {
          key: "arrecadacao",
          value: function arrecadacao(motorista_id) {
            return this.http.get(API_URL + "/totalarrecadado/" + motorista_id);
          }
        }, {
          key: "desbloqueio",
          value: function desbloqueio(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/LIBERADO");
          }
        }, {
          key: "motbloqueado",
          value: function motbloqueado() {
            return this.http.get(API_URL + "/motorista/bloqueado");
          }
        }]);

        return MotoristaLiberacaoService;
      }();

      MotoristaLiberacaoService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      MotoristaLiberacaoService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], MotoristaLiberacaoService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=default~carteira-carteira-module~carteira-motorista-carteira-motorista-module-es5.js.map
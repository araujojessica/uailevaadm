(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["editar-motorista-editar-motorista-module"], {
    /***/
    "./node_modules/jwt-decode/build/jwt-decode.esm.js":
    /*!*********************************************************!*\
      !*** ./node_modules/jwt-decode/build/jwt-decode.esm.js ***!
      \*********************************************************/

    /*! exports provided: default, InvalidTokenError */

    /***/
    function node_modulesJwtDecodeBuildJwtDecodeEsmJs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "InvalidTokenError", function () {
        return n;
      });

      function e(e) {
        this.message = e;
      }

      e.prototype = new Error(), e.prototype.name = "InvalidCharacterError";

      var r = "undefined" != typeof window && window.atob && window.atob.bind(window) || function (r) {
        var t = String(r).replace(/=+$/, "");
        if (t.length % 4 == 1) throw new e("'atob' failed: The string to be decoded is not correctly encoded.");

        for (var n, o, a = 0, i = 0, c = ""; o = t.charAt(i++); ~o && (n = a % 4 ? 64 * n + o : o, a++ % 4) ? c += String.fromCharCode(255 & n >> (-2 * a & 6)) : 0) {
          o = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(o);
        }

        return c;
      };

      function t(e) {
        var t = e.replace(/-/g, "+").replace(/_/g, "/");

        switch (t.length % 4) {
          case 0:
            break;

          case 2:
            t += "==";
            break;

          case 3:
            t += "=";
            break;

          default:
            throw "Illegal base64url string!";
        }

        try {
          return function (e) {
            return decodeURIComponent(r(e).replace(/(.)/g, function (e, r) {
              var t = r.charCodeAt(0).toString(16).toUpperCase();
              return t.length < 2 && (t = "0" + t), "%" + t;
            }));
          }(t);
        } catch (e) {
          return r(t);
        }
      }

      function n(e) {
        this.message = e;
      }

      function o(e, r) {
        if ("string" != typeof e) throw new n("Invalid token specified");
        var o = !0 === (r = r || {}).header ? 0 : 1;

        try {
          return JSON.parse(t(e.split(".")[o]));
        } catch (e) {
          throw new n("Invalid token specified: " + e.message);
        }
      }

      n.prototype = new Error(), n.prototype.name = "InvalidTokenError";
      /* harmony default export */

      __webpack_exports__["default"] = o; //# sourceMappingURL=jwt-decode.esm.js.map

      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/editar-motorista/editar-motorista.page.html":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/editar-motorista/editar-motorista.page.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppEditarMotoristaEditarMotoristaPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"bloqueio\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <br><br>\r\n  <ion-title>Editar Perfil</ion-title><br>\r\n  <form style=\"padding-left: 25px; padding-right: 25px;\">\r\n    <ion-item>\r\n      <ion-label position=\"stacked\">Nome Completo</ion-label>\r\n      <ion-input [(ngModel)]=\"nomeNovo\" placeholder=\"{{nome}}\" name = \"nomeNovo\" type=\"text\"></ion-input>\r\n    </ion-item>\r\n    \r\n    <ion-item>\r\n      <ion-label position=\"stacked\">Apelido</ion-label>\r\n      <ion-input [(ngModel)]=\"apelidoNovo\" placeholder=\"{{apelido}}\" name = \"apelidoNovo\" type=\"text\"></ion-input>\r\n    </ion-item>\r\n    \r\n    <ion-item>\r\n      <ion-label position=\"stacked\">Telefone</ion-label>\r\n      <ion-input [(ngModel)]=\"phoneNovo\" placeholder=\"{{phone}}\" name = \"phoneNovo\" type=\"tel\"></ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label position=\"stacked\">E-mail</ion-label>\r\n      <ion-input [(ngModel)]=\"emailNovo\" placeholder=\"{{email}}\" name = \"emailNovo\" type=\"text\"></ion-input>\r\n    </ion-item>\r\n    \r\n    <ion-item>\r\n      <ion-label position=\"stacked\">Senha</ion-label>\r\n      <ion-input [(ngModel)]=\"senhaNovo\" name = \"senhaNovo\" placeholder=\"********\" type=\"password\"></ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label position=\"stacked\">Cidade</ion-label>\r\n      <ion-input [(ngModel)]=\"cidadeNovo\" placeholder=\"{{cidade}}\"  name = \"cidadeNovo\" type=\"text\"></ion-input>\r\n    </ion-item>\r\n\r\n    <br>\r\n    <ion-label position=\"stacked\" style=\"padding-left: 18px; font-size: small; font-weight: bold;\">Veículo</ion-label>\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-item>\r\n          <ion-label position=\"stacked\">Placa</ion-label>\r\n          <ion-input  [(ngModel)]=\"placaNovo\" placeholder=\"{{placa}}\"  name=\"placaNovo\"></ion-input>\r\n        </ion-item>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-item>\r\n          <ion-label position=\"stacked\">Modelo</ion-label>\r\n          <ion-input [(ngModel)]=\"modeloNovo\" placeholder=\"{{modelo}}\"  name = \"modeloNovo\"></ion-input>\r\n        </ion-item>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-grid>\r\n      <ion-col class=\"col-md-12\"><img src=\"{{url_foto}}\"/></ion-col>&nbsp;\r\n      <ion-col class=\"col-md-12\"><img src=\"{{url_cnh}}\"/></ion-col>&nbsp;\r\n      <ion-col class=\"col-md-12\"><img src=\"{{url_crlv}}\"/></ion-col>\r\n    </ion-grid>\r\n\r\n    <ion-grid>\r\n      <ion-col class=\"col-md-12\">\r\n        <ion-label>Foto perfil : </ion-label>&nbsp;&nbsp;\r\n        <input type=\"file\" (change)= \"file = $event.target.files[0]\" accept=\"image/*\">\r\n      </ion-col>&nbsp;\r\n      <ion-col class=\"col-md-12\">\r\n        <ion-label>Foto CNH : </ion-label>&nbsp;&nbsp;\r\n        <input type=\"file\" (change)= \"fileCNH = $event.target.files[0]\" accept=\"image/*\">\r\n      </ion-col>&nbsp;\r\n      <ion-col class=\"col-md-12\">\r\n        <ion-label>Foto CRLV : </ion-label>&nbsp;&nbsp;\r\n        <input type=\"file\" (change)= \"fileCRLV = $event.target.files[0]\" accept=\"image/*\">\r\n      </ion-col>\r\n    </ion-grid>\r\n  </form><br>\r\n\r\n  <ion-button (click) = \"salvar();\" class=\"botao\" tappable>\r\n    Salvar Alterações\r\n    <ion-icon name=\"checkmark-outline\" style=\"color: #001c4e;\"></ion-icon>\r\n  </ion-button>\r\n<br>\r\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/editar-motorista/editar-motorista-routing.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/editar-motorista/editar-motorista-routing.module.ts ***!
      \*********************************************************************/

    /*! exports provided: EditarMotoristaPageRoutingModule */

    /***/
    function srcAppEditarMotoristaEditarMotoristaRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditarMotoristaPageRoutingModule", function () {
        return EditarMotoristaPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _editar_motorista_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./editar-motorista.page */
      "./src/app/editar-motorista/editar-motorista.page.ts");

      var routes = [{
        path: '',
        component: _editar_motorista_page__WEBPACK_IMPORTED_MODULE_3__["EditarMotoristaPage"]
      }];

      var EditarMotoristaPageRoutingModule = function EditarMotoristaPageRoutingModule() {
        _classCallCheck(this, EditarMotoristaPageRoutingModule);
      };

      EditarMotoristaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], EditarMotoristaPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/editar-motorista/editar-motorista.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/editar-motorista/editar-motorista.module.ts ***!
      \*************************************************************/

    /*! exports provided: EditarMotoristaPageModule */

    /***/
    function srcAppEditarMotoristaEditarMotoristaModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditarMotoristaPageModule", function () {
        return EditarMotoristaPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _editar_motorista_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./editar-motorista-routing.module */
      "./src/app/editar-motorista/editar-motorista-routing.module.ts");
      /* harmony import */


      var _editar_motorista_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./editar-motorista.page */
      "./src/app/editar-motorista/editar-motorista.page.ts");

      var EditarMotoristaPageModule = function EditarMotoristaPageModule() {
        _classCallCheck(this, EditarMotoristaPageModule);
      };

      EditarMotoristaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _editar_motorista_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditarMotoristaPageRoutingModule"]],
        declarations: [_editar_motorista_page__WEBPACK_IMPORTED_MODULE_6__["EditarMotoristaPage"]]
      })], EditarMotoristaPageModule);
      /***/
    },

    /***/
    "./src/app/editar-motorista/editar-motorista.page.scss":
    /*!*************************************************************!*\
      !*** ./src/app/editar-motorista/editar-motorista.page.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppEditarMotoristaEditarMotoristaPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "img {\n  border-radius: 5%;\n  max-width: 85%;\n}\n\nion-toolbar {\n  --background: #001c4e;\n}\n\nion-grid {\n  display: inline-block;\n  margin: auto;\n  text-align: center;\n  width: 100%;\n  border: 1px solid;\n  border-left: none;\n  border-right: none;\n}\n\nion-title {\n  text-align: center;\n  color: #001c4e;\n  font-size: 1.5em;\n  display: block;\n}\n\nion-item {\n  --highlight-height: 1px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZWRpdGFyLW1vdG9yaXN0YS9lZGl0YXItbW90b3Jpc3RhLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBRUEsY0FBQTtBQUFKOztBQUlBO0VBQ0kscUJBQUE7QUFESjs7QUFJQTtFQUNJLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQURKOztBQUlBO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FBREo7O0FBS0E7RUFDSSx1QkFBQTtBQUZKIiwiZmlsZSI6InNyYy9hcHAvZWRpdGFyLW1vdG9yaXN0YS9lZGl0YXItbW90b3Jpc3RhLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImltZyB7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1JTtcclxuICAgIC8vYm9yZGVyOiAjMDAxYzRlIDAuNXB4IHNvbGlkO1xyXG4gICAgbWF4LXdpZHRoOiA4NSU7XHJcbiAgICBcclxufVxyXG5cclxuaW9uLXRvb2xiYXIge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAxYzRlO1xyXG59IFxyXG5cclxuaW9uLWdyaWQge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIGJvcmRlci1sZWZ0OiBub25lO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiBub25lO1xyXG59XHJcblxyXG5pb24tdGl0bGUge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyOyBcclxuICAgIGNvbG9yOiAjMDAxYzRlO1xyXG4gICAgZm9udC1zaXplOiAxLjVlbTtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG5cclxuaW9uLWl0ZW17XHJcbiAgICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDFweDtcclxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/editar-motorista/editar-motorista.page.ts":
    /*!***********************************************************!*\
      !*** ./src/app/editar-motorista/editar-motorista.page.ts ***!
      \***********************************************************/

    /*! exports provided: EditarMotoristaPage */

    /***/
    function srcAppEditarMotoristaEditarMotoristaPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditarMotoristaPage", function () {
        return EditarMotoristaPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/core/user/user.service */
      "./src/core/user/user.service.ts");
      /* harmony import */


      var src_servico_editar_motorista__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/servico/editar-motorista */
      "./src/servico/editar-motorista.ts");
      /* harmony import */


      var src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/servico/motorista_liberacao */
      "./src/servico/motorista_liberacao.ts");

      var EditarMotoristaPage = /*#__PURE__*/function () {
        function EditarMotoristaPage(router, route, motoristaService, motoristaLiberacaoService, toastCtrl, editarService) {
          var _this = this;

          _classCallCheck(this, EditarMotoristaPage);

          this.router = router;
          this.route = route;
          this.motoristaService = motoristaService;
          this.motoristaLiberacaoService = motoristaLiberacaoService;
          this.toastCtrl = toastCtrl;
          this.editarService = editarService;
          this.apelido = ' - ';
          this.url_foto = 'assets/img/no.png';
          this.url_crlv = 'assets/img/no.png';
          this.url_cnh = 'assets/img/no.png';
          this.nomeNovo = '';
          this.apelidoNovo = '';
          this.phoneNovo = '';
          this.emailNovo = '';
          this.senhaNovo = '';
          this.cidadeNovo = '';
          this.placaNovo = '';
          this.modeloNovo = '';
          this.user$ = motoristaService.getUser();
          this.user$.subscribe(function (usuario) {
            _this.usuarioLogado = usuario;
            _this.user_id = _this.usuarioLogado.id;

            var a = _this.usuarioLogado.full_name.split(' ');

            if (_this.usuarioLogado.apelido == null) {
              _this.apelido = '';
            } else {
              _this.apelido = '(' + _this.usuarioLogado.apelido + ')';
            }

            _this.nome = a[0] + ' ' + _this.apelido;
          });
          this.route.params.subscribe(function (parametros) {
            _this.motorista_id = parametros['user_id'];

            _this.motoristaLiberacaoService.motoristaLiberadoUnico(_this.motorista_id).subscribe(function (motorista) {
              _this.mot = motorista;
              _this.nome = motorista.full_name;
              _this.phone = _this.mot.phone;
              _this.email = _this.mot.email;
              _this.password = _this.mot.password;
              _this.cidade = _this.mot.cidade;
              _this.apelido = _this.mot.apelido;
              _this.placa = _this.mot.placa;
              _this.modelo = _this.mot.modelo;
              _this.motId = _this.mot.id;

              _this.motoristaLiberacaoService.fotoMotorista(_this.mot.phone).subscribe(function (foto_motorista) {
                _this.photo = foto_motorista;
                _this.url_foto = "https://uaileva.com.br/api/imgs/" + _this.photo.url;
              });

              _this.motoristaLiberacaoService.fotoCrlv(_this.mot.phone).subscribe(function (foto_crlv) {
                _this.photo_crlv = foto_crlv;
                _this.url_crlv = "https://uaileva.com.br/api/imgs/" + _this.photo_crlv.url;
              });

              _this.motoristaLiberacaoService.fotoCnh(_this.mot.phone).subscribe(function (foto_cnh) {
                _this.photo_cnh = foto_cnh;
                _this.url_cnh = "https://uaileva.com.br/api/imgs/" + _this.photo_cnh.url;
              });
            });
          });
        }

        _createClass(EditarMotoristaPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "salvar",
          value: function salvar() {
            var _this2 = this;

            if (this.nomeNovo != '') {
              console.log("" + this.nomeNovo);
              this.editarService.updateNome(this.motorista_id, this.nomeNovo, '').subscribe(function () {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          _context.next = 2;
                          return this.toastCtrl.create({
                            message: 'Nome editado com sucesso!',
                            duration: 4000,
                            position: 'top',
                            color: 'success'
                          });

                        case 2:
                          _context.next = 4;
                          return _context.sent.present();

                        case 4:
                          toast = _context.sent;

                        case 5:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee, this);
                }));
              }, function (err) {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee2$(_context2) {
                    while (1) {
                      switch (_context2.prev = _context2.next) {
                        case 0:
                          _context2.next = 2;
                          return this.toastCtrl.create({
                            message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                            duration: 4000,
                            position: 'top',
                            color: 'danger'
                          });

                        case 2:
                          _context2.next = 4;
                          return _context2.sent.present();

                        case 4:
                          toast = _context2.sent;

                        case 5:
                        case "end":
                          return _context2.stop();
                      }
                    }
                  }, _callee2, this);
                }));
              });
            }

            if (this.apelidoNovo != '') {
              console.log("" + this.apelidoNovo);
              this.editarService.apelido(this.motorista_id, this.apelidoNovo).subscribe(function () {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                      switch (_context3.prev = _context3.next) {
                        case 0:
                          _context3.next = 2;
                          return this.toastCtrl.create({
                            message: 'Apelido editado com sucesso!',
                            duration: 4000,
                            position: 'top',
                            color: 'success'
                          });

                        case 2:
                          _context3.next = 4;
                          return _context3.sent.present();

                        case 4:
                          toast = _context3.sent;

                        case 5:
                        case "end":
                          return _context3.stop();
                      }
                    }
                  }, _callee3, this);
                }));
              }, function (err) {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee4$(_context4) {
                    while (1) {
                      switch (_context4.prev = _context4.next) {
                        case 0:
                          _context4.next = 2;
                          return this.toastCtrl.create({
                            message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                            duration: 4000,
                            position: 'top',
                            color: 'danger'
                          });

                        case 2:
                          _context4.next = 4;
                          return _context4.sent.present();

                        case 4:
                          toast = _context4.sent;

                        case 5:
                        case "end":
                          return _context4.stop();
                      }
                    }
                  }, _callee4, this);
                }));
              });
            }

            if (this.phoneNovo != '') {
              console.log("" + this.phoneNovo);
              this.editarService.phone(this.motorista_id, this.phoneNovo).subscribe(function () {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee5$(_context5) {
                    while (1) {
                      switch (_context5.prev = _context5.next) {
                        case 0:
                          _context5.next = 2;
                          return this.toastCtrl.create({
                            message: 'Phone editada com sucesso!',
                            duration: 4000,
                            position: 'top',
                            color: 'success'
                          });

                        case 2:
                          _context5.next = 4;
                          return _context5.sent.present();

                        case 4:
                          toast = _context5.sent;

                        case 5:
                        case "end":
                          return _context5.stop();
                      }
                    }
                  }, _callee5, this);
                }));
              }, function (err) {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee6$(_context6) {
                    while (1) {
                      switch (_context6.prev = _context6.next) {
                        case 0:
                          _context6.next = 2;
                          return this.toastCtrl.create({
                            message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                            duration: 4000,
                            position: 'top',
                            color: 'danger'
                          });

                        case 2:
                          _context6.next = 4;
                          return _context6.sent.present();

                        case 4:
                          toast = _context6.sent;

                        case 5:
                        case "end":
                          return _context6.stop();
                      }
                    }
                  }, _callee6, this);
                }));
              });
            }

            if (this.emailNovo != '') {
              console.log("" + this.emailNovo);
              this.editarService.email(this.motorista_id, this.emailNovo).subscribe(function () {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee7$(_context7) {
                    while (1) {
                      switch (_context7.prev = _context7.next) {
                        case 0:
                          _context7.next = 2;
                          return this.toastCtrl.create({
                            message: 'Email editado com sucesso!',
                            duration: 4000,
                            position: 'top',
                            color: 'success'
                          });

                        case 2:
                          _context7.next = 4;
                          return _context7.sent.present();

                        case 4:
                          toast = _context7.sent;

                        case 5:
                        case "end":
                          return _context7.stop();
                      }
                    }
                  }, _callee7, this);
                }));
              }, function (err) {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee8$(_context8) {
                    while (1) {
                      switch (_context8.prev = _context8.next) {
                        case 0:
                          _context8.next = 2;
                          return this.toastCtrl.create({
                            message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                            duration: 4000,
                            position: 'top',
                            color: 'danger'
                          });

                        case 2:
                          _context8.next = 4;
                          return _context8.sent.present();

                        case 4:
                          toast = _context8.sent;

                        case 5:
                        case "end":
                          return _context8.stop();
                      }
                    }
                  }, _callee8, this);
                }));
              });
            }

            if (this.senhaNovo != '') {
              console.log("" + this.senhaNovo);
              this.editarService.updatePassword(this.motorista_id, this.senhaNovo).subscribe(function () {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee9$(_context9) {
                    while (1) {
                      switch (_context9.prev = _context9.next) {
                        case 0:
                          _context9.next = 2;
                          return this.toastCtrl.create({
                            message: 'Senha editado com sucesso!',
                            duration: 4000,
                            position: 'top',
                            color: 'success'
                          });

                        case 2:
                          _context9.next = 4;
                          return _context9.sent.present();

                        case 4:
                          toast = _context9.sent;

                        case 5:
                        case "end":
                          return _context9.stop();
                      }
                    }
                  }, _callee9, this);
                }));
              }, function (err) {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee10$(_context10) {
                    while (1) {
                      switch (_context10.prev = _context10.next) {
                        case 0:
                          _context10.next = 2;
                          return this.toastCtrl.create({
                            message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                            duration: 4000,
                            position: 'top',
                            color: 'danger'
                          });

                        case 2:
                          _context10.next = 4;
                          return _context10.sent.present();

                        case 4:
                          toast = _context10.sent;

                        case 5:
                        case "end":
                          return _context10.stop();
                      }
                    }
                  }, _callee10, this);
                }));
              });
            }

            if (this.cidadeNovo != '') {
              console.log("" + this.cidadeNovo);
              this.editarService.cidade(this.motorista_id, this.cidadeNovo).subscribe(function () {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee11$(_context11) {
                    while (1) {
                      switch (_context11.prev = _context11.next) {
                        case 0:
                          _context11.next = 2;
                          return this.toastCtrl.create({
                            message: 'Cidade editada com sucesso!',
                            duration: 4000,
                            position: 'top',
                            color: 'success'
                          });

                        case 2:
                          _context11.next = 4;
                          return _context11.sent.present();

                        case 4:
                          toast = _context11.sent;

                        case 5:
                        case "end":
                          return _context11.stop();
                      }
                    }
                  }, _callee11, this);
                }));
              }, function (err) {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee12$(_context12) {
                    while (1) {
                      switch (_context12.prev = _context12.next) {
                        case 0:
                          _context12.next = 2;
                          return this.toastCtrl.create({
                            message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                            duration: 4000,
                            position: 'top',
                            color: 'danger'
                          });

                        case 2:
                          _context12.next = 4;
                          return _context12.sent.present();

                        case 4:
                          toast = _context12.sent;

                        case 5:
                        case "end":
                          return _context12.stop();
                      }
                    }
                  }, _callee12, this);
                }));
              });
            }

            if (this.placaNovo != '' || this.modeloNovo != '' || this.placaNovo != undefined || this.placaNovo != null || this.modeloNovo != null || this.modeloNovo != undefined) {
              console.log("" + this.placaNovo);

              if (this.modeloNovo == '') {
                this.modeloNovo = this.modelo;
              }

              if (this.placaNovo == '') {
                this.placaNovo = this.placa;
              }

              this.editarService.placa(this.motorista_id, this.placaNovo, this.modeloNovo).subscribe(function () {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee13() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee13$(_context13) {
                    while (1) {
                      switch (_context13.prev = _context13.next) {
                        case 0:
                          _context13.next = 2;
                          return this.toastCtrl.create({
                            message: 'Placa do carro editado com sucesso!',
                            duration: 4000,
                            position: 'top',
                            color: 'success'
                          });

                        case 2:
                          _context13.next = 4;
                          return _context13.sent.present();

                        case 4:
                          toast = _context13.sent;

                        case 5:
                        case "end":
                          return _context13.stop();
                      }
                    }
                  }, _callee13, this);
                }));
              }, function (err) {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee14() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee14$(_context14) {
                    while (1) {
                      switch (_context14.prev = _context14.next) {
                        case 0:
                          _context14.next = 2;
                          return this.toastCtrl.create({
                            message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                            duration: 4000,
                            position: 'top',
                            color: 'danger'
                          });

                        case 2:
                          _context14.next = 4;
                          return _context14.sent.present();

                        case 4:
                          toast = _context14.sent;

                        case 5:
                        case "end":
                          return _context14.stop();
                      }
                    }
                  }, _callee14, this);
                }));
              });
            }

            if (this.file != undefined) {
              console.log(this.file);
              var allowComments = true;
              var description = this.phone + '/ USER / PEFIL';
              this.editarService.updateFoto(description, allowComments, this.file).subscribe(function () {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee15() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee15$(_context15) {
                    while (1) {
                      switch (_context15.prev = _context15.next) {
                        case 0:
                          _context15.next = 2;
                          return this.toastCtrl.create({
                            message: 'Foto editada com sucesso!',
                            duration: 4000,
                            position: 'top',
                            color: 'success'
                          });

                        case 2:
                          _context15.next = 4;
                          return _context15.sent.present();

                        case 4:
                          toast = _context15.sent;

                        case 5:
                        case "end":
                          return _context15.stop();
                      }
                    }
                  }, _callee15, this);
                }));
              }, function (err) {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee16() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee16$(_context16) {
                    while (1) {
                      switch (_context16.prev = _context16.next) {
                        case 0:
                          _context16.next = 2;
                          return this.toastCtrl.create({
                            message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                            duration: 4000,
                            position: 'top',
                            color: 'danger'
                          });

                        case 2:
                          _context16.next = 4;
                          return _context16.sent.present();

                        case 4:
                          toast = _context16.sent;

                        case 5:
                        case "end":
                          return _context16.stop();
                      }
                    }
                  }, _callee16, this);
                }));
              });
            }

            console.log(this.phone);

            if (this.fileCNH != undefined) {
              console.log(this.fileCNH);
              var _allowComments = true;

              var _description = this.phone + '/ MOTORISTA / CNH';

              this.editarService.updateFoto(_description, _allowComments, this.file).subscribe(function () {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee17() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee17$(_context17) {
                    while (1) {
                      switch (_context17.prev = _context17.next) {
                        case 0:
                          _context17.next = 2;
                          return this.toastCtrl.create({
                            message: 'Foto editada com sucesso!',
                            duration: 4000,
                            position: 'top',
                            color: 'success'
                          });

                        case 2:
                          _context17.next = 4;
                          return _context17.sent.present();

                        case 4:
                          toast = _context17.sent;

                        case 5:
                        case "end":
                          return _context17.stop();
                      }
                    }
                  }, _callee17, this);
                }));
              }, function (err) {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee18() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee18$(_context18) {
                    while (1) {
                      switch (_context18.prev = _context18.next) {
                        case 0:
                          _context18.next = 2;
                          return this.toastCtrl.create({
                            message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                            duration: 4000,
                            position: 'top',
                            color: 'danger'
                          });

                        case 2:
                          _context18.next = 4;
                          return _context18.sent.present();

                        case 4:
                          toast = _context18.sent;

                        case 5:
                        case "end":
                          return _context18.stop();
                      }
                    }
                  }, _callee18, this);
                }));
              });
            }

            if (this.fileCRLV != undefined) {
              console.log(this.fileCRLV);
              var _allowComments2 = true;

              var _description2 = this.phone + '/ MOTORISTA / CRLV';

              this.editarService.updateFoto(_description2, _allowComments2, this.file).subscribe(function () {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee19() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee19$(_context19) {
                    while (1) {
                      switch (_context19.prev = _context19.next) {
                        case 0:
                          _context19.next = 2;
                          return this.toastCtrl.create({
                            message: 'Foto editada com sucesso!',
                            duration: 4000,
                            position: 'top',
                            color: 'success'
                          });

                        case 2:
                          _context19.next = 4;
                          return _context19.sent.present();

                        case 4:
                          toast = _context19.sent;

                        case 5:
                        case "end":
                          return _context19.stop();
                      }
                    }
                  }, _callee19, this);
                }));
              }, function (err) {
                return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee20() {
                  var toast;
                  return regeneratorRuntime.wrap(function _callee20$(_context20) {
                    while (1) {
                      switch (_context20.prev = _context20.next) {
                        case 0:
                          _context20.next = 2;
                          return this.toastCtrl.create({
                            message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                            duration: 4000,
                            position: 'top',
                            color: 'danger'
                          });

                        case 2:
                          _context20.next = 4;
                          return _context20.sent.present();

                        case 4:
                          toast = _context20.sent;

                        case 5:
                        case "end":
                          return _context20.stop();
                      }
                    }
                  }, _callee20, this);
                }));
              });
            }
          }
        }]);

        return EditarMotoristaPage;
      }();

      EditarMotoristaPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: src_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]
        }, {
          type: src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_6__["MotoristaLiberacaoService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
        }, {
          type: src_servico_editar_motorista__WEBPACK_IMPORTED_MODULE_5__["EditarPerfilService"]
        }];
      };

      EditarMotoristaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-editar-motorista',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./editar-motorista.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/editar-motorista/editar-motorista.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./editar-motorista.page.scss */
        "./src/app/editar-motorista/editar-motorista.page.scss"))["default"]]
      })], EditarMotoristaPage);
      /***/
    },

    /***/
    "./src/core/token/token.service.ts":
    /*!*****************************************!*\
      !*** ./src/core/token/token.service.ts ***!
      \*****************************************/

    /*! exports provided: TokenService */

    /***/
    function srcCoreTokenTokenServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TokenService", function () {
        return TokenService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var KEY = 'authToken';

      var TokenService = /*#__PURE__*/function () {
        function TokenService() {
          _classCallCheck(this, TokenService);
        }

        _createClass(TokenService, [{
          key: "hasToken",
          value: function hasToken() {
            return !!this.getToken();
          }
        }, {
          key: "setToken",
          value: function setToken(token) {
            window.localStorage.setItem(KEY, token);
          }
        }, {
          key: "getToken",
          value: function getToken() {
            return window.localStorage.getItem(KEY);
          }
        }, {
          key: "removeToken",
          value: function removeToken() {
            window.localStorage.removeItem(KEY);
          }
        }]);

        return TokenService;
      }();

      TokenService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], TokenService);
      /***/
    },

    /***/
    "./src/core/user/user.service.ts":
    /*!***************************************!*\
      !*** ./src/core/user/user.service.ts ***!
      \***************************************/

    /*! exports provided: UserService */

    /***/
    function srcCoreUserUserServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserService", function () {
        return UserService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _token_token_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../token/token.service */
      "./src/core/token/token.service.ts");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var jwt_decode__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! jwt-decode */
      "./node_modules/jwt-decode/build/jwt-decode.esm.js"); //import * as jwt_decode from 'jwt-decode';


      var UserService = /*#__PURE__*/function () {
        function UserService(tokenService) {
          _classCallCheck(this, UserService);

          this.tokenService = tokenService;
          this.userSubject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](null);
          this.tokenService.hasToken() && this.decodeAndNotify();
        }

        _createClass(UserService, [{
          key: "setToken",
          value: function setToken(token) {
            this.tokenService.setToken(token);
            this.decodeAndNotify();
          }
        }, {
          key: "getUser",
          value: function getUser() {
            return this.userSubject.asObservable();
          }
        }, {
          key: "decodeAndNotify",
          value: function decodeAndNotify() {
            var token = this.tokenService.getToken();
            var user = Object(jwt_decode__WEBPACK_IMPORTED_MODULE_4__["default"])(token);
            this.fullName = user.full_name;
            this.userSubject.next(user);
          }
        }, {
          key: "logout",
          value: function logout() {
            this.tokenService.removeToken();
            this.userSubject.next(null);
          }
        }, {
          key: "isLogged",
          value: function isLogged() {
            return this.tokenService.hasToken();
          }
        }, {
          key: "getUserName",
          value: function getUserName() {
            return this.fullName;
          }
        }]);

        return UserService;
      }();

      UserService.ctorParameters = function () {
        return [{
          type: _token_token_service__WEBPACK_IMPORTED_MODULE_2__["TokenService"]
        }];
      };

      UserService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], UserService);
      /***/
    },

    /***/
    "./src/servico/editar-motorista.ts":
    /*!*****************************************!*\
      !*** ./src/servico/editar-motorista.ts ***!
      \*****************************************/

    /*! exports provided: EditarPerfilService */

    /***/
    function srcServicoEditarMotoristaTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditarPerfilService", function () {
        return EditarPerfilService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var API_URL = "https://uaileva.com.br/api"; //const API_URL = "http://localhost:3000"; 

      var EditarPerfilService = /*#__PURE__*/function () {
        function EditarPerfilService(http) {
          _classCallCheck(this, EditarPerfilService);

          this.http = http;
        }

        _createClass(EditarPerfilService, [{
          key: "updateNome",
          value: function updateNome(user_id, nome, secondName) {
            return this.http.post("".concat(API_URL, "/motorista/updateNome"), {
              user_id: user_id,
              nome: nome,
              secondName: secondName
            });
          }
        }, {
          key: "updatePassword",
          value: function updatePassword(user_id, password) {
            return this.http.post("".concat(API_URL, "/motorista/updatePassword"), {
              user_id: user_id,
              password: password
            });
          }
        }, {
          key: "apelido",
          value: function apelido(user_id, _apelido) {
            return this.http.post("".concat(API_URL, "/motorista/updateApelido"), {
              user_id: user_id,
              apelido: _apelido
            });
          }
        }, {
          key: "placa",
          value: function placa(motorista_id, _placa, modelo) {
            console.log(_placa);
            console.log(modelo);
            console.log(motorista_id);
            return this.http.get("".concat(API_URL, "/carro/update/") + motorista_id + "/" + _placa + "/" + modelo + "");
          }
        }, {
          key: "modelo",
          value: function modelo(motorista_id, placa, _modelo) {
            console.log(placa);
            console.log(_modelo);
            return this.http.get("".concat(API_URL, "/carro/update/") + motorista_id + "/" + placa + "/" + _modelo + "");
          }
        }, {
          key: "cidade",
          value: function cidade(motorista_id, _cidade) {
            return this.http.get("".concat(API_URL, "/motorista/cidade/") + motorista_id + "/" + _cidade + "");
          }
        }, {
          key: "phone",
          value: function phone(motorista_id, _phone) {
            return this.http.get("".concat(API_URL, "/motorista/phone/") + motorista_id + "/" + _phone + "");
          }
        }, {
          key: "email",
          value: function email(motorista_id, _email) {
            return this.http.get("".concat(API_URL, "/motorista/email/") + motorista_id + "/" + _email + "");
          }
        }, {
          key: "updateFoto",
          value: function updateFoto(description, allowComments, file) {
            {
              var formData = new FormData();
              formData.append('description', description);
              formData.append('allowComments', allowComments ? 'true' : 'false');
              formData.append('imageFile', file);
              return this.http.post(API_URL + '/photos/update', formData);
            }
          }
        }]);

        return EditarPerfilService;
      }();

      EditarPerfilService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      EditarPerfilService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], EditarPerfilService);
      /***/
    },

    /***/
    "./src/servico/motorista_liberacao.ts":
    /*!********************************************!*\
      !*** ./src/servico/motorista_liberacao.ts ***!
      \********************************************/

    /*! exports provided: MotoristaLiberacaoService */

    /***/
    function srcServicoMotorista_liberacaoTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MotoristaLiberacaoService", function () {
        return MotoristaLiberacaoService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var API_URL = "https://uaileva.com.br/api";

      var MotoristaLiberacaoService = /*#__PURE__*/function () {
        function MotoristaLiberacaoService(http) {
          _classCallCheck(this, MotoristaLiberacaoService);

          this.http = http;
        }

        _createClass(MotoristaLiberacaoService, [{
          key: "liberar",
          value: function liberar() {
            return this.http.get(API_URL + "/motorista/liberacao");
          }
        }, {
          key: "motoristaUnico",
          value: function motoristaUnico(motorista_id) {
            return this.http.get(API_URL + "/motorista/unico/" + motorista_id);
          }
        }, {
          key: "fotoMotorista",
          value: function fotoMotorista(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/" + mot_phone);
          }
        }, {
          key: "fotoCrlv",
          value: function fotoCrlv(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/crlv/" + mot_phone);
          }
        }, {
          key: "fotoCnh",
          value: function fotoCnh(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/cnh/" + mot_phone);
          }
        }, {
          key: "carro",
          value: function carro(id_motorista, placa, modelo) {
            return this.http.get(API_URL + "/carro/signup/" + id_motorista + "/" + placa + "/" + modelo + "");
          }
        }, {
          key: "updateNome",
          value: function updateNome(user_id, nome, secondName) {
            return this.http.post("".concat(API_URL, "/motorista/updateNome"), {
              user_id: user_id,
              nome: nome,
              secondName: secondName
            });
          }
        }, {
          key: "liberarMotorista",
          value: function liberarMotorista(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/LIBERADO");
          }
        }, {
          key: "motoristaLiberado",
          value: function motoristaLiberado() {
            return this.http.get(API_URL + "/motoristaLiberado");
          }
        }, {
          key: "bloqueio",
          value: function bloqueio(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/BLOQUEADO");
          }
        }, {
          key: "bloqueioPassageiro",
          value: function bloqueioPassageiro(phone) {
            return this.http.get(API_URL + "/user/bloqueio/" + phone + "/BLOQUEADO");
          }
        }, {
          key: "corridaCM",
          value: function corridaCM(data) {
            console.log(data);
            return this.http.get(API_URL + "/corrida/diariacm/" + data + "");
          }
        }, {
          key: "corridaCP",
          value: function corridaCP(data) {
            return this.http.get(API_URL + "/corrida/diariacp/" + data + "");
          }
        }, {
          key: "corridaF",
          value: function corridaF(data) {
            return this.http.get(API_URL + "/corrida/diariaf/" + data + "");
          }
        }, {
          key: "corridaTotal",
          value: function corridaTotal() {
            return this.http.get(API_URL + "/totalCorridas");
          }
        }, {
          key: "motoristaLiberadoUnico",
          value: function motoristaLiberadoUnico(motorista_id) {
            return this.http.get(API_URL + "/motorista/liberadoUnico/" + motorista_id);
          }
        }, {
          key: "arrecadacao",
          value: function arrecadacao(motorista_id) {
            return this.http.get(API_URL + "/totalarrecadado/" + motorista_id);
          }
        }, {
          key: "desbloqueio",
          value: function desbloqueio(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/LIBERADO");
          }
        }, {
          key: "motbloqueado",
          value: function motbloqueado() {
            return this.http.get(API_URL + "/motorista/bloqueado");
          }
        }]);

        return MotoristaLiberacaoService;
      }();

      MotoristaLiberacaoService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      MotoristaLiberacaoService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], MotoristaLiberacaoService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=editar-motorista-editar-motorista-module-es5.js.map
(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["download-download-module"], {
    /***/
    "./node_modules/@ionic-native/downloader/__ivy_ngcc__/index.js":
    /*!*********************************************************************!*\
      !*** ./node_modules/@ionic-native/downloader/__ivy_ngcc__/index.js ***!
      \*********************************************************************/

    /*! exports provided: NotificationVisibility, Downloader */

    /***/
    function node_modulesIonicNativeDownloader__ivy_ngcc__IndexJs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NotificationVisibility", function () {
        return NotificationVisibility;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Downloader", function () {
        return Downloader;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_native_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @ionic-native/core */
      "./node_modules/@ionic-native/core/__ivy_ngcc__/index.js");

      var __extends = undefined && undefined.__extends || function () {
        var extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function (d, b) {
          d.__proto__ = b;
        } || function (d, b) {
          for (var p in b) {
            if (b.hasOwnProperty(p)) d[p] = b[p];
          }
        };

        return function (d, b) {
          extendStatics(d, b);

          function __() {
            this.constructor = d;
          }

          d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
      }();

      var __decorate = undefined && undefined.__decorate || function (decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
            d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
          if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        }
        return c > 3 && r && Object.defineProperty(target, key, r), r;
      };

      var __metadata = undefined && undefined.__metadata || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
      };

      var NotificationVisibility;

      (function (NotificationVisibility) {
        NotificationVisibility[NotificationVisibility["Visible"] = 0] = "Visible";
        NotificationVisibility[NotificationVisibility["VisibleNotifyCompleted"] = 1] = "VisibleNotifyCompleted";
        NotificationVisibility[NotificationVisibility["VisibilityHidden"] = 2] = "VisibilityHidden";
        NotificationVisibility[NotificationVisibility["VisibleNotifyOnlyCompletion"] = 3] = "VisibleNotifyOnlyCompletion";
      })(NotificationVisibility || (NotificationVisibility = {}));
      /**
       * @name Downloader
       * @description
       * This plugin is designed to support downloading files using Android DownloadManager.
       *
       *
       * @usage
       * ```typescript
       * import { Downloader } from '@ionic-native/downloader/ngx';
       *
       *
       * constructor(private downloader: Downloader) { }
       *
       * ...
       *
       *    var request: DownloadRequest = {
       *           uri: YOUR_URI,
       *           title: 'MyDownload',
       *           description: '',
       *           mimeType: '',
       *           visibleInDownloadsUi: true,
       *           notificationVisibility: NotificationVisibility.VisibleNotifyCompleted,
       *           destinationInExternalFilesDir: {
       *               dirType: 'Downloads',
       *               subPath: 'MyFile.apk'
       *           }
       *       };
       *
       *
       *   this.downloader.download(request)
       *   			.then((location: string) => console.log('File downloaded at:'+location))
       *   			.catch((error: any) => console.error(error));
       *
       * ```
       * @interfaces
       * NotificationVisibility
       * Header
       * DestinationDirectory
       * DownloadHttpHeader
       */


      var Downloader = function (_super) {
        __extends(Downloader, _super);

        function Downloader() {
          return _super !== null && _super.apply(this, arguments) || this;
        }
        /**
         *  Starts a new download and returns location of the downloaded file on completion
         *  @param request {DownloadRequest}
         */

        /**
           *  Starts a new download and returns location of the downloaded file on completion
           *  @param request {DownloadRequest}
           */


        Downloader.prototype.download =
        /**
        *  Starts a new download and returns location of the downloaded file on completion
        *  @param request {DownloadRequest}
        */
        function (request) {
          return;
        };

        __decorate([Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["Cordova"])(), __metadata("design:type", Function), __metadata("design:paramtypes", [Object]), __metadata("design:returntype", Promise)], Downloader.prototype, "download", null);
        /**
         * @name Downloader
         * @description
         * This plugin is designed to support downloading files using Android DownloadManager.
         *
         *
         * @usage
         * ```typescript
         * import { Downloader } from '@ionic-native/downloader/ngx';
         *
         *
         * constructor(private downloader: Downloader) { }
         *
         * ...
         *
         *    var request: DownloadRequest = {
         *           uri: YOUR_URI,
         *           title: 'MyDownload',
         *           description: '',
         *           mimeType: '',
         *           visibleInDownloadsUi: true,
         *           notificationVisibility: NotificationVisibility.VisibleNotifyCompleted,
         *           destinationInExternalFilesDir: {
         *               dirType: 'Downloads',
         *               subPath: 'MyFile.apk'
         *           }
         *       };
         *
         *
         *   this.downloader.download(request)
         *   			.then((location: string) => console.log('File downloaded at:'+location))
         *   			.catch((error: any) => console.error(error));
         *
         * ```
         * @interfaces
         * NotificationVisibility
         * Header
         * DestinationDirectory
         * DownloadHttpHeader
         */


        Downloader = __decorate([Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["Plugin"])({
          pluginName: 'Downloader',
          plugin: 'integrator-cordova-plugin-downloader',
          pluginRef: 'cordova.plugins.Downloader',
          repo: 'https://github.com/Luka313/integrator-cordova-plugin-downloader.git',
          platforms: ['Android']
        })], Downloader);

        Downloader.ɵfac = function Downloader_Factory(t) {
          return ɵDownloader_BaseFactory(t || Downloader);
        };

        Downloader.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
          token: Downloader,
          factory: function factory(t) {
            return Downloader.ɵfac(t);
          }
        });

        var ɵDownloader_BaseFactory = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetInheritedFactory"](Downloader);
        /*@__PURE__*/


        (function () {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](Downloader, [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
          }], null, null);
        })();

        return Downloader;
      }(_ionic_native_core__WEBPACK_IMPORTED_MODULE_1__["IonicNativePlugin"]); //# sourceMappingURL=index.js.map

      /***/

    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/download/download.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/download/download.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppDownloadDownloadPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"home\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <h1>Faça Download do seu APP!</h1>\r\n\r\n  <br>\r\n  <button class=\"botaoDownload\" tappable>Clique para baixar</button>\r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "./src/app/download/download-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/download/download-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: DownloadPageRoutingModule */

    /***/
    function srcAppDownloadDownloadRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DownloadPageRoutingModule", function () {
        return DownloadPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _download_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./download.page */
      "./src/app/download/download.page.ts");

      var routes = [{
        path: '',
        component: _download_page__WEBPACK_IMPORTED_MODULE_3__["DownloadPage"]
      }];

      var DownloadPageRoutingModule = function DownloadPageRoutingModule() {
        _classCallCheck(this, DownloadPageRoutingModule);
      };

      DownloadPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], DownloadPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/download/download.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/download/download.module.ts ***!
      \*********************************************/

    /*! exports provided: DownloadPageModule */

    /***/
    function srcAppDownloadDownloadModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DownloadPageModule", function () {
        return DownloadPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _download_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./download-routing.module */
      "./src/app/download/download-routing.module.ts");
      /* harmony import */


      var _download_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./download.page */
      "./src/app/download/download.page.ts");

      var DownloadPageModule = function DownloadPageModule() {
        _classCallCheck(this, DownloadPageModule);
      };

      DownloadPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _download_routing_module__WEBPACK_IMPORTED_MODULE_5__["DownloadPageRoutingModule"]],
        declarations: [_download_page__WEBPACK_IMPORTED_MODULE_6__["DownloadPage"]]
      })], DownloadPageModule);
      /***/
    },

    /***/
    "./src/app/download/download.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/download/download.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppDownloadDownloadPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-card, img {\n  border-radius: 5%;\n  border: #001c4e 0.5px solid;\n}\n\nion-toolbar {\n  --background: #001c4e;\n}\n\nion-title {\n  color: #fff;\n}\n\nion-card-title, ion-card-subtitle, ion-card-content {\n  text-align: center;\n}\n\nion-button {\n  --background: #001c4e;\n}\n\nion-label {\n  text-transform: none;\n}\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 20px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n\n.botaoDownload {\n  background: #001c4e;\n  border-radius: 7px;\n  outline: none;\n  box-shadow: 0 0 0 0;\n  color: white;\n  font-size: large;\n  height: 55px;\n  width: 190px;\n  text-align: center;\n  margin: auto;\n  display: block;\n}\n\n.baixar {\n  margin: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZG93bmxvYWQvZG93bmxvYWQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSwyQkFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQkFBQTtBQUNKOztBQUVBO0VBQ0ksb0JBQUE7QUFDSjs7QUFFQTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUFDSjs7QUFHQTtFQUNJLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7QUFBSjs7QUFHQTtFQUNJLFlBQUE7QUFBSiIsImZpbGUiOiJzcmMvYXBwL2Rvd25sb2FkL2Rvd25sb2FkLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkLCBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNSU7XHJcbiAgICBib3JkZXI6ICMwMDFjNGUgMC41cHggc29saWQ7XHJcbn1cclxuXHJcbmlvbi10b29sYmFyIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzAwMWM0ZTtcclxufVxyXG5cclxuaW9uLXRpdGxlIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG59XHJcblxyXG5pb24tY2FyZC10aXRsZSwgaW9uLWNhcmQtc3VidGl0bGUsIGlvbi1jYXJkLWNvbnRlbnQge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG5pb24tYnV0dG9uIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzAwMWM0ZTtcclxufVxyXG5cclxuaW9uLWxhYmVsIHtcclxuICAgIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG59XHJcblxyXG5oMSB7XHJcbiAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBtYXJnaW46IDIwcHggMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbiAgICB9XHJcblxyXG5cclxuLmJvdGFvRG93bmxvYWQge1xyXG4gICAgYmFja2dyb3VuZDogIzAwMWM0ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICBib3gtc2hhZG93OiAwIDAgMCAwO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiBsYXJnZTtcclxuICAgIGhlaWdodDogNTVweDtcclxuICAgIHdpZHRoOiAxOTBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4uYmFpeGFyIHtcclxuICAgIG1hcmdpbjogMTBweDtcclxufVxyXG4iXX0= */";
      /***/
    },

    /***/
    "./src/app/download/download.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/download/download.page.ts ***!
      \*******************************************/

    /*! exports provided: DownloadPage */

    /***/
    function srcAppDownloadDownloadPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DownloadPage", function () {
        return DownloadPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_native_downloader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic-native/downloader */
      "./node_modules/@ionic-native/downloader/__ivy_ngcc__/index.js");

      var DownloadPage = /*#__PURE__*/function () {
        function DownloadPage(downloader) {
          _classCallCheck(this, DownloadPage);

          this.downloader = downloader;
        }

        _createClass(DownloadPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "download",
          value: function download() {
            var request = {
              uri: '/assets/img/bg',
              title: 'MyDownload',
              description: '',
              mimeType: '',
              visibleInDownloadsUi: true,
              notificationVisibility: _ionic_native_downloader__WEBPACK_IMPORTED_MODULE_2__["NotificationVisibility"].VisibleNotifyCompleted,
              destinationInExternalFilesDir: {
                dirType: 'Downloads',
                subPath: 'UaiLeva.apk'
              }
            };
            this.downloader.download(request).then(function (location) {
              return console.log('File downloaded at:' + location);
            })["catch"](function (error) {
              return console.error(error);
            });
          }
        }]);

        return DownloadPage;
      }();

      DownloadPage.ctorParameters = function () {
        return [{
          type: _ionic_native_downloader__WEBPACK_IMPORTED_MODULE_2__["Downloader"]
        }];
      };

      DownloadPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-download',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./download.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/download/download.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./download.page.scss */
        "./src/app/download/download.page.scss"))["default"]]
      })], DownloadPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=download-download-module-es5.js.map
(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-adm-login-adm-module"], {
    /***/
    "./node_modules/jwt-decode/build/jwt-decode.esm.js":
    /*!*********************************************************!*\
      !*** ./node_modules/jwt-decode/build/jwt-decode.esm.js ***!
      \*********************************************************/

    /*! exports provided: default, InvalidTokenError */

    /***/
    function node_modulesJwtDecodeBuildJwtDecodeEsmJs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "InvalidTokenError", function () {
        return n;
      });

      function e(e) {
        this.message = e;
      }

      e.prototype = new Error(), e.prototype.name = "InvalidCharacterError";

      var r = "undefined" != typeof window && window.atob && window.atob.bind(window) || function (r) {
        var t = String(r).replace(/=+$/, "");
        if (t.length % 4 == 1) throw new e("'atob' failed: The string to be decoded is not correctly encoded.");

        for (var n, o, a = 0, i = 0, c = ""; o = t.charAt(i++); ~o && (n = a % 4 ? 64 * n + o : o, a++ % 4) ? c += String.fromCharCode(255 & n >> (-2 * a & 6)) : 0) {
          o = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(o);
        }

        return c;
      };

      function t(e) {
        var t = e.replace(/-/g, "+").replace(/_/g, "/");

        switch (t.length % 4) {
          case 0:
            break;

          case 2:
            t += "==";
            break;

          case 3:
            t += "=";
            break;

          default:
            throw "Illegal base64url string!";
        }

        try {
          return function (e) {
            return decodeURIComponent(r(e).replace(/(.)/g, function (e, r) {
              var t = r.charCodeAt(0).toString(16).toUpperCase();
              return t.length < 2 && (t = "0" + t), "%" + t;
            }));
          }(t);
        } catch (e) {
          return r(t);
        }
      }

      function n(e) {
        this.message = e;
      }

      function o(e, r) {
        if ("string" != typeof e) throw new n("Invalid token specified");
        var o = !0 === (r = r || {}).header ? 0 : 1;

        try {
          return JSON.parse(t(e.split(".")[o]));
        } catch (e) {
          throw new n("Invalid token specified: " + e.message);
        }
      }

      n.prototype = new Error(), n.prototype.name = "InvalidTokenError";
      /* harmony default export */

      __webpack_exports__["default"] = o; //# sourceMappingURL=jwt-decode.esm.js.map

      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/login-adm/login-adm.page.html":
    /*!*************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login-adm/login-adm.page.html ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppLoginAdmLoginAdmPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"home\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n    \r\n<ion-content>\r\n  <br><br>\r\n  <ion-grid style=\"margin-top: 9%;\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <form [formGroup]=\"onLoginForm\" > \r\n          <ion-item> \r\n            <ion-input type=\"tel\" formControlName=\"userName\" name=\"telefone\" placeholder=\"Telefone\" [brmasker]=\"{phone: true}\">\r\n              <ion-icon name=\"phone-portrait-outline\" size=\"small\" slot=\"start\"></ion-icon>&nbsp;\r\n            </ion-input>\r\n          </ion-item>\r\n\r\n          <p></p>\r\n\r\n          <ion-item>\r\n            <ion-input formControlName=\"password\" [type]=\"tipo ? 'text' : 'password'\" name=\"senha\" placeholder=\"Senha\">\r\n              <ion-icon name=\"lock-closed-outline\" size=\"small\" slot=\"start\"></ion-icon>&nbsp;\r\n            </ion-input>\r\n            <ion-icon [name]=\"tipo ? 'eye-outline' : 'eye-off-outline'\" slot=\"end\" size=\"small\" (click)=\"mostrarSenha()\" tappable></ion-icon>\r\n          </ion-item>\r\n        </form>\r\n        \r\n\r\n        <u class=\"senha\" (click)=\"forgotPass()\">Esqueci minha senha</u>\r\n        <br>\r\n\r\n        <ion-button class=\"botao\" (click)=\" toLoginHome()\">\r\n          <ion-icon name=\"checkmark-outline\" slot=\"end\" style=\"color: #001c4e;\"></ion-icon>ENTRAR\r\n        </ion-button>\r\n        \r\n        <br>\r\n      </ion-col>\r\n      </ion-row>\r\n  </ion-grid>\r\n  \r\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/login-adm/login-adm-routing.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/login-adm/login-adm-routing.module.ts ***!
      \*******************************************************/

    /*! exports provided: LoginAdmPageRoutingModule */

    /***/
    function srcAppLoginAdmLoginAdmRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginAdmPageRoutingModule", function () {
        return LoginAdmPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _login_adm_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login-adm.page */
      "./src/app/login-adm/login-adm.page.ts");

      var routes = [{
        path: '',
        component: _login_adm_page__WEBPACK_IMPORTED_MODULE_3__["LoginAdmPage"]
      }];

      var LoginAdmPageRoutingModule = function LoginAdmPageRoutingModule() {
        _classCallCheck(this, LoginAdmPageRoutingModule);
      };

      LoginAdmPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginAdmPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/login-adm/login-adm.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/login-adm/login-adm.module.ts ***!
      \***********************************************/

    /*! exports provided: LoginAdmPageModule */

    /***/
    function srcAppLoginAdmLoginAdmModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginAdmPageModule", function () {
        return LoginAdmPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _login_adm_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-adm-routing.module */
      "./src/app/login-adm/login-adm-routing.module.ts");
      /* harmony import */


      var _login_adm_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login-adm.page */
      "./src/app/login-adm/login-adm.page.ts");
      /* harmony import */


      var br_mask__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! br-mask */
      "./node_modules/br-mask/__ivy_ngcc__/dist/index.js");

      var LoginAdmPageModule = function LoginAdmPageModule() {
        _classCallCheck(this, LoginAdmPageModule);
      };

      LoginAdmPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], br_mask__WEBPACK_IMPORTED_MODULE_7__["BrMaskerModule"], _login_adm_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginAdmPageRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]],
        declarations: [_login_adm_page__WEBPACK_IMPORTED_MODULE_6__["LoginAdmPage"]]
      })], LoginAdmPageModule);
      /***/
    },

    /***/
    "./src/app/login-adm/login-adm.page.scss":
    /*!***********************************************!*\
      !*** ./src/app/login-adm/login-adm.page.scss ***!
      \***********************************************/

    /*! exports provided: default */

    /***/
    function srcAppLoginAdmLoginAdmPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-toolbar {\n  --background: #001c4e;\n}\n\nion-title {\n  color: #fff;\n}\n\nion-item {\n  border: 1px solid lightgray;\n  border-radius: 50px;\n  --highlight-height: 0px;\n  /*--highlight-color-focused: var(--ion-color-primary, #001c4e);\n  --highlight-color-valid: var(--ion-color-success, #2dd36f);\n  --highlight-color-invalid: var(--ion-color-danger, #eb445a);*/\n}\n\nion-icon {\n  color: gray;\n}\n\nion-content {\n  padding: 0px;\n}\n\nion-grid {\n  margin: auto;\n  display: block;\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  border: 1px solid;\n  border-left: none;\n  border-right: none;\n  padding: 0px;\n}\n\n.senha {\n  text-align: right;\n  font-size: small;\n  display: block;\n  margin: auto;\n  margin-right: 35px;\n}\n\n.gridCol1 {\n  text-align: center;\n  padding: 50px;\n  font-size: 0.9em;\n  text-decoration: underline;\n}\n\nion-button {\n  --background: #b1ff49;\n  --color: #001c4e;\n  display: block;\n  width: 80%;\n  height: 48px;\n  margin: auto;\n  --border-radius: 50px;\n  --background-activated: #001c4e;\n  --color-activated: white;\n  --background-hover: #001c4e;\n  --color-hover: white;\n}\n\nion-card {\n  background-color: aliceblue;\n  width: -webkit-fit-content 350px;\n  width: -moz-fit-content 350px;\n  width: fit-content 350px;\n  display: block;\n  margin: 15%;\n  margin-top: 12%;\n  box-shadow: none;\n  border: 1px dashed #001c4e;\n}\n\nform {\n  padding: 25px;\n}\n\n.logoLogin img {\n  margin: 30px auto;\n  display: block;\n  width: 20%;\n  background: #001c4e;\n  padding: 10px;\n}\n\ninput {\n  width: 95%;\n  border-radius: 50px;\n  display: block;\n  margin: auto;\n  height: 40px;\n  text-align: start;\n  padding-left: 10px;\n  border: #001c4e 0.5px solid;\n}\n\n.linkEsqueciSenha a {\n  color: #001c4e;\n  text-transform: uppercase;\n  font-size: 12px;\n  display: block;\n  text-align: center;\n  margin-top: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4tYWRtL2xvZ2luLWFkbS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxxQkFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksMkJBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0E7OytEQUFBO0FBR0o7O0FBRUE7RUFDSSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxZQUFBO0VBQ0EsY0FBQTtFQUNBLDBCQUFBO0VBQUEsdUJBQUE7RUFBQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLDBCQUFBO0FBQ0o7O0FBS0E7RUFDSSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0VBQ0EsK0JBQUE7RUFDQSx3QkFBQTtFQUNBLDJCQUFBO0VBQ0Esb0JBQUE7QUFGSjs7QUFLQTtFQUNJLDJCQUFBO0VBQ0EsZ0NBQUE7RUFBQSw2QkFBQTtFQUFBLHdCQUFBO0VBQ0EsY0FBQTtFQUVBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSwwQkFBQTtBQUhKOztBQU1BO0VBQ0ksYUFBQTtBQUhKOztBQU1BO0VBQ0ksaUJBQUE7RUFDQSxjQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtBQUhKOztBQU1BO0VBQ0ksVUFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0FBSEo7O0FBTUE7RUFDSSxjQUFBO0VBQ0EseUJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUFISiIsImZpbGUiOiJzcmMvYXBwL2xvZ2luLWFkbS9sb2dpbi1hZG0ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXIge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAxYzRlO1xyXG59XHJcblxyXG5pb24tdGl0bGUge1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbn1cclxuXHJcbmlvbi1pdGVtIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Z3JheTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDBweDtcclxuICAgIC8qLS1oaWdobGlnaHQtY29sb3ItZm9jdXNlZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnksICMwMDFjNGUpO1xyXG4gICAgLS1oaWdobGlnaHQtY29sb3ItdmFsaWQ6IHZhcigtLWlvbi1jb2xvci1zdWNjZXNzLCAjMmRkMzZmKTtcclxuICAgIC0taGlnaGxpZ2h0LWNvbG9yLWludmFsaWQ6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIsICNlYjQ0NWEpOyovXHJcbn1cclxuXHJcbmlvbi1pY29uIHtcclxuICAgIGNvbG9yOiBncmF5O1xyXG59XHJcblxyXG5pb24tY29udGVudCB7XHJcbiAgICBwYWRkaW5nOiAwcHg7XHJcbn1cclxuXHJcbmlvbi1ncmlkIHtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIGRpc3BsYXk6IGJsb2NrOyBcclxuICAgIHdpZHRoOiBmaXQtY29udGVudDtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgYm9yZGVyLWxlZnQ6IG5vbmU7XHJcbiAgICBib3JkZXItcmlnaHQ6IG5vbmU7XHJcbiAgICBwYWRkaW5nOiAwcHg7XHJcbn1cclxuXHJcbi5zZW5oYSB7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuICAgIGZvbnQtc2l6ZTogc21hbGw7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIG1hcmdpbi1yaWdodDogMzVweDtcclxufVxyXG5cclxuLmdyaWRDb2wxIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHBhZGRpbmc6IDUwcHg7XHJcbiAgICBmb250LXNpemU6IC45ZW07XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxuICAgIC8vYm9yZGVyLWxlZnQ6IG5vbmU7XHJcbiAgICAvL2JvcmRlci1yaWdodDogbm9uZTtcclxufVxyXG5cclxuXHJcbmlvbi1idXR0b24ge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjYjFmZjQ5OyAvLyMwMDFjNGU7XHJcbiAgICAtLWNvbG9yOiAjMDAxYzRlOy8vd2hpdGU7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBoZWlnaHQ6IDQ4cHg7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICAtLWJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiAjMDAxYzRlO1xyXG4gICAgLS1jb2xvci1hY3RpdmF0ZWQ6IHdoaXRlO1xyXG4gICAgLS1iYWNrZ3JvdW5kLWhvdmVyOiAjMDAxYzRlO1xyXG4gICAgLS1jb2xvci1ob3Zlcjogd2hpdGU7XHJcbn1cclxuXHJcbmlvbi1jYXJkIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGFsaWNlYmx1ZTsgXHJcbiAgICB3aWR0aDogZml0LWNvbnRlbnQgMzUwcHg7IFxyXG4gICAgZGlzcGxheTogYmxvY2s7IFxyXG4gICAgLy9wYWRkaW5nOiA0MHB4O1xyXG4gICAgbWFyZ2luOiAxNSU7IFxyXG4gICAgbWFyZ2luLXRvcDogMTIlO1xyXG4gICAgYm94LXNoYWRvdzogbm9uZTsgXHJcbiAgICBib3JkZXI6IDFweCBkYXNoZWQgIzAwMWM0ZTtcclxufVxyXG5cclxuZm9ybSB7XHJcbiAgICBwYWRkaW5nOiAyNXB4O1xyXG59XHJcblxyXG4ubG9nb0xvZ2luIGltZyB7XHJcbiAgICBtYXJnaW46IDMwcHggYXV0bztcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgd2lkdGg6IDIwJTtcclxuICAgIGJhY2tncm91bmQ6ICMwMDFjNGU7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcblxyXG5pbnB1dCB7XHJcbiAgICB3aWR0aDogOTUlO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgdGV4dC1hbGlnbjogc3RhcnQ7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgICBib3JkZXI6ICMwMDFjNGUgMC41cHggc29saWQ7XHJcbn1cclxuXHJcbi5saW5rRXNxdWVjaVNlbmhhIGEge1xyXG4gICAgY29sb3I6ICMwMDFjNGU7Ly93aGl0ZTtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/login-adm/login-adm.page.ts":
    /*!*********************************************!*\
      !*** ./src/app/login-adm/login-adm.page.ts ***!
      \*********************************************/

    /*! exports provided: LoginAdmPage */

    /***/
    function srcAppLoginAdmLoginAdmPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginAdmPage", function () {
        return LoginAdmPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_core_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/core/auth/auth.service */
      "./src/core/auth/auth.service.ts");
      /* harmony import */


      var src_core_plataform_dector_plataform_dector_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/core/plataform-dector/plataform-dector.service */
      "./src/core/plataform-dector/plataform-dector.service.ts");

      var LoginAdmPage = /*#__PURE__*/function () {
        function LoginAdmPage(router, formBuilder, motoristaService, navCtrl, platformDetectorService, toastCtrl, alertCtrl) {
          _classCallCheck(this, LoginAdmPage);

          this.router = router;
          this.formBuilder = formBuilder;
          this.motoristaService = motoristaService;
          this.navCtrl = navCtrl;
          this.platformDetectorService = platformDetectorService;
          this.toastCtrl = toastCtrl;
          this.alertCtrl = alertCtrl;
        }

        _createClass(LoginAdmPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.onLoginForm = this.formBuilder.group({
              'userName': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
              'password': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])]
            });
          }
        }, {
          key: "mostrarSenha",
          value: function mostrarSenha() {
            this.tipo = !this.tipo;
          }
        }, {
          key: "toLoginHome",
          value: function toLoginHome() {
            var _this = this;

            var userName = this.onLoginForm.get('userName').value;
            var password = this.onLoginForm.get('password').value;
            var a = userName.replace('-', '').replace(' ', '').replace('(', '').replace(')', '');
            this.motoristaService.authenticate(a, password).subscribe(function () {
              return _this.navCtrl.navigateRoot('/login-home');
            }, function (err) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                var toast;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        console.log('entrou aqui' + err.message);
                        this.onLoginForm.reset();
                        this.platformDetectorService.isPlataformBrowser();
                        _context.next = 5;
                        return this.toastCtrl.create({
                          message: 'Usuário não liberado, por favor fale com o administrativo.',
                          duration: 4000,
                          position: 'top',
                          color: 'danger'
                        });

                      case 5:
                        toast = _context.sent;
                        toast.present();

                      case 7:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));
            });
          }
        }, {
          key: "forgotPass",
          value: function forgotPass() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var alert;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.alertCtrl.create({
                        header: 'Esqueceu sua senha?',
                        message: 'Informe seu e-mail para a recuperação.',
                        inputs: [{
                          name: 'email',
                          type: 'email',
                          placeholder: 'E-mail'
                        }],
                        buttons: [{
                          text: 'Cancelar',
                          role: 'cancel',
                          cssClass: 'secondary',
                          handler: function handler() {
                            console.log('Confirmar Cancelamento?');
                          }
                        }
                        /*, {
                        text: 'Confirmar',
                        handler: async data => {
                          console.log(data.email)
                          this.recuperarSenha.sendemail(data.email).subscribe(
                              async () =>{
                                console.log('entrou aqui')
                                this.navCtrl.navigateRoot('')
                                const toast = await this.toastCtrl.create({
                                          
                                  message:'Email enviado com sucesso!',
                                  duration:4000, position:'top',
                                  color:'success'
                                  });
                          
                                
                                toast.present();
                              },
                              async err => {
                           
                                 const toast = await this.toastCtrl.create({
                                          
                                          message:'Usuário não cadastrado, por favor cadastre-se no APP!',
                                          duration:4000, position:'top',
                                          color:'danger'
                                  });
                          
                                 
                                 toast.present();
                                                }
                              
                          );
                          
                        }
                        }*/
                        ]
                      });

                    case 2:
                      alert = _context2.sent;
                      _context2.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }]);

        return LoginAdmPage;
      }();

      LoginAdmPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
        }, {
          type: src_core_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
        }, {
          type: src_core_plataform_dector_plataform_dector_service__WEBPACK_IMPORTED_MODULE_6__["PlatformDectorService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
        }];
      };

      LoginAdmPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login-adm',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login-adm.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/login-adm/login-adm.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login-adm.page.scss */
        "./src/app/login-adm/login-adm.page.scss"))["default"]]
      })], LoginAdmPage);
      /***/
    },

    /***/
    "./src/core/auth/auth.service.ts":
    /*!***************************************!*\
      !*** ./src/core/auth/auth.service.ts ***!
      \***************************************/

    /*! exports provided: AuthService */

    /***/
    function srcCoreAuthAuthServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthService", function () {
        return AuthService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../user/user.service */
      "./src/core/user/user.service.ts");

      var API_URL = 'https://uaileva.com.br/api'; //const API_URL = "http://localhost:3000"; 

      var AuthService = /*#__PURE__*/function () {
        function AuthService(http, userService) {
          _classCallCheck(this, AuthService);

          this.http = http;
          this.userService = userService;
        }

        _createClass(AuthService, [{
          key: "authenticate",
          value: function authenticate(userName, password) {
            var _this2 = this;

            console.log("".concat(API_URL, "/motorista/login"));
            return this.http.post("".concat(API_URL, "/motorista/loginadm"), {
              userName: userName,
              password: password
            }, {
              observe: 'response'
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (res) {
              var authToken = res.headers.get('x-access-token');

              _this2.userService.setToken(authToken);

              console.log("User ".concat(userName, " authenticated with token ").concat(authToken));
            }));
          }
        }]);

        return AuthService;
      }();

      AuthService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }, {
          type: _user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]
        }];
      };

      AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], AuthService);
      /***/
    },

    /***/
    "./src/core/plataform-dector/plataform-dector.service.ts":
    /*!***************************************************************!*\
      !*** ./src/core/plataform-dector/plataform-dector.service.ts ***!
      \***************************************************************/

    /*! exports provided: PlatformDectorService */

    /***/
    function srcCorePlataformDectorPlataformDectorServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PlatformDectorService", function () {
        return PlatformDectorService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

      var PlatformDectorService = /*#__PURE__*/function () {
        function PlatformDectorService(platformId) {
          _classCallCheck(this, PlatformDectorService);

          this.platformId = platformId;
        }

        _createClass(PlatformDectorService, [{
          key: "isPlataformBrowser",
          value: function isPlataformBrowser() {
            return Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId);
          }
        }]);

        return PlatformDectorService;
      }();

      PlatformDectorService.ctorParameters = function () {
        return [{
          type: String,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"],
            args: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["PLATFORM_ID"]]
          }]
        }];
      };

      PlatformDectorService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], PlatformDectorService);
      /***/
    },

    /***/
    "./src/core/token/token.service.ts":
    /*!*****************************************!*\
      !*** ./src/core/token/token.service.ts ***!
      \*****************************************/

    /*! exports provided: TokenService */

    /***/
    function srcCoreTokenTokenServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TokenService", function () {
        return TokenService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var KEY = 'authToken';

      var TokenService = /*#__PURE__*/function () {
        function TokenService() {
          _classCallCheck(this, TokenService);
        }

        _createClass(TokenService, [{
          key: "hasToken",
          value: function hasToken() {
            return !!this.getToken();
          }
        }, {
          key: "setToken",
          value: function setToken(token) {
            window.localStorage.setItem(KEY, token);
          }
        }, {
          key: "getToken",
          value: function getToken() {
            return window.localStorage.getItem(KEY);
          }
        }, {
          key: "removeToken",
          value: function removeToken() {
            window.localStorage.removeItem(KEY);
          }
        }]);

        return TokenService;
      }();

      TokenService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], TokenService);
      /***/
    },

    /***/
    "./src/core/user/user.service.ts":
    /*!***************************************!*\
      !*** ./src/core/user/user.service.ts ***!
      \***************************************/

    /*! exports provided: UserService */

    /***/
    function srcCoreUserUserServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserService", function () {
        return UserService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _token_token_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../token/token.service */
      "./src/core/token/token.service.ts");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var jwt_decode__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! jwt-decode */
      "./node_modules/jwt-decode/build/jwt-decode.esm.js"); //import * as jwt_decode from 'jwt-decode';


      var UserService = /*#__PURE__*/function () {
        function UserService(tokenService) {
          _classCallCheck(this, UserService);

          this.tokenService = tokenService;
          this.userSubject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](null);
          this.tokenService.hasToken() && this.decodeAndNotify();
        }

        _createClass(UserService, [{
          key: "setToken",
          value: function setToken(token) {
            this.tokenService.setToken(token);
            this.decodeAndNotify();
          }
        }, {
          key: "getUser",
          value: function getUser() {
            return this.userSubject.asObservable();
          }
        }, {
          key: "decodeAndNotify",
          value: function decodeAndNotify() {
            var token = this.tokenService.getToken();
            var user = Object(jwt_decode__WEBPACK_IMPORTED_MODULE_4__["default"])(token);
            this.fullName = user.full_name;
            this.userSubject.next(user);
          }
        }, {
          key: "logout",
          value: function logout() {
            this.tokenService.removeToken();
            this.userSubject.next(null);
          }
        }, {
          key: "isLogged",
          value: function isLogged() {
            return this.tokenService.hasToken();
          }
        }, {
          key: "getUserName",
          value: function getUserName() {
            return this.fullName;
          }
        }]);

        return UserService;
      }();

      UserService.ctorParameters = function () {
        return [{
          type: _token_token_service__WEBPACK_IMPORTED_MODULE_2__["TokenService"]
        }];
      };

      UserService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], UserService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=login-adm-login-adm-module-es5.js.map
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["editar-motorista-editar-motorista-module"],{

/***/ "./node_modules/jwt-decode/build/jwt-decode.esm.js":
/*!*********************************************************!*\
  !*** ./node_modules/jwt-decode/build/jwt-decode.esm.js ***!
  \*********************************************************/
/*! exports provided: default, InvalidTokenError */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvalidTokenError", function() { return n; });
function e(e){this.message=e}e.prototype=new Error,e.prototype.name="InvalidCharacterError";var r="undefined"!=typeof window&&window.atob&&window.atob.bind(window)||function(r){var t=String(r).replace(/=+$/,"");if(t.length%4==1)throw new e("'atob' failed: The string to be decoded is not correctly encoded.");for(var n,o,a=0,i=0,c="";o=t.charAt(i++);~o&&(n=a%4?64*n+o:o,a++%4)?c+=String.fromCharCode(255&n>>(-2*a&6)):0)o="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(o);return c};function t(e){var t=e.replace(/-/g,"+").replace(/_/g,"/");switch(t.length%4){case 0:break;case 2:t+="==";break;case 3:t+="=";break;default:throw"Illegal base64url string!"}try{return function(e){return decodeURIComponent(r(e).replace(/(.)/g,(function(e,r){var t=r.charCodeAt(0).toString(16).toUpperCase();return t.length<2&&(t="0"+t),"%"+t})))}(t)}catch(e){return r(t)}}function n(e){this.message=e}function o(e,r){if("string"!=typeof e)throw new n("Invalid token specified");var o=!0===(r=r||{}).header?0:1;try{return JSON.parse(t(e.split(".")[o]))}catch(e){throw new n("Invalid token specified: "+e.message)}}n.prototype=new Error,n.prototype.name="InvalidTokenError";/* harmony default export */ __webpack_exports__["default"] = (o);
//# sourceMappingURL=jwt-decode.esm.js.map


/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/editar-motorista/editar-motorista.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/editar-motorista/editar-motorista.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"bloqueio\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <br><br>\r\n  <ion-title>Editar Perfil</ion-title><br>\r\n  <form style=\"padding-left: 25px; padding-right: 25px;\">\r\n    <ion-item>\r\n      <ion-label position=\"stacked\">Nome Completo</ion-label>\r\n      <ion-input [(ngModel)]=\"nomeNovo\" placeholder=\"{{nome}}\" name = \"nomeNovo\" type=\"text\"></ion-input>\r\n    </ion-item>\r\n    \r\n    <ion-item>\r\n      <ion-label position=\"stacked\">Apelido</ion-label>\r\n      <ion-input [(ngModel)]=\"apelidoNovo\" placeholder=\"{{apelido}}\" name = \"apelidoNovo\" type=\"text\"></ion-input>\r\n    </ion-item>\r\n    \r\n    <ion-item>\r\n      <ion-label position=\"stacked\">Telefone</ion-label>\r\n      <ion-input [(ngModel)]=\"phoneNovo\" placeholder=\"{{phone}}\" name = \"phoneNovo\" type=\"tel\"></ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label position=\"stacked\">E-mail</ion-label>\r\n      <ion-input [(ngModel)]=\"emailNovo\" placeholder=\"{{email}}\" name = \"emailNovo\" type=\"text\"></ion-input>\r\n    </ion-item>\r\n    \r\n    <ion-item>\r\n      <ion-label position=\"stacked\">Senha</ion-label>\r\n      <ion-input [(ngModel)]=\"senhaNovo\" name = \"senhaNovo\" placeholder=\"********\" type=\"password\"></ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label position=\"stacked\">Cidade</ion-label>\r\n      <ion-input [(ngModel)]=\"cidadeNovo\" placeholder=\"{{cidade}}\"  name = \"cidadeNovo\" type=\"text\"></ion-input>\r\n    </ion-item>\r\n\r\n    <br>\r\n    <ion-label position=\"stacked\" style=\"padding-left: 18px; font-size: small; font-weight: bold;\">Veículo</ion-label>\r\n    <ion-row>\r\n      <ion-col>\r\n        <ion-item>\r\n          <ion-label position=\"stacked\">Placa</ion-label>\r\n          <ion-input  [(ngModel)]=\"placaNovo\" placeholder=\"{{placa}}\"  name=\"placaNovo\"></ion-input>\r\n        </ion-item>\r\n      </ion-col>\r\n\r\n      <ion-col>\r\n        <ion-item>\r\n          <ion-label position=\"stacked\">Modelo</ion-label>\r\n          <ion-input [(ngModel)]=\"modeloNovo\" placeholder=\"{{modelo}}\"  name = \"modeloNovo\"></ion-input>\r\n        </ion-item>\r\n      </ion-col>\r\n    </ion-row>\r\n\r\n    <ion-grid>\r\n      <ion-col class=\"col-md-12\"><img src=\"{{url_foto}}\"/></ion-col>&nbsp;\r\n      <ion-col class=\"col-md-12\"><img src=\"{{url_cnh}}\"/></ion-col>&nbsp;\r\n      <ion-col class=\"col-md-12\"><img src=\"{{url_crlv}}\"/></ion-col>\r\n    </ion-grid>\r\n\r\n    <ion-grid>\r\n      <ion-col class=\"col-md-12\">\r\n        <ion-label>Foto perfil : </ion-label>&nbsp;&nbsp;\r\n        <input type=\"file\" (change)= \"file = $event.target.files[0]\" accept=\"image/*\">\r\n      </ion-col>&nbsp;\r\n      <ion-col class=\"col-md-12\">\r\n        <ion-label>Foto CNH : </ion-label>&nbsp;&nbsp;\r\n        <input type=\"file\" (change)= \"fileCNH = $event.target.files[0]\" accept=\"image/*\">\r\n      </ion-col>&nbsp;\r\n      <ion-col class=\"col-md-12\">\r\n        <ion-label>Foto CRLV : </ion-label>&nbsp;&nbsp;\r\n        <input type=\"file\" (change)= \"fileCRLV = $event.target.files[0]\" accept=\"image/*\">\r\n      </ion-col>\r\n    </ion-grid>\r\n  </form><br>\r\n\r\n  <ion-button (click) = \"salvar();\" class=\"botao\" tappable>\r\n    Salvar Alterações\r\n    <ion-icon name=\"checkmark-outline\" style=\"color: #001c4e;\"></ion-icon>\r\n  </ion-button>\r\n<br>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/editar-motorista/editar-motorista-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/editar-motorista/editar-motorista-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: EditarMotoristaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarMotoristaPageRoutingModule", function() { return EditarMotoristaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _editar_motorista_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./editar-motorista.page */ "./src/app/editar-motorista/editar-motorista.page.ts");




const routes = [
    {
        path: '',
        component: _editar_motorista_page__WEBPACK_IMPORTED_MODULE_3__["EditarMotoristaPage"]
    }
];
let EditarMotoristaPageRoutingModule = class EditarMotoristaPageRoutingModule {
};
EditarMotoristaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EditarMotoristaPageRoutingModule);



/***/ }),

/***/ "./src/app/editar-motorista/editar-motorista.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/editar-motorista/editar-motorista.module.ts ***!
  \*************************************************************/
/*! exports provided: EditarMotoristaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarMotoristaPageModule", function() { return EditarMotoristaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _editar_motorista_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./editar-motorista-routing.module */ "./src/app/editar-motorista/editar-motorista-routing.module.ts");
/* harmony import */ var _editar_motorista_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./editar-motorista.page */ "./src/app/editar-motorista/editar-motorista.page.ts");







let EditarMotoristaPageModule = class EditarMotoristaPageModule {
};
EditarMotoristaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _editar_motorista_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditarMotoristaPageRoutingModule"]
        ],
        declarations: [_editar_motorista_page__WEBPACK_IMPORTED_MODULE_6__["EditarMotoristaPage"]]
    })
], EditarMotoristaPageModule);



/***/ }),

/***/ "./src/app/editar-motorista/editar-motorista.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/editar-motorista/editar-motorista.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("img {\n  border-radius: 5%;\n  max-width: 85%;\n}\n\nion-toolbar {\n  --background: #001c4e;\n}\n\nion-grid {\n  display: inline-block;\n  margin: auto;\n  text-align: center;\n  width: 100%;\n  border: 1px solid;\n  border-left: none;\n  border-right: none;\n}\n\nion-title {\n  text-align: center;\n  color: #001c4e;\n  font-size: 1.5em;\n  display: block;\n}\n\nion-item {\n  --highlight-height: 1px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZWRpdGFyLW1vdG9yaXN0YS9lZGl0YXItbW90b3Jpc3RhLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBRUEsY0FBQTtBQUFKOztBQUlBO0VBQ0kscUJBQUE7QUFESjs7QUFJQTtFQUNJLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQURKOztBQUlBO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FBREo7O0FBS0E7RUFDSSx1QkFBQTtBQUZKIiwiZmlsZSI6InNyYy9hcHAvZWRpdGFyLW1vdG9yaXN0YS9lZGl0YXItbW90b3Jpc3RhLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImltZyB7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1JTtcclxuICAgIC8vYm9yZGVyOiAjMDAxYzRlIDAuNXB4IHNvbGlkO1xyXG4gICAgbWF4LXdpZHRoOiA4NSU7XHJcbiAgICBcclxufVxyXG5cclxuaW9uLXRvb2xiYXIge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAxYzRlO1xyXG59IFxyXG5cclxuaW9uLWdyaWQge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZDtcclxuICAgIGJvcmRlci1sZWZ0OiBub25lO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiBub25lO1xyXG59XHJcblxyXG5pb24tdGl0bGUge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyOyBcclxuICAgIGNvbG9yOiAjMDAxYzRlO1xyXG4gICAgZm9udC1zaXplOiAxLjVlbTtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG5cclxuaW9uLWl0ZW17XHJcbiAgICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDFweDtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/editar-motorista/editar-motorista.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/editar-motorista/editar-motorista.page.ts ***!
  \***********************************************************/
/*! exports provided: EditarMotoristaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarMotoristaPage", function() { return EditarMotoristaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/core/user/user.service */ "./src/core/user/user.service.ts");
/* harmony import */ var src_servico_editar_motorista__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/servico/editar-motorista */ "./src/servico/editar-motorista.ts");
/* harmony import */ var src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/servico/motorista_liberacao */ "./src/servico/motorista_liberacao.ts");







let EditarMotoristaPage = class EditarMotoristaPage {
    constructor(router, route, motoristaService, motoristaLiberacaoService, toastCtrl, editarService) {
        this.router = router;
        this.route = route;
        this.motoristaService = motoristaService;
        this.motoristaLiberacaoService = motoristaLiberacaoService;
        this.toastCtrl = toastCtrl;
        this.editarService = editarService;
        this.apelido = ' - ';
        this.url_foto = 'assets/img/no.png';
        this.url_crlv = 'assets/img/no.png';
        this.url_cnh = 'assets/img/no.png';
        this.nomeNovo = '';
        this.apelidoNovo = '';
        this.phoneNovo = '';
        this.emailNovo = '';
        this.senhaNovo = '';
        this.cidadeNovo = '';
        this.placaNovo = '';
        this.modeloNovo = '';
        this.user$ = motoristaService.getUser();
        this.user$.subscribe(usuario => {
            this.usuarioLogado = usuario;
            this.user_id = this.usuarioLogado.id;
            var a = this.usuarioLogado.full_name.split(' ');
            if (this.usuarioLogado.apelido == null) {
                this.apelido = '';
            }
            else {
                this.apelido = '(' + this.usuarioLogado.apelido + ')';
            }
            this.nome = a[0] + ' ' + this.apelido;
        });
        this.route.params.subscribe(parametros => {
            this.motorista_id = parametros['user_id'];
            this.motoristaLiberacaoService.motoristaLiberadoUnico(this.motorista_id).subscribe(motorista => {
                this.mot = motorista;
                this.nome = motorista.full_name;
                this.phone = this.mot.phone;
                this.email = this.mot.email;
                this.password = this.mot.password;
                this.cidade = this.mot.cidade;
                this.apelido = this.mot.apelido;
                this.placa = this.mot.placa;
                this.modelo = this.mot.modelo;
                this.motId = this.mot.id;
                this.motoristaLiberacaoService.fotoMotorista(this.mot.phone).subscribe(foto_motorista => {
                    this.photo = foto_motorista;
                    this.url_foto = `https://uaileva.com.br/api/imgs/` + this.photo.url;
                });
                this.motoristaLiberacaoService.fotoCrlv(this.mot.phone).subscribe(foto_crlv => {
                    this.photo_crlv = foto_crlv;
                    this.url_crlv = `https://uaileva.com.br/api/imgs/` + this.photo_crlv.url;
                });
                this.motoristaLiberacaoService.fotoCnh(this.mot.phone).subscribe(foto_cnh => {
                    this.photo_cnh = foto_cnh;
                    this.url_cnh = `https://uaileva.com.br/api/imgs/` + this.photo_cnh.url;
                });
            });
        });
    }
    ngOnInit() {
    }
    salvar() {
        if (this.nomeNovo != '') {
            console.log("" + this.nomeNovo);
            this.editarService.updateNome(this.motorista_id, this.nomeNovo, '').subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Nome editado com sucesso!',
                    duration: 4000, position: 'top',
                    color: 'success'
                })).present();
            }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                    duration: 4000, position: 'top',
                    color: 'danger'
                })).present();
            }));
        }
        if (this.apelidoNovo != '') {
            console.log("" + this.apelidoNovo);
            this.editarService.apelido(this.motorista_id, this.apelidoNovo).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Apelido editado com sucesso!',
                    duration: 4000, position: 'top',
                    color: 'success'
                })).present();
            }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                    duration: 4000, position: 'top',
                    color: 'danger'
                })).present();
            }));
        }
        if (this.phoneNovo != '') {
            console.log("" + this.phoneNovo);
            this.editarService.phone(this.motorista_id, this.phoneNovo).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Phone editada com sucesso!',
                    duration: 4000, position: 'top',
                    color: 'success'
                })).present();
            }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                    duration: 4000, position: 'top',
                    color: 'danger'
                })).present();
            }));
        }
        if (this.emailNovo != '') {
            console.log("" + this.emailNovo);
            this.editarService.email(this.motorista_id, this.emailNovo).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Email editado com sucesso!',
                    duration: 4000, position: 'top',
                    color: 'success'
                })).present();
            }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                    duration: 4000, position: 'top',
                    color: 'danger'
                })).present();
            }));
        }
        if (this.senhaNovo != '') {
            console.log("" + this.senhaNovo);
            this.editarService.updatePassword(this.motorista_id, this.senhaNovo).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Senha editado com sucesso!',
                    duration: 4000, position: 'top',
                    color: 'success'
                })).present();
            }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                    duration: 4000, position: 'top',
                    color: 'danger'
                })).present();
            }));
        }
        if (this.cidadeNovo != '') {
            console.log("" + this.cidadeNovo);
            this.editarService.cidade(this.motorista_id, this.cidadeNovo).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Cidade editada com sucesso!',
                    duration: 4000, position: 'top',
                    color: 'success'
                })).present();
            }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                    duration: 4000, position: 'top',
                    color: 'danger'
                })).present();
            }));
        }
        if (this.placaNovo != '' || this.modeloNovo != '' || this.placaNovo != undefined || this.placaNovo != null || this.modeloNovo != null || this.modeloNovo != undefined) {
            console.log("" + this.placaNovo);
            if (this.modeloNovo == '') {
                this.modeloNovo = this.modelo;
            }
            if (this.placaNovo == '') {
                this.placaNovo = this.placa;
            }
            this.editarService.placa(this.motorista_id, this.placaNovo, this.modeloNovo).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Placa do carro editado com sucesso!',
                    duration: 4000, position: 'top',
                    color: 'success'
                })).present();
            }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                    duration: 4000, position: 'top',
                    color: 'danger'
                })).present();
            }));
        }
        if (this.file != undefined) {
            console.log(this.file);
            let allowComments = true;
            let description = this.phone + '/ USER / PEFIL';
            this.editarService.updateFoto(description, allowComments, this.file).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Foto editada com sucesso!',
                    duration: 4000, position: 'top',
                    color: 'success'
                })).present();
            }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                    duration: 4000, position: 'top',
                    color: 'danger'
                })).present();
            }));
        }
        console.log(this.phone);
        if (this.fileCNH != undefined) {
            console.log(this.fileCNH);
            let allowComments = true;
            let description = this.phone + '/ MOTORISTA / CNH';
            this.editarService.updateFoto(description, allowComments, this.file).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Foto editada com sucesso!',
                    duration: 4000, position: 'top',
                    color: 'success'
                })).present();
            }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                    duration: 4000, position: 'top',
                    color: 'danger'
                })).present();
            }));
        }
        if (this.fileCRLV != undefined) {
            console.log(this.fileCRLV);
            let allowComments = true;
            let description = this.phone + '/ MOTORISTA / CRLV';
            this.editarService.updateFoto(description, allowComments, this.file).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Foto editada com sucesso!',
                    duration: 4000, position: 'top',
                    color: 'success'
                })).present();
            }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                    duration: 4000, position: 'top',
                    color: 'danger'
                })).present();
            }));
        }
    }
};
EditarMotoristaPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: src_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_6__["MotoristaLiberacaoService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: src_servico_editar_motorista__WEBPACK_IMPORTED_MODULE_5__["EditarPerfilService"] }
];
EditarMotoristaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-editar-motorista',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./editar-motorista.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/editar-motorista/editar-motorista.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./editar-motorista.page.scss */ "./src/app/editar-motorista/editar-motorista.page.scss")).default]
    })
], EditarMotoristaPage);



/***/ }),

/***/ "./src/core/token/token.service.ts":
/*!*****************************************!*\
  !*** ./src/core/token/token.service.ts ***!
  \*****************************************/
/*! exports provided: TokenService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenService", function() { return TokenService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


const KEY = 'authToken';
let TokenService = class TokenService {
    hasToken() {
        return !!this.getToken();
    }
    setToken(token) {
        window.localStorage.setItem(KEY, token);
    }
    getToken() {
        return window.localStorage.getItem(KEY);
    }
    removeToken() {
        window.localStorage.removeItem(KEY);
    }
};
TokenService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], TokenService);



/***/ }),

/***/ "./src/core/user/user.service.ts":
/*!***************************************!*\
  !*** ./src/core/user/user.service.ts ***!
  \***************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _token_token_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../token/token.service */ "./src/core/token/token.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/build/jwt-decode.esm.js");




//import * as jwt_decode from 'jwt-decode';

let UserService = class UserService {
    constructor(tokenService) {
        this.tokenService = tokenService;
        this.userSubject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](null);
        this.tokenService.hasToken() && this.decodeAndNotify();
    }
    setToken(token) {
        this.tokenService.setToken(token);
        this.decodeAndNotify();
    }
    getUser() {
        return this.userSubject.asObservable();
    }
    decodeAndNotify() {
        const token = this.tokenService.getToken();
        const user = Object(jwt_decode__WEBPACK_IMPORTED_MODULE_4__["default"])(token);
        this.fullName = user.full_name;
        this.userSubject.next(user);
    }
    logout() {
        this.tokenService.removeToken();
        this.userSubject.next(null);
    }
    isLogged() {
        return this.tokenService.hasToken();
    }
    getUserName() {
        return this.fullName;
    }
};
UserService.ctorParameters = () => [
    { type: _token_token_service__WEBPACK_IMPORTED_MODULE_2__["TokenService"] }
];
UserService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], UserService);



/***/ }),

/***/ "./src/servico/editar-motorista.ts":
/*!*****************************************!*\
  !*** ./src/servico/editar-motorista.ts ***!
  \*****************************************/
/*! exports provided: EditarPerfilService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarPerfilService", function() { return EditarPerfilService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



const API_URL = "https://uaileva.com.br/api";
//const API_URL = "http://localhost:3000"; 
let EditarPerfilService = class EditarPerfilService {
    constructor(http) {
        this.http = http;
    }
    updateNome(user_id, nome, secondName) {
        return this.http.post(`${API_URL}/motorista/updateNome`, { user_id, nome, secondName });
    }
    ;
    updatePassword(user_id, password) {
        return this.http.post(`${API_URL}/motorista/updatePassword`, { user_id, password });
    }
    ;
    apelido(user_id, apelido) {
        return this.http.post(`${API_URL}/motorista/updateApelido`, { user_id, apelido });
    }
    ;
    placa(motorista_id, placa, modelo) {
        console.log(placa);
        console.log(modelo);
        console.log(motorista_id);
        return this.http.get(`${API_URL}/carro/update/` + motorista_id + `/` + placa + `/` + modelo + ``);
    }
    ;
    modelo(motorista_id, placa, modelo) {
        console.log(placa);
        console.log(modelo);
        return this.http.get(`${API_URL}/carro/update/` + motorista_id + `/` + placa + `/` + modelo + ``);
    }
    ;
    cidade(motorista_id, cidade) {
        return this.http.get(`${API_URL}/motorista/cidade/` + motorista_id + `/` + cidade + ``);
    }
    ;
    phone(motorista_id, phone) {
        return this.http.get(`${API_URL}/motorista/phone/` + motorista_id + `/` + phone + ``);
    }
    ;
    email(motorista_id, email) {
        return this.http.get(`${API_URL}/motorista/email/` + motorista_id + `/` + email + ``);
    }
    ;
    updateFoto(description, allowComments, file) {
        {
            const formData = new FormData();
            formData.append('description', description);
            formData.append('allowComments', allowComments ? 'true' : 'false');
            formData.append('imageFile', file);
            return this.http.post(API_URL + '/photos/update', formData);
        }
    }
};
EditarPerfilService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
EditarPerfilService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], EditarPerfilService);



/***/ }),

/***/ "./src/servico/motorista_liberacao.ts":
/*!********************************************!*\
  !*** ./src/servico/motorista_liberacao.ts ***!
  \********************************************/
/*! exports provided: MotoristaLiberacaoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MotoristaLiberacaoService", function() { return MotoristaLiberacaoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



const API_URL = "https://uaileva.com.br/api";
let MotoristaLiberacaoService = class MotoristaLiberacaoService {
    constructor(http) {
        this.http = http;
    }
    liberar() {
        return this.http.get(API_URL + `/motorista/liberacao`);
    }
    motoristaUnico(motorista_id) {
        return this.http.get(API_URL + `/motorista/unico/` + motorista_id);
    }
    fotoMotorista(mot_phone) {
        return this.http.get(API_URL + `/photos/motorista/` + mot_phone);
    }
    fotoCrlv(mot_phone) {
        return this.http.get(API_URL + `/photos/motorista/crlv/` + mot_phone);
    }
    fotoCnh(mot_phone) {
        return this.http.get(API_URL + `/photos/motorista/cnh/` + mot_phone);
    }
    carro(id_motorista, placa, modelo) {
        return this.http.get(API_URL + `/carro/signup/` + id_motorista + `/` + placa + `/` + modelo + ``);
    }
    updateNome(user_id, nome, secondName) {
        return this.http.post(`${API_URL}/motorista/updateNome`, { user_id, nome, secondName });
    }
    ;
    liberarMotorista(phone, email) {
        return this.http.get(API_URL + `/motorista/status/` + phone + `/` + email + `/LIBERADO`);
    }
    motoristaLiberado() {
        return this.http.get(API_URL + `/motoristaLiberado`);
    }
    bloqueio(phone, email) {
        return this.http.get(API_URL + `/motorista/status/` + phone + `/` + email + `/BLOQUEADO`);
    }
    bloqueioPassageiro(phone) {
        return this.http.get(API_URL + `/user/bloqueio/` + phone + `/BLOQUEADO`);
    }
    corridaCM(data) {
        console.log(data);
        return this.http.get(API_URL + `/corrida/diariacm/` + data + ``);
    }
    corridaCP(data) {
        return this.http.get(API_URL + `/corrida/diariacp/` + data + ``);
    }
    corridaF(data) {
        return this.http.get(API_URL + `/corrida/diariaf/` + data + ``);
    }
    corridaTotal() {
        return this.http.get(API_URL + `/totalCorridas`);
    }
    motoristaLiberadoUnico(motorista_id) {
        return this.http.get(API_URL + `/motorista/liberadoUnico/` + motorista_id);
    }
    arrecadacao(motorista_id) {
        return this.http.get(API_URL + `/totalarrecadado/` + motorista_id);
    }
    desbloqueio(phone, email) {
        return this.http.get(API_URL + `/motorista/status/` + phone + `/` + email + `/LIBERADO`);
    }
    motbloqueado() {
        return this.http.get(API_URL + `/motorista/bloqueado`);
    }
};
MotoristaLiberacaoService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
MotoristaLiberacaoService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], MotoristaLiberacaoService);



/***/ })

}]);
//# sourceMappingURL=editar-motorista-editar-motorista-module-es2015.js.map
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["carteira-carteira-module"],{

/***/ "./node_modules/ng2-search-filter/__ivy_ngcc__/ng2-search-filter.js":
/*!**************************************************************************!*\
  !*** ./node_modules/ng2-search-filter/__ivy_ngcc__/ng2-search-filter.js ***!
  \**************************************************************************/
/*! exports provided: Ng2SearchPipeModule, Ng2SearchPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ng2SearchPipeModule", function() { return Ng2SearchPipeModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Ng2SearchPipe", function() { return Ng2SearchPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");



class Ng2SearchPipe {
    /**
     * @param {?} items object from array
     * @param {?} term term's search
     * @return {?}
     */
    transform(items, term) {
        if (!term || !items)
            return items;
        return Ng2SearchPipe.filter(items, term);
    }
    /**
     *
     * @param {?} items List of items to filter
     * @param {?} term  a string term to compare with every property of the list
     *
     * @return {?}
     */
    static filter(items, term) {
        const /** @type {?} */ toCompare = term.toLowerCase();
        /**
         * @param {?} item
         * @param {?} term
         * @return {?}
         */
        function checkInside(item, term) {
            for (let /** @type {?} */ property in item) {
                if (item[property] === null || item[property] == undefined) {
                    continue;
                }
                if (typeof item[property] === 'object') {
                    if (checkInside(item[property], term)) {
                        return true;
                    }
                }
                if (item[property].toString().toLowerCase().includes(toCompare)) {
                    return true;
                }
            }
            return false;
        }
        return items.filter(function (item) {
            return checkInside(item, term);
        });
    }
}
Ng2SearchPipe.ɵfac = function Ng2SearchPipe_Factory(t) { return new (t || Ng2SearchPipe)(); };
Ng2SearchPipe.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({ name: "filter", type: Ng2SearchPipe, pure: false });
Ng2SearchPipe.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: Ng2SearchPipe, factory: Ng2SearchPipe.ɵfac });
/**
 * @nocollapse
 */
Ng2SearchPipe.ctorParameters = () => [];
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](Ng2SearchPipe, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"],
        args: [{
                name: 'filter',
                pure: false
            }]
    }, {
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"]
    }], null, null); })();

class Ng2SearchPipeModule {
}
Ng2SearchPipeModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: Ng2SearchPipeModule });
Ng2SearchPipeModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function Ng2SearchPipeModule_Factory(t) { return new (t || Ng2SearchPipeModule)(); } });
/**
 * @nocollapse
 */
Ng2SearchPipeModule.ctorParameters = () => [];
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](Ng2SearchPipeModule, { declarations: [Ng2SearchPipe], exports: [Ng2SearchPipe] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](Ng2SearchPipeModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [Ng2SearchPipe],
                exports: [Ng2SearchPipe]
            }]
    }], null, null); })();

/**
 * Generated bundle index. Do not edit.
 */



//# sourceMappingURL=ng2-search-filter.js.map

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/carteira/carteira.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/carteira/carteira.page.html ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"login-home\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <br><br>\r\n  <ion-title>Carteira</ion-title><br>\r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-col style=\"border-right: 1px solid; border-bottom: 1px solid;\">Total Arrecadado</ion-col>\r\n      <ion-col style=\"text-align: center; border-bottom: 1px solid;\">R$ {{valortotal}}</ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <ion-col style=\"border-right: 1px solid;\">Quantidade de Corridas</ion-col>\r\n      <ion-col style=\"text-align: center;\">{{total}}</ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n\r\n  <br>\r\n\r\n  <ion-grid style=\"border: none;\">\r\n   <!--<ion-item>\r\n      <ion-label position=\"stacked\">Carteira do Motorista</ion-label>\r\n      <ion-input placeholder=\"Pesquise pelo telefone\" [(ngModel)]=\"telefone\" type=\"tel\" [brmasker]=\"{phone: true}\" ></ion-input>\r\n    </ion-item>\r\n    <br>-->\r\n\r\n    \r\n    <ion-item-divider mode=\"md\">Selecione o motorista para abrir a carteira</ion-item-divider>\r\n    <ion-searchbar [(ngModel)]=\"filterItem\" mode=\"ios\" placeholder=\"Procure pelo nome ou telefone\"></ion-searchbar>\r\n    <ion-list *ngIf=\"filterItem\">\r\n      <ion-item *ngFor=\"let m of m | filter: filterItem\" (click)=\"carteiraMotorista(m.id)\">\r\n        <ion-label>{{m.phone}}\r\n          <p>{{m.full_name}}</p>\r\n        </ion-label>\r\n        <ion-icon name=\"chevron-forward-outline\" size=\"small\" slot=\"end\"></ion-icon>\r\n      </ion-item>\r\n    </ion-list>\r\n    <br>\r\n    <!--<ion-button (click)=\"carteiraMotorista(tel)\" class=\"botaoC\">Abrir carteira</ion-button>-->\r\n  </ion-grid>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/carteira/carteira-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/carteira/carteira-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: CarteiraPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarteiraPageRoutingModule", function() { return CarteiraPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _carteira_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./carteira.page */ "./src/app/carteira/carteira.page.ts");




const routes = [
    {
        path: '',
        component: _carteira_page__WEBPACK_IMPORTED_MODULE_3__["CarteiraPage"]
    }
];
let CarteiraPageRoutingModule = class CarteiraPageRoutingModule {
};
CarteiraPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CarteiraPageRoutingModule);



/***/ }),

/***/ "./src/app/carteira/carteira.module.ts":
/*!*********************************************!*\
  !*** ./src/app/carteira/carteira.module.ts ***!
  \*********************************************/
/*! exports provided: CarteiraPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarteiraPageModule", function() { return CarteiraPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _carteira_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./carteira-routing.module */ "./src/app/carteira/carteira-routing.module.ts");
/* harmony import */ var _carteira_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./carteira.page */ "./src/app/carteira/carteira.page.ts");
/* harmony import */ var br_mask__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! br-mask */ "./node_modules/br-mask/__ivy_ngcc__/dist/index.js");
/* harmony import */ var ng2_search_filter__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-search-filter */ "./node_modules/ng2-search-filter/__ivy_ngcc__/ng2-search-filter.js");









let CarteiraPageModule = class CarteiraPageModule {
};
CarteiraPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            br_mask__WEBPACK_IMPORTED_MODULE_7__["BrMaskerModule"],
            ng2_search_filter__WEBPACK_IMPORTED_MODULE_8__["Ng2SearchPipeModule"],
            _carteira_routing_module__WEBPACK_IMPORTED_MODULE_5__["CarteiraPageRoutingModule"]
        ],
        declarations: [_carteira_page__WEBPACK_IMPORTED_MODULE_6__["CarteiraPage"]]
    })
], CarteiraPageModule);



/***/ }),

/***/ "./src/app/carteira/carteira.page.scss":
/*!*********************************************!*\
  !*** ./src/app/carteira/carteira.page.scss ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-card, img {\n  border-radius: 5%;\n  border: #001c4e 0.5px solid;\n}\n\nion-icon {\n  color: gray;\n}\n\nion-toolbar {\n  --background: #001c4e;\n}\n\nion-grid {\n  margin: auto;\n  display: block;\n  width: 90%;\n  border: 1px solid;\n  border-left: none;\n  border-right: none;\n}\n\nion-title {\n  text-align: center;\n  color: #001c4e;\n  font-size: 1.5em;\n  display: block;\n}\n\nion-item {\n  --highlight-height: 1px;\n  --ion-item-background: transparent;\n  --border-style: var(--border-style);\n  --border: 0 none;\n  --box-shadow: 0 0 0 0;\n  --outline: 0;\n}\n\nion-label {\n  border-left: 1px solid gray;\n  padding-left: 10px;\n}\n\n.botaoC {\n  --background: #b1ff49;\n  --color: #001c4e;\n  font-weight: bold;\n  display: block;\n  height: 40px;\n  width: 85%;\n  margin: auto;\n  --border-radius: 50px;\n  --background-activated: #001c4e;\n  --color-activated: #001c4e;\n  --background-hover: #b1ff49;\n  --color-hover: #001c4e;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FydGVpcmEvY2FydGVpcmEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSwyQkFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7RUFDQSxjQUFBO0VBQ0EsVUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FBQ0o7O0FBRUE7RUFDSSx1QkFBQTtFQUNBLGtDQUFBO0VBQ0EsbUNBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUVBO0VBQ0ksMkJBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLHFCQUFBO0VBQ0EsK0JBQUE7RUFDQSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQkFBQTtFQUNBLFVBQUE7QUFDSiIsImZpbGUiOiJzcmMvYXBwL2NhcnRlaXJhL2NhcnRlaXJhLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkLCBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNSU7XHJcbiAgICBib3JkZXI6ICMwMDFjNGUgMC41cHggc29saWQ7XHJcbn1cclxuXHJcbmlvbi1pY29uIHtcclxuICAgIGNvbG9yOiBncmF5O1xyXG59XHJcblxyXG5pb24tdG9vbGJhciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGU7XHJcbn0gXHJcblxyXG5pb24tZ3JpZCB7XHJcbiAgICBtYXJnaW46IGF1dG87IFxyXG4gICAgZGlzcGxheTogYmxvY2s7IFxyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgYm9yZGVyLWxlZnQ6IG5vbmU7XHJcbiAgICBib3JkZXItcmlnaHQ6IG5vbmU7XHJcbn1cclxuXHJcbmlvbi10aXRsZSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7IFxyXG4gICAgY29sb3I6ICMwMDFjNGU7XHJcbiAgICBmb250LXNpemU6IDEuNWVtO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbmlvbi1pdGVte1xyXG4gICAgLS1oaWdobGlnaHQtaGVpZ2h0OiAxcHg7XHJcbiAgICAtLWlvbi1pdGVtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgLS1ib3JkZXItc3R5bGU6IHZhcigtLWJvcmRlci1zdHlsZSk7XHJcbiAgICAtLWJvcmRlcjogMCBub25lOyBcclxuICAgIC0tYm94LXNoYWRvdzogMCAwIDAgMDsgXHJcbiAgICAtLW91dGxpbmU6IDA7XHJcbn1cclxuXHJcbmlvbi1sYWJlbCB7XHJcbiAgICBib3JkZXItbGVmdDogMXB4IHNvbGlkIGdyYXk7IFxyXG4gICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG59XHJcblxyXG4uYm90YW9DIHtcclxuICAgIC0tYmFja2dyb3VuZDogI2IxZmY0OTsgIC8vIzAwMWM0ZTtcclxuICAgIC0tY29sb3I6ICMwMDFjNGU7IC8vd2hpdGU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgd2lkdGg6IDg1JTtcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIC0tYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6ICMwMDFjNGU7XHJcbiAgICAtLWNvbG9yLWFjdGl2YXRlZDogIzAwMWM0ZTtcclxuICAgIC0tYmFja2dyb3VuZC1ob3ZlcjogI2IxZmY0OTtcclxuICAgIC0tY29sb3ItaG92ZXI6ICAjMDAxYzRlO1xyXG4gICAgLS1ib3gtc2hhZG93OiAwIDAgMCAwOyBcclxuICAgIG91dGxpbmU6IDA7XHJcbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/carteira/carteira.page.ts":
/*!*******************************************!*\
  !*** ./src/app/carteira/carteira.page.ts ***!
  \*******************************************/
/*! exports provided: CarteiraPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarteiraPage", function() { return CarteiraPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/servico/motorista_liberacao */ "./src/servico/motorista_liberacao.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_core_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/core/user/user.service */ "./src/core/user/user.service.ts");
/* harmony import */ var _carteira_motorista_carteira_motorista_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../carteira-motorista/carteira-motorista.page */ "./src/app/carteira-motorista/carteira-motorista.page.ts");







let CarteiraPage = class CarteiraPage {
    constructor(router, route, motoristaService, motoristaLiberacaoService, toastCtrl, carteiraService, navCtrl, alertCtrl, modalCtrl) {
        this.router = router;
        this.route = route;
        this.motoristaService = motoristaService;
        this.motoristaLiberacaoService = motoristaLiberacaoService;
        this.toastCtrl = toastCtrl;
        this.carteiraService = carteiraService;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.url_foto = 'assets/img/no.png';
        this.url_crlv = 'assets/img/no.png';
        this.url_cnh = 'assets/img/no.png';
        this.carteiraService.corridaTotal().subscribe(carteira => {
            this.carteira = carteira;
            this.total = this.carteira.total;
            this.valortotal = parseFloat(this.carteira.valor_total).toFixed(2);
        });
        this.user$ = motoristaService.getUser();
        this.route.params.subscribe(parametros => {
            this.motorista_id = parametros['user_id'];
        });
        this.user$.subscribe(usuario => {
            this.usuarioLogado = usuario;
            this.user_id = this.usuarioLogado.id;
            var a = this.usuarioLogado.full_name.split(' ');
            /* if(this.usuarioLogado.apelido == null){
               this.apelido = '';
             } else {
               this.apelido = '(' + this.usuarioLogado.apelido + ')';
             }
             this.nomeMotorista = a[0] + ' ' + this.apelido;*/
        });
        this.motoristaLiberacaoService.motoristaLiberado().subscribe(motoristaLiberado => {
            this.m = motoristaLiberado;
        });
    }
    ngOnInit() {
    }
    carteiraMotorista(tel) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            //this.telefone = this.telefone.replace('-','').replace(' ','').replace('(','').replace(')','');
            //tel = this.telefone;
            this.filterItem = '';
            const modal = yield this.modalCtrl.create({
                component: _carteira_motorista_carteira_motorista_page__WEBPACK_IMPORTED_MODULE_6__["CarteiraMotoristaPage"],
                componentProps: {
                    telMot: tel
                },
                cssClass: 'my-custom-class'
            });
            return yield modal.present();
        });
    }
};
CarteiraPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: src_core_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_2__["MotoristaLiberacaoService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_2__["MotoristaLiberacaoService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] }
];
CarteiraPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-carteira',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./carteira.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/carteira/carteira.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./carteira.page.scss */ "./src/app/carteira/carteira.page.scss")).default]
    })
], CarteiraPage);



/***/ })

}]);
//# sourceMappingURL=carteira-carteira-module-es2015.js.map
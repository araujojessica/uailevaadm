(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["corridas-corridas-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/corridas/corridas.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/corridas/corridas.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppCorridasCorridasPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"login-home\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <br><br>\r\n  <ion-title>Corridas Diárias</ion-title><br>\r\n  <ion-grid>\r\n    <ion-row>\r\n      <ion-col style=\"border-right: 1px solid; border-bottom: 1px solid;\">Corridas canceladas pelos passageiros</ion-col>\r\n      <ion-col style=\"text-align: center; border-bottom: 1px solid;\">{{canceladoPassageiro}}</ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <ion-col style=\"border-right: 1px solid; border-bottom: 1px solid;\">Corridas canceladas pelos motoristas</ion-col>\r\n      <ion-col style=\"text-align: center; border-bottom: 1px solid;\">{{canceladoMotorista}}</ion-col>\r\n    </ion-row>\r\n\r\n    <ion-row>\r\n      <ion-col style=\"border-right: 1px solid;\">Corridas finalizadas</ion-col>\r\n      <ion-col style=\"text-align: center;\">{{finalizadas}}</ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "./src/app/corridas/corridas-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/corridas/corridas-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: CorridasPageRoutingModule */

    /***/
    function srcAppCorridasCorridasRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CorridasPageRoutingModule", function () {
        return CorridasPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _corridas_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./corridas.page */
      "./src/app/corridas/corridas.page.ts");

      var routes = [{
        path: '',
        component: _corridas_page__WEBPACK_IMPORTED_MODULE_3__["CorridasPage"]
      }];

      var CorridasPageRoutingModule = function CorridasPageRoutingModule() {
        _classCallCheck(this, CorridasPageRoutingModule);
      };

      CorridasPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], CorridasPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/corridas/corridas.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/corridas/corridas.module.ts ***!
      \*********************************************/

    /*! exports provided: CorridasPageModule */

    /***/
    function srcAppCorridasCorridasModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CorridasPageModule", function () {
        return CorridasPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _corridas_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./corridas-routing.module */
      "./src/app/corridas/corridas-routing.module.ts");
      /* harmony import */


      var _corridas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./corridas.page */
      "./src/app/corridas/corridas.page.ts");

      var CorridasPageModule = function CorridasPageModule() {
        _classCallCheck(this, CorridasPageModule);
      };

      CorridasPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _corridas_routing_module__WEBPACK_IMPORTED_MODULE_5__["CorridasPageRoutingModule"]],
        declarations: [_corridas_page__WEBPACK_IMPORTED_MODULE_6__["CorridasPage"]]
      })], CorridasPageModule);
      /***/
    },

    /***/
    "./src/app/corridas/corridas.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/corridas/corridas.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppCorridasCorridasPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-card, img {\n  border-radius: 5%;\n  border: #001c4e 0.5px solid;\n}\n\nion-toolbar {\n  --background: #001c4e;\n}\n\nion-grid {\n  margin: auto;\n  display: block;\n  width: 90%;\n  border: 1px solid;\n  border-left: none;\n  border-right: none;\n}\n\nion-title {\n  text-align: center;\n  color: #001c4e;\n  font-size: 1.5em;\n  display: block;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29ycmlkYXMvY29ycmlkYXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSwyQkFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7RUFDQSxjQUFBO0VBQ0EsVUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksa0JBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9jb3JyaWRhcy9jb3JyaWRhcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZCwgaW1nIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUlO1xyXG4gICAgYm9yZGVyOiAjMDAxYzRlIDAuNXB4IHNvbGlkO1xyXG59XHJcblxyXG5pb24tdG9vbGJhciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGU7XHJcbn0gXHJcblxyXG5pb24tZ3JpZCB7XHJcbiAgICBtYXJnaW46IGF1dG87IFxyXG4gICAgZGlzcGxheTogYmxvY2s7IFxyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgYm9yZGVyLWxlZnQ6IG5vbmU7XHJcbiAgICBib3JkZXItcmlnaHQ6IG5vbmU7XHJcbn1cclxuXHJcbmlvbi10aXRsZSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7IFxyXG4gICAgY29sb3I6ICMwMDFjNGU7XHJcbiAgICBmb250LXNpemU6IDEuNWVtO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/corridas/corridas.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/corridas/corridas.page.ts ***!
      \*******************************************/

    /*! exports provided: CorridasPage */

    /***/
    function srcAppCorridasCorridasPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CorridasPage", function () {
        return CorridasPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/servico/motorista_liberacao */
      "./src/servico/motorista_liberacao.ts");

      var CorridasPage = /*#__PURE__*/function () {
        function CorridasPage(corridasService, modalCtrl) {
          var _this = this;

          _classCallCheck(this, CorridasPage);

          this.corridasService = corridasService;
          this.modalCtrl = modalCtrl;
          var d = new Date();
          var data = d.getDate();
          var month = parseInt(d.getMonth() + '') + 1;

          if (month == 1) {
            var m = '01';
          } else if (month == 2) {
            var m = '02';
          } else if (month == 3) {
            var m = '03';
          } else if (month == 4) {
            var m = '04';
          } else if (month == 5) {
            var m = '05';
          } else if (month == 6) {
            var m = '06';
          } else if (month == 7) {
            var m = '07';
          } else if (month == 8) {
            var m = '08';
          } else if (month == 9) {
            var m = '09';
          } else {
            var m = '' + month;
          }

          if (d.getDate() == 1) {
            var h = '01';
          } else if (d.getDate() == 2) {
            var h = '02';
          } else if (d.getDate() == 3) {
            var h = '03';
          } else if (d.getDate() == 4) {
            var h = '04';
          } else if (d.getDate() == 5) {
            var h = '05';
          } else if (d.getDate() == 6) {
            var h = '06';
          } else if (d.getDate() == 7) {
            var h = '07';
          } else if (d.getDate() == 8) {
            var h = '08';
          } else if (d.getDate() == 9) {
            var h = '09';
          } else {
            var h = '' + d.getDate();
          }

          var date = d.getFullYear() + "-" + m + '-' + h;
          this.corridasService.corridaCM(date).subscribe(function (totalcm) {
            _this.totalCm = totalcm;
            _this.canceladoMotorista = _this.totalCm.total_viagem_cm;
          });
          this.corridasService.corridaCP(date).subscribe(function (totalcp) {
            _this.totalCp = totalcp;
            _this.canceladoPassageiro = _this.totalCp.total_viagem_cp;
          });
          this.corridasService.corridaF(date).subscribe(function (totalf) {
            _this.totalf = totalf;
            _this.finalizadas = _this.totalf.total_viagem_f;
          });
        }

        _createClass(CorridasPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return CorridasPage;
      }();

      CorridasPage.ctorParameters = function () {
        return [{
          type: src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_3__["MotoristaLiberacaoService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }];
      };

      CorridasPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-corridas',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./corridas.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/corridas/corridas.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./corridas.page.scss */
        "./src/app/corridas/corridas.page.scss"))["default"]]
      })], CorridasPage);
      /***/
    },

    /***/
    "./src/servico/motorista_liberacao.ts":
    /*!********************************************!*\
      !*** ./src/servico/motorista_liberacao.ts ***!
      \********************************************/

    /*! exports provided: MotoristaLiberacaoService */

    /***/
    function srcServicoMotorista_liberacaoTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MotoristaLiberacaoService", function () {
        return MotoristaLiberacaoService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var API_URL = "https://uaileva.com.br/api";

      var MotoristaLiberacaoService = /*#__PURE__*/function () {
        function MotoristaLiberacaoService(http) {
          _classCallCheck(this, MotoristaLiberacaoService);

          this.http = http;
        }

        _createClass(MotoristaLiberacaoService, [{
          key: "liberar",
          value: function liberar() {
            return this.http.get(API_URL + "/motorista/liberacao");
          }
        }, {
          key: "motoristaUnico",
          value: function motoristaUnico(motorista_id) {
            return this.http.get(API_URL + "/motorista/unico/" + motorista_id);
          }
        }, {
          key: "fotoMotorista",
          value: function fotoMotorista(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/" + mot_phone);
          }
        }, {
          key: "fotoCrlv",
          value: function fotoCrlv(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/crlv/" + mot_phone);
          }
        }, {
          key: "fotoCnh",
          value: function fotoCnh(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/cnh/" + mot_phone);
          }
        }, {
          key: "carro",
          value: function carro(id_motorista, placa, modelo) {
            return this.http.get(API_URL + "/carro/signup/" + id_motorista + "/" + placa + "/" + modelo + "");
          }
        }, {
          key: "updateNome",
          value: function updateNome(user_id, nome, secondName) {
            return this.http.post("".concat(API_URL, "/motorista/updateNome"), {
              user_id: user_id,
              nome: nome,
              secondName: secondName
            });
          }
        }, {
          key: "liberarMotorista",
          value: function liberarMotorista(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/LIBERADO");
          }
        }, {
          key: "motoristaLiberado",
          value: function motoristaLiberado() {
            return this.http.get(API_URL + "/motoristaLiberado");
          }
        }, {
          key: "bloqueio",
          value: function bloqueio(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/BLOQUEADO");
          }
        }, {
          key: "bloqueioPassageiro",
          value: function bloqueioPassageiro(phone) {
            return this.http.get(API_URL + "/user/bloqueio/" + phone + "/BLOQUEADO");
          }
        }, {
          key: "corridaCM",
          value: function corridaCM(data) {
            console.log(data);
            return this.http.get(API_URL + "/corrida/diariacm/" + data + "");
          }
        }, {
          key: "corridaCP",
          value: function corridaCP(data) {
            return this.http.get(API_URL + "/corrida/diariacp/" + data + "");
          }
        }, {
          key: "corridaF",
          value: function corridaF(data) {
            return this.http.get(API_URL + "/corrida/diariaf/" + data + "");
          }
        }, {
          key: "corridaTotal",
          value: function corridaTotal() {
            return this.http.get(API_URL + "/totalCorridas");
          }
        }, {
          key: "motoristaLiberadoUnico",
          value: function motoristaLiberadoUnico(motorista_id) {
            return this.http.get(API_URL + "/motorista/liberadoUnico/" + motorista_id);
          }
        }, {
          key: "arrecadacao",
          value: function arrecadacao(motorista_id) {
            return this.http.get(API_URL + "/totalarrecadado/" + motorista_id);
          }
        }, {
          key: "desbloqueio",
          value: function desbloqueio(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/LIBERADO");
          }
        }, {
          key: "motbloqueado",
          value: function motbloqueado() {
            return this.http.get(API_URL + "/motorista/bloqueado");
          }
        }]);

        return MotoristaLiberacaoService;
      }();

      MotoristaLiberacaoService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      MotoristaLiberacaoService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], MotoristaLiberacaoService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=corridas-corridas-module-es5.js.map
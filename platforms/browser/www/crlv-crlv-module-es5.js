(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["crlv-crlv-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/crlv/crlv.page.html":
    /*!***************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/crlv/crlv.page.html ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppCrlvCrlvPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button text=\"\"  defaultHref=\"etapas\" color=\"light\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <h1>Tire uma foto do(a) Certificado de Registro e Licenciamento de Veículo - CRLV</h1>\r\n  <br>\r\n  <div>\r\n    <p class=\"termo\">\r\n      Lembre-se, é preciso que: 1. A foto mostre todo o documento, como na imagem abaixo. \r\n      2. Todos os campos estejam legíveis. 3. A categoria do veículo apareça como \"Particular\".\r\n      4. O veículo não seja anterior a 2010. Obs: O CRLV eletrônico é aceito regularmente.\r\n    </p>\r\n  </div>\r\n\r\n  <img src=\"/assets/img/exemplo-crlv.jpg\"><br>\r\n\r\n  <br><br>\r\n\r\n  <!--<script type=\"text/javascript\">\r\n    $fileName = document.getElementById('file-name');\r\n\r\n    $input.addEventListener('change', function(){\r\n      $fileName.textContent = this.value;\r\n    });\r\n  </script>\r\n\r\n  <div class='input-wrapper'>\r\n    <label for='input-file'>\r\n      Selecionar um arquivo\r\n    </label>\r\n    <input id='input-file' type='file' value='' (change)= \"file = $event.target.files[0]\" accept=\"image/*\"/>\r\n    <span id='file-name' style=\"color: black;\"></span>\r\n  </div>\r\n\r\n  <label>Selecionar Foto</label>-->\r\n  \r\n  <input type=\"file\" (change)= \"file = $event.target.files[0]\" accept=\"image/*\"><br>\r\n  <ion-button class=\"botao\" (click)= \"salvarFoto();\">Enviar foto</ion-button>\r\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/crlv/crlv-routing.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/crlv/crlv-routing.module.ts ***!
      \*********************************************/

    /*! exports provided: CrlvPageRoutingModule */

    /***/
    function srcAppCrlvCrlvRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CrlvPageRoutingModule", function () {
        return CrlvPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _crlv_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./crlv.page */
      "./src/app/crlv/crlv.page.ts");

      var routes = [{
        path: '',
        component: _crlv_page__WEBPACK_IMPORTED_MODULE_3__["CrlvPage"]
      }];

      var CrlvPageRoutingModule = function CrlvPageRoutingModule() {
        _classCallCheck(this, CrlvPageRoutingModule);
      };

      CrlvPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], CrlvPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/crlv/crlv.module.ts":
    /*!*************************************!*\
      !*** ./src/app/crlv/crlv.module.ts ***!
      \*************************************/

    /*! exports provided: CrlvPageModule */

    /***/
    function srcAppCrlvCrlvModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CrlvPageModule", function () {
        return CrlvPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _crlv_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./crlv-routing.module */
      "./src/app/crlv/crlv-routing.module.ts");
      /* harmony import */


      var _crlv_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./crlv.page */
      "./src/app/crlv/crlv.page.ts");

      var CrlvPageModule = function CrlvPageModule() {
        _classCallCheck(this, CrlvPageModule);
      };

      CrlvPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _crlv_routing_module__WEBPACK_IMPORTED_MODULE_5__["CrlvPageRoutingModule"]],
        declarations: [_crlv_page__WEBPACK_IMPORTED_MODULE_6__["CrlvPage"]]
      })], CrlvPageModule);
      /***/
    },

    /***/
    "./src/app/crlv/crlv.page.scss":
    /*!*************************************!*\
      !*** ./src/app/crlv/crlv.page.scss ***!
      \*************************************/

    /*! exports provided: default */

    /***/
    function srcAppCrlvCrlvPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --background: var(--ion-color-light);\n  --background: url(\"/assets/img/bg-uai-leva2.png\") #eaeaea no-repeat bottom 75px left 45px;\n}\n\nion-toolbar {\n  --background: #001c4e !important;\n}\n\nion-header {\n  --background: #001c4e !important;\n}\n\nion-item {\n  --ion-item-background: transparent;\n  --border-style: var(--border-style);\n  --border: 0 none;\n  --box-shadow: 0 0 0 0;\n  --outline: 0;\n}\n\nion-icon {\n  color: gray;\n}\n\n/*input[type=\"file\"]{\n    display: none;\n}\n\nlabel {\n    background: #b1ff49;  //#001c4e;\n    color: #001c4e; //white;\n    font-weight: bold;\n    display: block;\n    height: 48px;\n    width: 95%;\n    margin: auto;\n    border-radius: 50px;\n    text-align: center;\n    padding: 16px;\n    --box-shadow: 0 0 0 0; \n    outline: 0;\n    text-transform: uppercase;\n    font-size: .9em;\n    cursor: pointer;\n}*/\n\n/*input[type='file'] {\n    display: none\n  }\n\n  .input-wrapper label {\n    background-color: #3498db;\n    border-radius: 5px;\n    color: #fff;\n    margin: 10px;\n    padding: 6px 20px\n  }\n\n  .input-wrapper label:hover {\n    background-color: #2980b9\n  }\n\nimg {\n    height: 300px;\n    width: 300px;\n    display: block;\n    margin: auto;\n}*/\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  margin: 15px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n\n.declaracao {\n  font-size: 1.1rem;\n  padding: 10px;\n  color: #123b7d;\n  text-align: justify;\n}\n\n.termo {\n  padding: 10px;\n  padding-top: 0px;\n  color: #123b7d;\n  margin-top: -20px;\n  text-align: justify;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY3Jsdi9jcmx2LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG9DQUFBO0VBQ0EseUZBQUE7QUFDSjs7QUFFQTtFQUNJLGdDQUFBO0FBQ0o7O0FBRUE7RUFDSSxnQ0FBQTtBQUNKOztBQUVBO0VBQ0ksa0NBQUE7RUFDQSxtQ0FBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0FBQ0o7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0VBQUE7O0FBc0JBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7RUFBQTs7QUF1QkE7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBRUEsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQUZKOztBQUtBO0VBQ0ksaUJBQUE7RUFFQSxhQUFBO0VBRUEsY0FBQTtFQUNBLG1CQUFBO0FBSko7O0FBT0E7RUFDSSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtBQUpKIiwiZmlsZSI6InNyYy9hcHAvY3Jsdi9jcmx2LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWcvYmctdWFpLWxldmEyLnBuZ1wiKSAjZWFlYWVhIG5vLXJlcGVhdCBib3R0b20gNzVweCBsZWZ0IDQ1cHg7XHJcbn1cclxuXHJcbmlvbi10b29sYmFyIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzAwMWM0ZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5pb24taGVhZGVyIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzAwMWM0ZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5pb24taXRlbSB7XHJcbiAgICAtLWlvbi1pdGVtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgLS1ib3JkZXItc3R5bGU6IHZhcigtLWJvcmRlci1zdHlsZSk7XHJcbiAgICAtLWJvcmRlcjogMCBub25lOyBcclxuICAgIC0tYm94LXNoYWRvdzogMCAwIDAgMDsgXHJcbiAgICAtLW91dGxpbmU6IDA7XHJcbn1cclxuXHJcbmlvbi1pY29uIHtcclxuICAgIGNvbG9yOiBncmF5O1xyXG59XHJcblxyXG4vKmlucHV0W3R5cGU9XCJmaWxlXCJde1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxubGFiZWwge1xyXG4gICAgYmFja2dyb3VuZDogI2IxZmY0OTsgIC8vIzAwMWM0ZTtcclxuICAgIGNvbG9yOiAjMDAxYzRlOyAvL3doaXRlO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGhlaWdodDogNDhweDtcclxuICAgIHdpZHRoOiA5NSU7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgcGFkZGluZzogMTZweDtcclxuICAgIC0tYm94LXNoYWRvdzogMCAwIDAgMDsgXHJcbiAgICBvdXRsaW5lOiAwO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIGZvbnQtc2l6ZTogLjllbTtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufSovXHJcblxyXG4vKmlucHV0W3R5cGU9J2ZpbGUnXSB7XHJcbiAgICBkaXNwbGF5OiBub25lXHJcbiAgfVxyXG4gIFxyXG4gIC5pbnB1dC13cmFwcGVyIGxhYmVsIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzNDk4ZGI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIG1hcmdpbjogMTBweDtcclxuICAgIHBhZGRpbmc6IDZweCAyMHB4XHJcbiAgfVxyXG4gIFxyXG4gIC5pbnB1dC13cmFwcGVyIGxhYmVsOmhvdmVyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMyOTgwYjlcclxuICB9XHJcblxyXG5pbWcge1xyXG4gICAgaGVpZ2h0OiAzMDBweDtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG59Ki9cclxuXHJcbmgxIHtcclxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgLy90ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgbWFyZ2luOiAxNXB4IDA7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGNvbG9yOiAjMTIzYjdkO1xyXG59XHJcblxyXG4uZGVjbGFyYWNhbyB7XHJcbiAgICBmb250LXNpemU6IDEuMXJlbTtcclxuICAgIC8vZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgLy9wYWRkaW5nLWJvdHRvbTogMHB4O1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xyXG59XHJcblxyXG4udGVybW8ge1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIHBhZGRpbmctdG9wOiAwcHg7XHJcbiAgICBjb2xvcjogIzEyM2I3ZDtcclxuICAgIG1hcmdpbi10b3A6IC0yMHB4O1xyXG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcclxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/crlv/crlv.page.ts":
    /*!***********************************!*\
      !*** ./src/app/crlv/crlv.page.ts ***!
      \***********************************/

    /*! exports provided: CrlvPage */

    /***/
    function srcAppCrlvCrlvPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CrlvPage", function () {
        return CrlvPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var CrlvPage = /*#__PURE__*/function () {
        function CrlvPage() {
          _classCallCheck(this, CrlvPage);
        }

        _createClass(CrlvPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return CrlvPage;
      }();

      CrlvPage.ctorParameters = function () {
        return [];
      };

      CrlvPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-crlv',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./crlv.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/crlv/crlv.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./crlv.page.scss */
        "./src/app/crlv/crlv.page.scss"))["default"]]
      })], CrlvPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=crlv-crlv-module-es5.js.map
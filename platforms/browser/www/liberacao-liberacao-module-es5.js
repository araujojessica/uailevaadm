(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["liberacao-liberacao-module"], {
    /***/
    "./node_modules/jwt-decode/build/jwt-decode.esm.js":
    /*!*********************************************************!*\
      !*** ./node_modules/jwt-decode/build/jwt-decode.esm.js ***!
      \*********************************************************/

    /*! exports provided: default, InvalidTokenError */

    /***/
    function node_modulesJwtDecodeBuildJwtDecodeEsmJs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "InvalidTokenError", function () {
        return n;
      });

      function e(e) {
        this.message = e;
      }

      e.prototype = new Error(), e.prototype.name = "InvalidCharacterError";

      var r = "undefined" != typeof window && window.atob && window.atob.bind(window) || function (r) {
        var t = String(r).replace(/=+$/, "");
        if (t.length % 4 == 1) throw new e("'atob' failed: The string to be decoded is not correctly encoded.");

        for (var n, o, a = 0, i = 0, c = ""; o = t.charAt(i++); ~o && (n = a % 4 ? 64 * n + o : o, a++ % 4) ? c += String.fromCharCode(255 & n >> (-2 * a & 6)) : 0) {
          o = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(o);
        }

        return c;
      };

      function t(e) {
        var t = e.replace(/-/g, "+").replace(/_/g, "/");

        switch (t.length % 4) {
          case 0:
            break;

          case 2:
            t += "==";
            break;

          case 3:
            t += "=";
            break;

          default:
            throw "Illegal base64url string!";
        }

        try {
          return function (e) {
            return decodeURIComponent(r(e).replace(/(.)/g, function (e, r) {
              var t = r.charCodeAt(0).toString(16).toUpperCase();
              return t.length < 2 && (t = "0" + t), "%" + t;
            }));
          }(t);
        } catch (e) {
          return r(t);
        }
      }

      function n(e) {
        this.message = e;
      }

      function o(e, r) {
        if ("string" != typeof e) throw new n("Invalid token specified");
        var o = !0 === (r = r || {}).header ? 0 : 1;

        try {
          return JSON.parse(t(e.split(".")[o]));
        } catch (e) {
          throw new n("Invalid token specified: " + e.message);
        }
      }

      n.prototype = new Error(), n.prototype.name = "InvalidTokenError";
      /* harmony default export */

      __webpack_exports__["default"] = o; //# sourceMappingURL=jwt-decode.esm.js.map

      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/liberacao/liberacao.page.html":
    /*!*************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/liberacao/liberacao.page.html ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppLiberacaoLiberacaoPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"lista-motoristas\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <br><br>\r\n  <ion-button style=\"width: 25%;\"(click)=\"foto()\" class=\"botao\"><ion-icon name=\"checkmark-outline\" slot=\"end\" style=\"color: #001c4e;\"></ion-icon>add Fotos\r\n  </ion-button><br>\r\n\r\n\r\n  <div style=\"padding-left: 25px; padding-right: 25px;\">\r\n  <ion-grid>\r\n    <ion-col class=\"col-md-12\"><img src=\"{{url_foto}}\"/></ion-col>&nbsp;\r\n    <ion-col class=\"col-md-12\"><img src=\"{{url_cnh}}\"/></ion-col>&nbsp;\r\n    <ion-col class=\"col-md-12\"><img src=\"{{url_crlv}}\"/></ion-col>\r\n  </ion-grid>\r\n\r\n  <br>\r\n\r\n    <ion-grid>\r\n      <form>\r\n        <ion-row>\r\n          <ion-col>\r\n            <ion-item>\r\n              <ion-label position=\"stacked\">Nome Completo</ion-label>\r\n              <ion-input [(ngModel)]=\"nomeCompleto\" name = \"nomeCompleto\" placeholder=\"{{nome}}\"></ion-input>\r\n            </ion-item>\r\n          </ion-col>\r\n        </ion-row>\r\n\r\n        <ion-row>\r\n          <ion-col>\r\n            <ion-item>\r\n              <ion-label position=\"stacked\">Telefone</ion-label>\r\n              <ion-input readonly placeholder=\"{{phone}}\"></ion-input>\r\n            </ion-item>\r\n          </ion-col>\r\n          \r\n          <ion-col>\r\n            <ion-item>\r\n              <ion-label position=\"stacked\">E-mail</ion-label>\r\n              <ion-input readonly placeholder=\"{{email}}\"></ion-input>\r\n            </ion-item>\r\n          </ion-col>\r\n        </ion-row>\r\n\r\n        <ion-row>\r\n          <ion-col>\r\n            <ion-item>\r\n              <ion-label position=\"stacked\">Veículo</ion-label>\r\n              <ion-input readonly></ion-input>\r\n            </ion-item>\r\n          </ion-col>\r\n\r\n          <ion-col>\r\n            <ion-item>\r\n              <ion-label position=\"stacked\">Placa</ion-label>\r\n              <ion-input [(ngModel)]=\"placa\" name=\"placa\"></ion-input>\r\n            </ion-item>\r\n          </ion-col>\r\n\r\n          <ion-col>\r\n            <ion-item>\r\n              <ion-label position=\"stacked\">Modelo</ion-label>\r\n              <ion-input [(ngModel)]=\"modelo\" name = \"modelo\"></ion-input>\r\n            </ion-item>\r\n          </ion-col>\r\n        </ion-row>\r\n      </form>\r\n\r\n      <br> \r\n\r\n      <ion-button (click)=\"salvar(motorista_id)\" class=\"botao\">\r\n        <ion-icon name=\"checkmark-outline\" slot=\"end\" style=\"color: #001c4e;\"></ion-icon>Liberar\r\n      </ion-button>\r\n\r\n      <br>\r\n    </ion-grid>\r\n  </div>\r\n  <br>\r\n</ion-content>\r\n\r\n\r\n";
      /***/
    },

    /***/
    "./src/app/liberacao/liberacao-routing.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/liberacao/liberacao-routing.module.ts ***!
      \*******************************************************/

    /*! exports provided: LiberacaoPageRoutingModule */

    /***/
    function srcAppLiberacaoLiberacaoRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LiberacaoPageRoutingModule", function () {
        return LiberacaoPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _liberacao_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./liberacao.page */
      "./src/app/liberacao/liberacao.page.ts");

      var routes = [{
        path: '',
        component: _liberacao_page__WEBPACK_IMPORTED_MODULE_3__["LiberacaoPage"]
      }];

      var LiberacaoPageRoutingModule = function LiberacaoPageRoutingModule() {
        _classCallCheck(this, LiberacaoPageRoutingModule);
      };

      LiberacaoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LiberacaoPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/liberacao/liberacao.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/liberacao/liberacao.module.ts ***!
      \***********************************************/

    /*! exports provided: LiberacaoPageModule */

    /***/
    function srcAppLiberacaoLiberacaoModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LiberacaoPageModule", function () {
        return LiberacaoPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _liberacao_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./liberacao-routing.module */
      "./src/app/liberacao/liberacao-routing.module.ts");
      /* harmony import */


      var _liberacao_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./liberacao.page */
      "./src/app/liberacao/liberacao.page.ts");

      var LiberacaoPageModule = function LiberacaoPageModule() {
        _classCallCheck(this, LiberacaoPageModule);
      };

      LiberacaoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _liberacao_routing_module__WEBPACK_IMPORTED_MODULE_5__["LiberacaoPageRoutingModule"]],
        declarations: [_liberacao_page__WEBPACK_IMPORTED_MODULE_6__["LiberacaoPage"]]
      })], LiberacaoPageModule);
      /***/
    },

    /***/
    "./src/app/liberacao/liberacao.page.scss":
    /*!***********************************************!*\
      !*** ./src/app/liberacao/liberacao.page.scss ***!
      \***********************************************/

    /*! exports provided: default */

    /***/
    function srcAppLiberacaoLiberacaoPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "img {\n  border-radius: 5%;\n  max-width: 85%;\n}\n\nion-item {\n  --highlight-height: 1px;\n  outline: none !important;\n}\n\nion-toolbar {\n  --background: #001c4e;\n}\n\nion-title {\n  text-align: center;\n  color: #001c4e;\n  font-size: 1.5em;\n  display: block;\n}\n\nion-card {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\nion-card-title, ion-card-subtitle, ion-card-content {\n  text-align: center;\n  box-shadow: none;\n}\n\nion-grid {\n  display: inline-block;\n  margin: auto;\n  text-align: center;\n  width: 100%;\n  border: 1px solid;\n  border-left: none;\n  border-right: none;\n}\n\nion-col {\n  padding: 0px;\n}\n\nion-label {\n  font-weight: bold;\n  --highlight-color-focused: var(--ion-color-primary, #001c4e);\n}\n\nimg {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n}\n\n.botao {\n  --background: #b1ff49;\n  --color: #001c4e;\n  font-weight: bold;\n  display: block;\n  height: 48px;\n  width: 45%;\n  margin: auto;\n  text-transform: uppercase;\n  --border-radius: 50px;\n  --background-activated: #001c4e;\n  --color-activated: white;\n  --background-hover: #001c4e;\n  --color-hover: white;\n  --box-shadow: 0 0 0 0;\n  outline: none !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGliZXJhY2FvL2xpYmVyYWNhby5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBQTtFQUVBLGNBQUE7QUFBSjs7QUFFQTtFQUNJLHVCQUFBO0VBQ0Esd0JBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUNJLDBCQUFBO0VBQUEsdUJBQUE7RUFBQSxrQkFBQTtBQUNKOztBQUNBO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtBQUVKOztBQUNBO0VBQ0kscUJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBRUo7O0FBQ0E7RUFDSSxZQUFBO0FBRUo7O0FBQ0E7RUFDSSxpQkFBQTtFQUNBLDREQUFBO0FBRUo7O0FBRUE7RUFDSSwwQkFBQTtFQUFBLHVCQUFBO0VBQUEsa0JBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLHFCQUFBO0VBQ0EsK0JBQUE7RUFDQSx3QkFBQTtFQUNBLDJCQUFBO0VBQ0Esb0JBQUE7RUFDRCxxQkFBQTtFQUNDLHdCQUFBO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9saWJlcmFjYW8vbGliZXJhY2FvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImltZyB7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1JTtcclxuICAgIC8vYm9yZGVyOiAjMDAxYzRlIDAuNXB4IHNvbGlkO1xyXG4gICAgbWF4LXdpZHRoOiA4NSU7ICBcclxufVxyXG5pb24taXRlbSB7XHJcbiAgICAtLWhpZ2hsaWdodC1oZWlnaHQ6IDFweDtcclxuICAgIG91dGxpbmU6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLXRvb2xiYXIge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMDAxYzRlO1xyXG59XHJcblxyXG5pb24tdGl0bGUge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyOyBcclxuICAgIGNvbG9yOiAjMDAxYzRlO1xyXG4gICAgZm9udC1zaXplOiAxLjVlbTtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG5pb24tY2FyZCB7XHJcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XHJcbn1cclxuaW9uLWNhcmQtdGl0bGUsIGlvbi1jYXJkLXN1YnRpdGxlLCBpb24tY2FyZC1jb250ZW50IHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbn1cclxuXHJcbmlvbi1ncmlkIHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICBib3JkZXItbGVmdDogbm9uZTtcclxuICAgIGJvcmRlci1yaWdodDogbm9uZTtcclxufVxyXG5cclxuaW9uLWNvbCB7XHJcbiAgICBwYWRkaW5nOiAwcHg7XHJcbn1cclxuXHJcbmlvbi1sYWJlbCB7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIC0taGlnaGxpZ2h0LWNvbG9yLWZvY3VzZWQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LCAjMDAxYzRlKTtcclxufVxyXG5cclxuXHJcbmltZyB7XHJcbiAgICB3aWR0aDogZml0LWNvbnRlbnQ7XHJcbiB9XHJcblxyXG4uYm90YW8ge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjYjFmZjQ5OyAgLy8jMDAxYzRlO1xyXG4gICAgLS1jb2xvcjogIzAwMWM0ZTsgLy93aGl0ZTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBoZWlnaHQ6IDQ4cHg7XHJcbiAgICB3aWR0aDogNDUlO1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6ICMwMDFjNGU7XHJcbiAgICAtLWNvbG9yLWFjdGl2YXRlZDogd2hpdGU7XHJcbiAgICAtLWJhY2tncm91bmQtaG92ZXI6ICMwMDFjNGU7XHJcbiAgICAtLWNvbG9yLWhvdmVyOiB3aGl0ZTtcclxuICAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xyXG4gICAgb3V0bGluZTogbm9uZSAhaW1wb3J0YW50O1xyXG59Il19 */";
      /***/
    },

    /***/
    "./src/app/liberacao/liberacao.page.ts":
    /*!*********************************************!*\
      !*** ./src/app/liberacao/liberacao.page.ts ***!
      \*********************************************/

    /*! exports provided: LiberacaoPage */

    /***/
    function srcAppLiberacaoLiberacaoPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LiberacaoPage", function () {
        return LiberacaoPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/core/user/user.service */
      "./src/core/user/user.service.ts");
      /* harmony import */


      var src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/servico/motorista_liberacao */
      "./src/servico/motorista_liberacao.ts");

      var LiberacaoPage = /*#__PURE__*/function () {
        function LiberacaoPage(router, route, motoristaService, motoristaLiberacaoService, toastCtrl) {
          var _this = this;

          _classCallCheck(this, LiberacaoPage);

          this.router = router;
          this.route = route;
          this.motoristaService = motoristaService;
          this.motoristaLiberacaoService = motoristaLiberacaoService;
          this.toastCtrl = toastCtrl;
          this.url_foto = 'assets/img/no.png';
          this.url_crlv = 'assets/img/no.png';
          this.url_cnh = 'assets/img/no.png';
          this.placa = '';
          this.modelo = '';
          this.user$ = motoristaService.getUser();
          this.route.params.subscribe(function (parametros) {
            _this.motorista_id = parametros['user_id'];

            _this.motoristaLiberacaoService.motoristaUnico(_this.motorista_id).subscribe(function (motorista) {
              _this.mot = motorista;
              _this.nome = motorista.full_name;
              _this.phone = _this.mot.phone;
              _this.email = _this.mot.email;

              _this.motoristaLiberacaoService.fotoMotorista(_this.mot.phone).subscribe(function (foto_motorista) {
                _this.photo = foto_motorista;
                _this.url_foto = "https://uaileva.com.br/api/imgs/" + _this.photo.url;
              });

              _this.motoristaLiberacaoService.fotoCrlv(_this.mot.phone).subscribe(function (foto_crlv) {
                _this.photo_crlv = foto_crlv;
                _this.url_crlv = "https://uaileva.com.br/api/imgs/" + _this.photo_crlv.url;
              });

              _this.motoristaLiberacaoService.fotoCnh(_this.mot.phone).subscribe(function (foto_cnh) {
                _this.photo_cnh = foto_cnh;
                _this.url_cnh = "https://uaileva.com.br/api/imgs/" + _this.photo_cnh.url;
              });
            });
          });
          this.user$.subscribe(function (usuario) {
            _this.usuarioLogado = usuario;
            _this.user_id = _this.usuarioLogado.id;

            var a = _this.usuarioLogado.full_name.split(' ');

            if (_this.usuarioLogado.apelido == null) {
              _this.apelido = '';
            } else {
              _this.apelido = '(' + _this.usuarioLogado.apelido + ')';
            }

            _this.nome = a[0] + ' ' + _this.apelido;
          });
          this.motoristaLiberacaoService.liberar().subscribe(function (motoristaLiberado) {
            _this.m = motoristaLiberado;
          });
        }

        _createClass(LiberacaoPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "salvar",
          value: function salvar(motorista_id) {
            var _this2 = this;

            if (this.nomeCompleto != '' || this.nomeCompleto != null) {
              this.motoristaLiberacaoService.updateNome(this.user_id, this.nomeCompleto, '').subscribe();
            }

            if (this.placa != '' || this.modelo != '') {
              this.motoristaLiberacaoService.carro(motorista_id, this.placa, this.modelo).subscribe();
            }

            this.motoristaLiberacaoService.liberarMotorista(this.phone, this.email).subscribe(function () {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                var toast;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        _context.next = 2;
                        return this.toastCtrl.create({
                          message: 'Motorista Liberado!',
                          duration: 4000,
                          position: 'top',
                          color: 'success'
                        });

                      case 2:
                        _context.next = 4;
                        return _context.sent.present();

                      case 4:
                        toast = _context.sent;

                      case 5:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));
            }, function (err) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                var toast;
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        console.log(err);
                        _context2.next = 3;
                        return this.toastCtrl.create({
                          message: 'Por favor confira se a sua internet está funcionando e tente cadastrar novamente! UaiLeva agradece!',
                          duration: 4000,
                          position: 'top',
                          color: 'danger'
                        });

                      case 3:
                        _context2.next = 5;
                        return _context2.sent.present();

                      case 5:
                        toast = _context2.sent;

                      case 6:
                      case "end":
                        return _context2.stop();
                    }
                  }
                }, _callee2, this);
              }));
            });
          }
        }, {
          key: "foto",
          value: function foto() {
            window.open('https://motorista.uaileva.com.br/#/etapas/' + this.nome + '/' + this.phone + '');
          }
        }]);

        return LiberacaoPage;
      }();

      LiberacaoPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: src_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]
        }, {
          type: src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_5__["MotoristaLiberacaoService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
        }];
      };

      LiberacaoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-liberacao',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./liberacao.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/liberacao/liberacao.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./liberacao.page.scss */
        "./src/app/liberacao/liberacao.page.scss"))["default"]]
      })], LiberacaoPage);
      /***/
    },

    /***/
    "./src/core/token/token.service.ts":
    /*!*****************************************!*\
      !*** ./src/core/token/token.service.ts ***!
      \*****************************************/

    /*! exports provided: TokenService */

    /***/
    function srcCoreTokenTokenServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TokenService", function () {
        return TokenService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var KEY = 'authToken';

      var TokenService = /*#__PURE__*/function () {
        function TokenService() {
          _classCallCheck(this, TokenService);
        }

        _createClass(TokenService, [{
          key: "hasToken",
          value: function hasToken() {
            return !!this.getToken();
          }
        }, {
          key: "setToken",
          value: function setToken(token) {
            window.localStorage.setItem(KEY, token);
          }
        }, {
          key: "getToken",
          value: function getToken() {
            return window.localStorage.getItem(KEY);
          }
        }, {
          key: "removeToken",
          value: function removeToken() {
            window.localStorage.removeItem(KEY);
          }
        }]);

        return TokenService;
      }();

      TokenService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], TokenService);
      /***/
    },

    /***/
    "./src/core/user/user.service.ts":
    /*!***************************************!*\
      !*** ./src/core/user/user.service.ts ***!
      \***************************************/

    /*! exports provided: UserService */

    /***/
    function srcCoreUserUserServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserService", function () {
        return UserService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _token_token_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../token/token.service */
      "./src/core/token/token.service.ts");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var jwt_decode__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! jwt-decode */
      "./node_modules/jwt-decode/build/jwt-decode.esm.js"); //import * as jwt_decode from 'jwt-decode';


      var UserService = /*#__PURE__*/function () {
        function UserService(tokenService) {
          _classCallCheck(this, UserService);

          this.tokenService = tokenService;
          this.userSubject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](null);
          this.tokenService.hasToken() && this.decodeAndNotify();
        }

        _createClass(UserService, [{
          key: "setToken",
          value: function setToken(token) {
            this.tokenService.setToken(token);
            this.decodeAndNotify();
          }
        }, {
          key: "getUser",
          value: function getUser() {
            return this.userSubject.asObservable();
          }
        }, {
          key: "decodeAndNotify",
          value: function decodeAndNotify() {
            var token = this.tokenService.getToken();
            var user = Object(jwt_decode__WEBPACK_IMPORTED_MODULE_4__["default"])(token);
            this.fullName = user.full_name;
            this.userSubject.next(user);
          }
        }, {
          key: "logout",
          value: function logout() {
            this.tokenService.removeToken();
            this.userSubject.next(null);
          }
        }, {
          key: "isLogged",
          value: function isLogged() {
            return this.tokenService.hasToken();
          }
        }, {
          key: "getUserName",
          value: function getUserName() {
            return this.fullName;
          }
        }]);

        return UserService;
      }();

      UserService.ctorParameters = function () {
        return [{
          type: _token_token_service__WEBPACK_IMPORTED_MODULE_2__["TokenService"]
        }];
      };

      UserService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], UserService);
      /***/
    },

    /***/
    "./src/servico/motorista_liberacao.ts":
    /*!********************************************!*\
      !*** ./src/servico/motorista_liberacao.ts ***!
      \********************************************/

    /*! exports provided: MotoristaLiberacaoService */

    /***/
    function srcServicoMotorista_liberacaoTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MotoristaLiberacaoService", function () {
        return MotoristaLiberacaoService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var API_URL = "https://uaileva.com.br/api";

      var MotoristaLiberacaoService = /*#__PURE__*/function () {
        function MotoristaLiberacaoService(http) {
          _classCallCheck(this, MotoristaLiberacaoService);

          this.http = http;
        }

        _createClass(MotoristaLiberacaoService, [{
          key: "liberar",
          value: function liberar() {
            return this.http.get(API_URL + "/motorista/liberacao");
          }
        }, {
          key: "motoristaUnico",
          value: function motoristaUnico(motorista_id) {
            return this.http.get(API_URL + "/motorista/unico/" + motorista_id);
          }
        }, {
          key: "fotoMotorista",
          value: function fotoMotorista(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/" + mot_phone);
          }
        }, {
          key: "fotoCrlv",
          value: function fotoCrlv(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/crlv/" + mot_phone);
          }
        }, {
          key: "fotoCnh",
          value: function fotoCnh(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/cnh/" + mot_phone);
          }
        }, {
          key: "carro",
          value: function carro(id_motorista, placa, modelo) {
            return this.http.get(API_URL + "/carro/signup/" + id_motorista + "/" + placa + "/" + modelo + "");
          }
        }, {
          key: "updateNome",
          value: function updateNome(user_id, nome, secondName) {
            return this.http.post("".concat(API_URL, "/motorista/updateNome"), {
              user_id: user_id,
              nome: nome,
              secondName: secondName
            });
          }
        }, {
          key: "liberarMotorista",
          value: function liberarMotorista(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/LIBERADO");
          }
        }, {
          key: "motoristaLiberado",
          value: function motoristaLiberado() {
            return this.http.get(API_URL + "/motoristaLiberado");
          }
        }, {
          key: "bloqueio",
          value: function bloqueio(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/BLOQUEADO");
          }
        }, {
          key: "bloqueioPassageiro",
          value: function bloqueioPassageiro(phone) {
            return this.http.get(API_URL + "/user/bloqueio/" + phone + "/BLOQUEADO");
          }
        }, {
          key: "corridaCM",
          value: function corridaCM(data) {
            console.log(data);
            return this.http.get(API_URL + "/corrida/diariacm/" + data + "");
          }
        }, {
          key: "corridaCP",
          value: function corridaCP(data) {
            return this.http.get(API_URL + "/corrida/diariacp/" + data + "");
          }
        }, {
          key: "corridaF",
          value: function corridaF(data) {
            return this.http.get(API_URL + "/corrida/diariaf/" + data + "");
          }
        }, {
          key: "corridaTotal",
          value: function corridaTotal() {
            return this.http.get(API_URL + "/totalCorridas");
          }
        }, {
          key: "motoristaLiberadoUnico",
          value: function motoristaLiberadoUnico(motorista_id) {
            return this.http.get(API_URL + "/motorista/liberadoUnico/" + motorista_id);
          }
        }, {
          key: "arrecadacao",
          value: function arrecadacao(motorista_id) {
            return this.http.get(API_URL + "/totalarrecadado/" + motorista_id);
          }
        }, {
          key: "desbloqueio",
          value: function desbloqueio(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/LIBERADO");
          }
        }, {
          key: "motbloqueado",
          value: function motbloqueado() {
            return this.http.get(API_URL + "/motorista/bloqueado");
          }
        }]);

        return MotoristaLiberacaoService;
      }();

      MotoristaLiberacaoService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      MotoristaLiberacaoService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], MotoristaLiberacaoService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=liberacao-liberacao-module-es5.js.map
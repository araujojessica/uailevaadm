(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~corrida-aberta-corrida-aberta-module~viagens-viagens-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/corrida-aberta/corrida-aberta.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/corrida-aberta/corrida-aberta.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header no-border>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"dismiss()\"><ion-icon name=\"close-outline\" style=\"color: white;\" size=\"large\"></ion-icon></ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-item>\r\n    <ion-button href=\"https://wa.me/5535998716904?text=UaiLeva%20Ajuda\" target=\"_blank\" slot=\"end\">\r\n      <ion-icon name=\"logo-whatsapp\" style=\"color: lime; font-size: 40px;\"></ion-icon>\r\n    </ion-button>\r\n  </ion-item>\r\n  <ion-grid>\r\n    <ion-item>\r\n      <ion-label><b>Corrida:</b> {{status}}</ion-label>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label> <b>Motorista:</b> {{motorista}}\r\n                  <p>Telefone: {{mottel}}</p>\r\n      </ion-label>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label> <b>Passageiro:</b> {{nome}}\r\n                  <p>Telefone: {{usertel}}</p>\r\n      </ion-label>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label><b>Valor da corrida:</b> R$ {{valor}} </ion-label>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label><b>Origem:</b> {{origem}}</ion-label>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label><b>Destino:</b> {{destino}}</ion-label>\r\n    </ion-item>\r\n\r\n    <ion-item mode=\"md\">\r\n    <ion-button class=\"btn-cancelar\" (click) = \"cancelar(id_viagem);\" slot=\"end\">CANCELAR</ion-button>\r\n    <ion-button class=\"btn-finalizar\" (click) = \"finalizar(id_viagem);\" slot=\"end\">FINALIZAR</ion-button>\r\n  </ion-item>\r\n  </ion-grid>\r\n  \r\n</ion-content>");

/***/ }),

/***/ "./src/app/corrida-aberta/corrida-aberta.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/corrida-aberta/corrida-aberta.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-card, img {\n  border-radius: 5%;\n  border: #001c4e 0.5px solid;\n  width: 25%;\n}\n\nion-toolbar {\n  --background: #001c4e;\n}\n\nion-title {\n  color: #fff;\n}\n\nion-item {\n  --ion-item-background: transparent;\n  --border-style: var(--border-style);\n  --border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n\nion-button {\n  --border-style: var(--border-style);\n  --border: 0 none;\n  --box-shadow: 0 0 0 0 !important;\n  outline: 0;\n  height: 45px;\n  --background: transparent;\n  --ion-item-background: transparent;\n  --background-activated: white;\n  --color-activated: lime;\n  --background-hover: white;\n  --color-hover: lime;\n}\n\n.btn-cancelar {\n  --background:red;\n  color: #fff;\n  font-weight: bold;\n  border-radius: 30px;\n  height: 35px;\n}\n\n.btn-finalizar {\n  --background:#b1ff49;\n  color: #001c4e;\n  font-weight: bold;\n  border-radius: 30px;\n  height: 35px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29ycmlkYS1hYmVydGEvY29ycmlkYS1hYmVydGEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSwyQkFBQTtFQUNBLFVBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0FBQ0o7O0FBR0E7RUFHSSxrQ0FBQTtFQUNBLG1DQUFBO0VBQ0EsZ0JBQUE7RUFDRCxtQkFBQTtFQUNDLFVBQUE7QUFGSjs7QUFLQTtFQUNJLG1DQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQ0FBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EseUJBQUE7RUFDQSxrQ0FBQTtFQUNBLDZCQUFBO0VBQ0EsdUJBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0FBRko7O0FBS0E7RUFDSSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQUZKOztBQUtBO0VBQ0ksb0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUFGSiIsImZpbGUiOiJzcmMvYXBwL2NvcnJpZGEtYWJlcnRhL2NvcnJpZGEtYWJlcnRhLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkLCBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNSU7XHJcbiAgICBib3JkZXI6ICMwMDFjNGUgMC41cHggc29saWQ7XHJcbiAgICB3aWR0aDogMjUlO1xyXG59XHJcblxyXG5pb24tdG9vbGJhciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGU7XHJcbn1cclxuXHJcbmlvbi10aXRsZSB7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxufVxyXG5cclxuIFxyXG5pb24taXRlbSB7XHJcbiAgICAvLy0tY29sb3I6IGJsYWNrO1xyXG4gICAgLy9kZWl4YXIgbWVudSBzZW0gbGluaGFzXHJcbiAgICAtLWlvbi1pdGVtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgLS1ib3JkZXItc3R5bGU6IHZhcigtLWJvcmRlci1zdHlsZSk7XHJcbiAgICAtLWJvcmRlcjogMCBub25lOyBcclxuICAgYm94LXNoYWRvdzogMCAwIDAgMDsgXHJcbiAgICBvdXRsaW5lOiAwO1xyXG59XHJcblxyXG5pb24tYnV0dG9ue1xyXG4gICAgLS1ib3JkZXItc3R5bGU6IHZhcigtLWJvcmRlci1zdHlsZSk7XHJcbiAgICAtLWJvcmRlcjogMCBub25lOyBcclxuICAgIC0tYm94LXNoYWRvdzogMCAwIDAgMCAhaW1wb3J0YW50OyBcclxuICAgIG91dGxpbmU6IDA7XHJcbiAgICBoZWlnaHQ6IDQ1cHg7XHJcbiAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgLS1pb24taXRlbS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6IHdoaXRlO1xyXG4gICAgLS1jb2xvci1hY3RpdmF0ZWQ6IGxpbWU7XHJcbiAgICAtLWJhY2tncm91bmQtaG92ZXI6IHdoaXRlO1xyXG4gICAgLS1jb2xvci1ob3ZlcjogbGltZTtcclxufVxyXG5cclxuLmJ0bi1jYW5jZWxhcntcclxuICAgIC0tYmFja2dyb3VuZDpyZWQ7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkOyBcclxuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgICBoZWlnaHQ6IDM1cHg7XHJcbn1cclxuXHJcbi5idG4tZmluYWxpemFyIHtcclxuICAgIC0tYmFja2dyb3VuZDojYjFmZjQ5O1xyXG4gICAgY29sb3I6ICMwMDFjNGU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDsgXHJcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgaGVpZ2h0OiAzNXB4O1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/corrida-aberta/corrida-aberta.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/corrida-aberta/corrida-aberta.page.ts ***!
  \*******************************************************/
/*! exports provided: CorridaAbertaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CorridaAbertaPage", function() { return CorridaAbertaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_servico_motorista_motorista__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/servico/motorista/motorista */ "./src/servico/motorista/motorista.ts");
/* harmony import */ var src_servico_usuario_usuario__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/servico/usuario/usuario */ "./src/servico/usuario/usuario.ts");
/* harmony import */ var src_servico_viagem_viagem__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/servico/viagem/viagem */ "./src/servico/viagem/viagem.ts");






let CorridaAbertaPage = class CorridaAbertaPage {
    constructor(modalCtrl, params, viagemService, usuarioService, motoristaService, toastCtrl) {
        this.modalCtrl = modalCtrl;
        this.params = params;
        this.viagemService = viagemService;
        this.usuarioService = usuarioService;
        this.motoristaService = motoristaService;
        this.toastCtrl = toastCtrl;
        this.id_viagem = this.params.get('viagem');
        this.viagemService.viagem(this.id_viagem).subscribe(viagem => {
            if (viagem.viagem_status == 'ACEITO') {
                this.status = 'MOTORISTA  A CAMINHO DO PASSAGEIRO';
            }
            if (viagem.viagem_status == 'INICIANDO') {
                this.status = 'MOTORISTA A CAMINHO DO DESTINO';
            }
            if (viagem.viagem_status == 'CHEGUEI') {
                this.status = 'MOTORISTA AGUARDANDO PASSAGEIRO';
            }
            this.mottel = viagem.phone;
            this.usertel = viagem.user_phone;
            this.origem = viagem.origem;
            this.destino = viagem.destino;
            this.valor = viagem.valor_corrida;
            this.id_motorista = viagem.id_motorista;
            this.usuarioService.usuario(viagem.id_usuario).subscribe(usuario => {
                this.nome = usuario.full_name;
            });
            this.motoristaService.motorista(viagem.id_motorista).subscribe(motorista => {
                this.motorista = motorista.full_name;
            });
        });
    }
    ngOnInit() {
    }
    dismiss() {
        this.modalCtrl.dismiss({
            'dismissed': true
        });
    }
    cancelar(id_viagem) {
        this.viagemService.cancelar(id_viagem).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield (yield this.toastCtrl.create({
                message: 'Viagem cancelada com sucesso !',
                duration: 4000, position: 'middle',
                color: 'success'
            })).present();
        }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(err);
            const toast = yield (yield this.toastCtrl.create({
                message: 'Ocorreu um erro!',
                duration: 4000, position: 'middle',
                color: 'danger'
            })).present();
        }));
        this.updateOnline();
    }
    finalizar(id_viagem) {
        this.viagemService.finalizar(id_viagem).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield (yield this.toastCtrl.create({
                message: 'Viagem finalizada com sucesso !',
                duration: 4000, position: 'middle',
                color: 'success'
            })).present();
        }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(err);
            const toast = yield (yield this.toastCtrl.create({
                message: 'Ocorreu um erro!',
                duration: 4000, position: 'middle',
                color: 'danger'
            })).present();
        }));
        this.updateOnline();
    }
    updateOnline() {
        this.viagemService.updateViagemVT(this.id_motorista).subscribe();
    }
};
CorridaAbertaPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavParams"] },
    { type: src_servico_viagem_viagem__WEBPACK_IMPORTED_MODULE_5__["ViagemService"] },
    { type: src_servico_usuario_usuario__WEBPACK_IMPORTED_MODULE_4__["UsuarioService"] },
    { type: src_servico_motorista_motorista__WEBPACK_IMPORTED_MODULE_3__["MotoristaService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
CorridaAbertaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-corrida-aberta',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./corrida-aberta.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/corrida-aberta/corrida-aberta.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./corrida-aberta.page.scss */ "./src/app/corrida-aberta/corrida-aberta.page.scss")).default]
    })
], CorridaAbertaPage);



/***/ }),

/***/ "./src/servico/motorista/motorista.ts":
/*!********************************************!*\
  !*** ./src/servico/motorista/motorista.ts ***!
  \********************************************/
/*! exports provided: MotoristaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MotoristaService", function() { return MotoristaService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



const API_URL = "https://uaileva.com.br/api";
let MotoristaService = class MotoristaService {
    constructor(http) {
        this.http = http;
    }
    motorista(id_motorista) {
        return this.http.get(API_URL + `/motorista/unico/` + id_motorista + ``);
    }
};
MotoristaService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
MotoristaService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], MotoristaService);



/***/ }),

/***/ "./src/servico/usuario/usuario.ts":
/*!****************************************!*\
  !*** ./src/servico/usuario/usuario.ts ***!
  \****************************************/
/*! exports provided: UsuarioService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioService", function() { return UsuarioService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



const API_URL = "https://uaileva.com.br/api";
let UsuarioService = class UsuarioService {
    constructor(http) {
        this.http = http;
    }
    usuario(id_usuario) {
        return this.http.get(API_URL + `/user/findUser/` + id_usuario + ``);
    }
};
UsuarioService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
UsuarioService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], UsuarioService);



/***/ }),

/***/ "./src/servico/viagem/viagem.ts":
/*!**************************************!*\
  !*** ./src/servico/viagem/viagem.ts ***!
  \**************************************/
/*! exports provided: ViagemService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViagemService", function() { return ViagemService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



const API_URL = "https://uaileva.com.br/api";
let ViagemService = class ViagemService {
    constructor(http) {
        this.http = http;
    }
    viagemAberta() {
        return this.http.get(API_URL + `/viagem/aberta`);
    }
    viagem(id_viagem) {
        return this.http.get(API_URL + `/viagem/` + id_viagem + ``);
    }
    cancelar(id_viagem) {
        return this.http.get(API_URL + `/viagem/cancelaMotorista/` + id_viagem + ``);
    }
    finalizar(id_viagem) {
        return this.http.get(API_URL + `/viagem/finalizada/` + id_viagem + ``);
    }
    updateViagemVT(id_motorista) {
        return this.http.get(API_URL + `/viagem/updateTrue/` + id_motorista + ``);
    }
};
ViagemService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ViagemService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], ViagemService);



/***/ })

}]);
//# sourceMappingURL=default~corrida-aberta-corrida-aberta-module~viagens-viagens-module-es2015.js.map
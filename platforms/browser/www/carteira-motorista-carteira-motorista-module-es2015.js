(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["carteira-motorista-carteira-motorista-module"],{

/***/ "./src/app/carteira-motorista/carteira-motorista-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/carteira-motorista/carteira-motorista-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: CarteiraMotoristaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarteiraMotoristaPageRoutingModule", function() { return CarteiraMotoristaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _carteira_motorista_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./carteira-motorista.page */ "./src/app/carteira-motorista/carteira-motorista.page.ts");




const routes = [
    {
        path: '',
        component: _carteira_motorista_page__WEBPACK_IMPORTED_MODULE_3__["CarteiraMotoristaPage"]
    }
];
let CarteiraMotoristaPageRoutingModule = class CarteiraMotoristaPageRoutingModule {
};
CarteiraMotoristaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CarteiraMotoristaPageRoutingModule);



/***/ }),

/***/ "./src/app/carteira-motorista/carteira-motorista.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/carteira-motorista/carteira-motorista.module.ts ***!
  \*****************************************************************/
/*! exports provided: CarteiraMotoristaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarteiraMotoristaPageModule", function() { return CarteiraMotoristaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _carteira_motorista_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./carteira-motorista-routing.module */ "./src/app/carteira-motorista/carteira-motorista-routing.module.ts");
/* harmony import */ var _carteira_motorista_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./carteira-motorista.page */ "./src/app/carteira-motorista/carteira-motorista.page.ts");







let CarteiraMotoristaPageModule = class CarteiraMotoristaPageModule {
};
CarteiraMotoristaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _carteira_motorista_routing_module__WEBPACK_IMPORTED_MODULE_5__["CarteiraMotoristaPageRoutingModule"]
        ],
        declarations: [_carteira_motorista_page__WEBPACK_IMPORTED_MODULE_6__["CarteiraMotoristaPage"]],
        providers: [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavParams"]]
    })
], CarteiraMotoristaPageModule);



/***/ })

}]);
//# sourceMappingURL=carteira-motorista-carteira-motorista-module-es2015.js.map
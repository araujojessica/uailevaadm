import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Foto } from "src/app/interfaces/foto";


//const API_URL = "https://uaileva.com.br/api"; 
const API_URL = "http://localhost:3000"; 

@Injectable({providedIn: 'root'})
export class EditarPerfilService{

    constructor(private http: HttpClient){}

    updateNome(user_id:number, nome:string,secondName:string){
        
        return this.http.post(`${API_URL}/motorista/updateNome`,{user_id,nome,secondName})
    };

    updatePassword(user_id:number, password:string){        
        return this.http.post(`${API_URL}/motorista/updatePassword`,{user_id,password})
    };

    apelido(user_id:number, apelido:string){        
        return this.http.post(`${API_URL}/motorista/updateApelido`,{user_id, apelido})
    };

    placa(motorista_id:number, placa:string, modelo:string){        
       return this.http.get(`${API_URL}/carro/update/`+motorista_id+`/`+placa+`/`+modelo+``)
    };

    modelo(motorista_id:number, placa, modelo:string){  
 
        return this.http.get(`${API_URL}/carro/update/`+motorista_id+`/`+placa+`/`+modelo+``)
    };

    cidade(motorista_id:number, cidade:string){        
        return this.http.get(`${API_URL}/motorista/cidade/`+motorista_id+`/`+cidade+``)
    };

    phone(motorista_id:number, phone:string){        
        return this.http.get(`${API_URL}/motorista/phone/`+motorista_id+`/`+phone+``)
    };

    email(motorista_id:number, email:string){        
        return this.http.get(`${API_URL}/motorista/email/`+motorista_id+`/`+email+``)
    };

    updateFoto(description: string, allowComments: boolean, file: File) {
        {
            const formData = new FormData();
            formData.append('description', description);
            formData.append('allowComments', allowComments ? 'true' : 'false');
            formData.append('imageFile', file);           
            
            return this.http.post(API_URL + '/photos/update', formData);
        }
    }  
   



}
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

//const API_URL = "https://uaileva.com.br/api"; 
const API_URL = "http://localhost:3000";
@Injectable({providedIn: 'root'})
export class MotoristaService {

    constructor(   
        private http: HttpClient
    ){}
    
    motorista(id_motorista){
        return this.http.get<any>(API_URL +  `/motorista/unico/`+id_motorista+``);
    }

    
   

}
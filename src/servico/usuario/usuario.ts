import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

//const API_URL = "https://uaileva.com.br/api"; 

const API_URL = "http://localhost:3000";

@Injectable({providedIn: 'root'})
export class UsuarioService {

    constructor(   
        private http: HttpClient
    ){}
    
    usuario(id_usuario){
        return this.http.get<any>(API_URL +  `/user/findUser/`+id_usuario+``);
    }

    
   

}
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Viagem } from "src/app/interfaces/viagem";
import { MotoristaLiberacao } from "src/interface/motoristaLiberacao";


//const API_URL = "https://uaileva.com.br/api"; 

const API_URL = "http://localhost:3000"; 

@Injectable({providedIn: 'root'})
export class ViagemService {

    constructor(   
        private http: HttpClient
    ){}
    
    viagemAberta(){
        return this.http.get<any[]>(API_URL +  `/viagem/aberta`);
    }

    viagem(id_viagem){
        return this.http.get<any>(API_URL +  `/viagem/`+id_viagem+``);
    }

    cancelar(id_viagem){
        return this.http.get<any>(API_URL +  `/viagem/cancelaMotorista/`+id_viagem+``);
    }

    finalizar(id_viagem){
        return this.http.get<any>(API_URL +  `/viagem/finalizada/`+id_viagem+``);
    }

    updateViagemVT(id_motorista){
        return this.http.get<any>(API_URL + `/viagem/updateTrue/`+id_motorista+``)
    }
   

}
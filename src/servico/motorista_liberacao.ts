import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Viagem } from "src/app/interfaces/viagem";
import { MotoristaLiberacao } from "src/interface/motoristaLiberacao";


const API_URL = "https://uaileva.com.br/api"; 
//const API_URL = "http://localhost:3000";
@Injectable({providedIn: 'root'})
export class MotoristaLiberacaoService {

    constructor(   
        private http: HttpClient
    ){}
    
    liberar(){
        return this.http.get<MotoristaLiberacao[]>(API_URL +  `/motorista/liberacao`);
    }

    motoristaUnico(motorista_id){
        return this.http.get<any>(API_URL + `/motorista/unico/` + motorista_id);
    }

    fotoMotorista(mot_phone){
        return this.http.get<any>(API_URL + `/photos/motorista/` + mot_phone);
    }

    fotoCrlv(mot_phone){
        return this.http.get<any>(API_URL + `/photos/motorista/crlv/` + mot_phone);
    }

    fotoCnh(mot_phone){
        return this.http.get<any>(API_URL + `/photos/motorista/cnh/` + mot_phone);
    }

    carro(id_motorista,placa, modelo){
        return this.http.get<any>(API_URL + `/carro/signup/`+id_motorista+`/`+placa+`/`+modelo+``);
    }

    updateNome(user_id:number, nome:string,secondName:string){
        
        return this.http.post(`${API_URL}/motorista/updateNome`,{user_id,nome,secondName})
    };

    liberarMotorista(phone, email){
        return this.http.get<any>(API_URL + `/motorista/status/`+phone+`/`+email+`/LIBERADO`);
      
    }

    motoristaLiberado(){
        return this.http.get<any>(API_URL + `/motoristaLiberado`);      
    }

    bloqueio(phone, email){
        return this.http.get<any>(API_URL + `/motorista/status/`+phone+`/`+email+`/BLOQUEADO`);
    }

    bloqueioPassageiro(phone){
       
        return this.http.get<any>(API_URL + `/user/bloqueio/`+phone+`/BLOQUEADO`);
      
    }

    corridaCM(data){
      console.log(data)
       return this.http.get<any>(API_URL + `/corrida/diariacm/`+data+``);
      
    }

    corridaCP(data){
       
        return this.http.get<any>(API_URL + `/corrida/diariacp/`+data+``);
      
    }
    corridaF(data){
       
        return this.http.get<any>(API_URL + `/corrida/diariaf/`+data+``);
      
    }

    corridaTotal(){
       
        return this.http.get<any>(API_URL + `/totalCorridas`);
      
    }
    motoristaLiberadoUnico(motorista_id){
        return this.http.get<any>(API_URL + `/motorista/liberadoUnico/` + motorista_id);
    }

    arrecadacao(motorista_id){
        return this.http.get<any>(API_URL + `/totalarrecadado/` + motorista_id);
    }

    desbloqueio(phone, email){
        return this.http.get<any>(API_URL + `/motorista/status/`+phone+`/`+email+`/LIBERADO`);
    }

    motbloqueado() {
        return this.http.get<any>(API_URL + `/motorista/bloqueado`);
    }




   

   





    


}
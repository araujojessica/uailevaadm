import { NgModule} from '@angular/core';
import { PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule) },
  { path: 'motoristas', loadChildren: () => import('./motoristas/motoristas.module').then( m => m.MotoristasPageModule) },
  { path: 'login-adm', loadChildren: () => import('./login-adm/login-adm.module').then( m => m.LoginAdmPageModule) },
  { path: 'download', loadChildren: () => import('./download/download.module').then( m => m.DownloadPageModule) },
  { path: 'login-home', loadChildren: () => import('./login-home/login-home.module').then( m => m.LoginHomePageModule) },
  { path: 'liberacao/:user_id', loadChildren: () => import('./liberacao/liberacao.module').then( m => m.LiberacaoPageModule) },
  { path: 'bloqueio', loadChildren: () => import('./bloqueio/bloqueio.module').then( m => m.BloqueioPageModule) },
  { path: 'cadastro', loadChildren: () => import('./cadastro/cadastro.module').then( m => m.CadastroPageModule) },
  { path: 'carteira',  loadChildren: () => import('./carteira/carteira.module').then( m => m.CarteiraPageModule) },
  { path: 'banco', loadChildren: () => import('./banco/banco.module').then( m => m.BancoPageModule) },
  { path: 'boleto', loadChildren: () => import('./boleto/boleto.module').then( m => m.BoletoPageModule) },
  { path: 'corridas', loadChildren: () => import('./corridas/corridas.module').then( m => m.CorridasPageModule) },
  { path: 'bloqueio-cliente',  loadChildren: () => import('./bloqueio-cliente/bloqueio-cliente.module').then( m => m.BloqueioClientePageModule) },
  { path: 'lista-motoristas', loadChildren: () => import('./lista-motoristas/lista-motoristas.module').then( m => m.ListaMotoristasPageModule) },
  { path: 'etapas/:nome/:phone', loadChildren: () => import('./etapas/etapas.module').then( m => m.EtapasPageModule) },
  { path: 'acordos', loadChildren: () => import('./acordos/acordos.module').then( m => m.AcordosPageModule) },
  { path: 'foto-perfil', loadChildren: () => import('./foto-perfil/foto-perfil.module').then( m => m.FotoPerfilPageModule) } ,
  { path: 'cnh', loadChildren: () => import('./cnh/cnh.module').then( m => m.CnhPageModule) },
  { path: 'crlv', loadChildren: () => import('./crlv/crlv.module').then( m => m.CrlvPageModule) },
  { path: 'editar-motorista/:user_id', loadChildren: () => import('./editar-motorista/editar-motorista.module').then( m => m.EditarMotoristaPageModule) },
  { path: 'carteira-motorista', loadChildren: () => import('./carteira-motorista/carteira-motorista.module').then( m => m.CarteiraMotoristaPageModule) },
  { path: 'corrida-aberta/:id_viagem', loadChildren: () => import('./corrida-aberta/corrida-aberta.module').then( m => m.CorridaAbertaPageModule) },
  { path: 'viagens', loadChildren: () => import('./viagens/viagens.module').then( m => m.ViagensPageModule) },
  { path: 'editar-passageiro', loadChildren: () => import('./editar-passageiro/editar-passageiro.module').then( m => m.EditarPassageiroPageModule) },
  { path: 'quitar', loadChildren: () => import('./quitar/quitar.module').then( m => m.QuitarPageModule) },
  { path: 'cobranca', loadChildren: () => import('./cobranca/cobranca.module').then( m => m.CobrancaPageModule) },
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
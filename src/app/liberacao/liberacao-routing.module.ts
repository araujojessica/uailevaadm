import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LiberacaoPage } from './liberacao.page';

const routes: Routes = [
  {
    path: '',
    component: LiberacaoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LiberacaoPageRoutingModule {}

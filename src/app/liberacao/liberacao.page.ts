import { AbstractType, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/core/user/user';
import { UserService } from 'src/core/user/user.service';
import { MotoristaLiberacao } from 'src/interface/motoristaLiberacao';
import { MotoristaLiberacaoService } from 'src/servico/motorista_liberacao';

@Component({
  selector: 'app-liberacao',
  templateUrl: './liberacao.page.html',
  styleUrls: ['./liberacao.page.scss'],
})
export class LiberacaoPage implements OnInit {

  user$: Observable<User>;
  usuarioLogado: User;
  user_id: number;
  motorista_id:number;
  apelido: string;
  nome: string;
  m: MotoristaLiberacao[];
  mot: any;
  photo: any;
  url_foto: string = 'assets/img/no.png';
  photo_crlv: any;
  url_crlv: string = 'assets/img/no.png';
  photo_cnh: any;
  url_cnh: string = 'assets/img/no.png';
  nomeCompleto:string;
  placa: string = '';
  modelo: string ='';
  secondNome:string;
  phone:string;
  email:string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private motoristaService: UserService,
    private motoristaLiberacaoService: MotoristaLiberacaoService,
    private toastCtrl: ToastController
  ) { 

    this.user$ = motoristaService.getUser();
    this.route.params.subscribe( parametros => {
      this.motorista_id = parametros['user_id'];
      this.motoristaLiberacaoService.motoristaUnico(this.motorista_id).subscribe( motorista => {
          this.mot = motorista;
          this.nome = motorista.full_name;
          this.phone = this.mot.phone;
          this.email = this.mot.email;
          this.motoristaLiberacaoService.fotoMotorista(this.mot.phone).subscribe( foto_motorista => {
            this.photo = foto_motorista;
            this.url_foto = `https://uaileva.com.br/api/imgs/` + this.photo.url;
          });

          this.motoristaLiberacaoService.fotoCrlv(this.mot.phone).subscribe( foto_crlv => {
            this.photo_crlv = foto_crlv;
            this.url_crlv = `https://uaileva.com.br/api/imgs/` + this.photo_crlv.url;
          });

          this.motoristaLiberacaoService.fotoCnh(this.mot.phone).subscribe( foto_cnh => {
            this.photo_cnh = foto_cnh;
            this.url_cnh = `https://uaileva.com.br/api/imgs/` + this.photo_cnh.url;
          });
      });  
    });
    
    this.user$.subscribe( usuario => {
      this.usuarioLogado = usuario;
      this.user_id = this.usuarioLogado.id;
      var a = this.usuarioLogado.full_name.split(' ');

      if(this.usuarioLogado.apelido == null){
        this.apelido = '';
      } else {
        this.apelido = '(' + this.usuarioLogado.apelido + ')';
      }
      this.nome = a[0] + ' ' + this.apelido;
      
    });

    this.motoristaLiberacaoService.liberar().subscribe(motoristaLiberado => {
      this.m = motoristaLiberado;
    });
  }

  ngOnInit() {
  }

  salvar(motorista_id){
    if(this.nomeCompleto != '' || this.nomeCompleto != null){     
        this.motoristaLiberacaoService.updateNome(this.user_id, this.nomeCompleto, '').subscribe();
    }
    if(this.placa != '' || this.modelo != ''){
      this.motoristaLiberacaoService.carro(motorista_id,this.placa,this.modelo).subscribe();
    }

    this.motoristaLiberacaoService.liberarMotorista(this.phone,this.email).subscribe(      
        async () => {
          const toast = await (await this.toastCtrl.create({
            message: 'Motorista Liberado!',
            duration: 4000, position: 'top',
            color: 'success'
          })).present();
        },  
        async err => {
          console.log(err)
          const toast = await (await this.toastCtrl.create({
            message: 'Por favor confira se a sua internet está funcionando e tente cadastrar novamente! UaiLeva agradece!',
            duration: 4000, position: 'top',
            color: 'danger'
          })).present();
        }
      
    );
  }
  foto(){
    
    window.open('https://motorista.uaileva.com.br/#/etapas/'+this.nome+'/'+this.phone+'');
  

  }
}

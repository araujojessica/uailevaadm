import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LiberacaoPage } from './liberacao.page';

describe('LiberacaoPage', () => {
  let component: LiberacaoPage;
  let fixture: ComponentFixture<LiberacaoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiberacaoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LiberacaoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

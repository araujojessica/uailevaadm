import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LiberacaoPageRoutingModule } from './liberacao-routing.module';

import { LiberacaoPage } from './liberacao.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LiberacaoPageRoutingModule
  ],
  declarations: [LiberacaoPage]
})
export class LiberacaoPageModule {}

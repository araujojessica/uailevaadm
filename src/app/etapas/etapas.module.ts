import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EtapasPageRoutingModule } from './etapas-routing.module';

import { EtapasPage } from './etapas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EtapasPageRoutingModule
  ],
  declarations: [EtapasPage]
})
export class EtapasPageModule {}

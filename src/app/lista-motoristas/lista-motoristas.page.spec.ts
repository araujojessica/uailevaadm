import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListaMotoristasPage } from './lista-motoristas.page';

describe('ListaMotoristasPage', () => {
  let component: ListaMotoristasPage;
  let fixture: ComponentFixture<ListaMotoristasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaMotoristasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListaMotoristasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

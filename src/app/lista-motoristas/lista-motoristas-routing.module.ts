import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListaMotoristasPage } from './lista-motoristas.page';

const routes: Routes = [
  {
    path: '',
    component: ListaMotoristasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListaMotoristasPageRoutingModule {}

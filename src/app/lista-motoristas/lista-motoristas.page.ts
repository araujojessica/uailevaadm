import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from 'src/core/user/user';
import { UserService } from 'src/core/user/user.service';
import { MotoristaLiberacao } from 'src/interface/motoristaLiberacao';
import { MotoristaLiberacaoService } from 'src/servico/motorista_liberacao';

@Component({
  selector: 'app-lista-motoristas',
  templateUrl: './lista-motoristas.page.html',
  styleUrls: ['./lista-motoristas.page.scss'],
})
export class ListaMotoristasPage implements OnInit {

  user$: Observable<User>;
  usuarioLogado: User;
  user_id: number;
  apelido: string;
  nome:string;
  m : MotoristaLiberacao[];

  constructor(
    private router: Router,
    private motoristaService: UserService,
    private mortoristaLiberacaoService: MotoristaLiberacaoService
    ) { 
    this.user$ = motoristaService.getUser();

    this.user$.subscribe( usuario => {
      this.usuarioLogado = usuario;
      this.user_id = this.usuarioLogado.id;
      var a = this.usuarioLogado.full_name.split(' ');

      if(this.usuarioLogado.apelido == null){
        this.apelido = '';
      }else {
        this.apelido = '(' + this.usuarioLogado.apelido + ')';
      }
      this.nome = a[0] + ' ' + this.apelido;
    });
    this.mortoristaLiberacaoService.liberar().subscribe(motoristaLiberado =>{
      this.m = motoristaLiberado;
   });

  }

  ngOnInit(){
    
  }

  liberacao(user_id: number){
    this.router.navigate(['/liberacao/'+user_id+'']);
  }

}
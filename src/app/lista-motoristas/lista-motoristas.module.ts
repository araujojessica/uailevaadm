import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListaMotoristasPageRoutingModule } from './lista-motoristas-routing.module';

import { ListaMotoristasPage } from './lista-motoristas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListaMotoristasPageRoutingModule
  ],
  declarations: [ListaMotoristasPage]
})
export class ListaMotoristasPageModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CobrancaPage } from './cobranca.page';

describe('CobrancaPage', () => {
  let component: CobrancaPage;
  let fixture: ComponentFixture<CobrancaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CobrancaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CobrancaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

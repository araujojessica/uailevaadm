import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/core/user/user';
import { UserService } from 'src/core/user/user.service';
import { MotoristaLiberacao } from 'src/interface/motoristaLiberacao';
import { MotoristaLiberacaoService } from 'src/servico/motorista_liberacao';

@Component({
  selector: 'app-cobranca',
  templateUrl: './cobranca.page.html',
  styleUrls: ['./cobranca.page.scss'],
})
export class CobrancaPage implements OnInit {
  user$: Observable<User>;
  usuarioLogado: User;
  user_id: number;
  motorista_id:number;
  apelido: string;
  nome: string;
  m: MotoristaLiberacao[];
  mot: any;
  photo: any;
  url_foto: string = 'assets/img/no.png';
  photo_crlv: any;
  url_crlv: string = 'assets/img/no.png';
  photo_cnh: any;
  url_cnh: string = 'assets/img/no.png';
  nomeCompleto:string;
  placa: string;
  modelo: string;
  secondNome:string;
  phone:string;
  email:string;
  nomeMotorista:string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private motoristaService: UserService,
    private motoristaLiberacaoService: MotoristaLiberacaoService,
    private toastCtrl: ToastController,
    private modalCtrl: ModalController
  ) { 

    this.user$ = motoristaService.getUser();
    this.route.params.subscribe( parametros => {
      this.motorista_id = parametros['user_id'];
      
    });
    
    this.user$.subscribe( usuario => {
      this.usuarioLogado = usuario;
      this.user_id = this.usuarioLogado.id;
      var a = this.usuarioLogado.full_name.split(' ');

      if(this.usuarioLogado.apelido == null){
        this.apelido = '';
      } else {
        this.apelido = '(' + this.usuarioLogado.apelido + ')';
      }
      this.nomeMotorista = a[0] + ' ' + this.apelido;
      
    });

    this.motoristaLiberacaoService.motoristaLiberado().subscribe(motoristaLiberado => {
      this.m = motoristaLiberado;
    });
  }

  ngOnInit(){
    
  }
}

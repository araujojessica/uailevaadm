import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CarteiraMotoristaPage } from './carteira-motorista.page';

describe('CarteiraMotoristaPage', () => {
  let component: CarteiraMotoristaPage;
  let fixture: ComponentFixture<CarteiraMotoristaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarteiraMotoristaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CarteiraMotoristaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

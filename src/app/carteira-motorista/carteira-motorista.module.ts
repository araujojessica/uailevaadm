import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule, NavParams } from '@ionic/angular';

import { CarteiraMotoristaPageRoutingModule } from './carteira-motorista-routing.module';

import { CarteiraMotoristaPage } from './carteira-motorista.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CarteiraMotoristaPageRoutingModule
  ],
  declarations: [CarteiraMotoristaPage],
  providers: [NavParams]
})
export class CarteiraMotoristaPageModule {}

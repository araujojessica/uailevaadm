import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CarteiraMotoristaPage } from './carteira-motorista.page';

const routes: Routes = [
  {
    path: '',
    component: CarteiraMotoristaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CarteiraMotoristaPageRoutingModule {}

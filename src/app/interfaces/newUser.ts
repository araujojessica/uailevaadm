export interface newUser {

    id: string;
    name: string;
    secondName: string;
    phone: string;
    apelido: string;
    sexo: string;
    email: string;
    cidade: string;
    password: string;
}
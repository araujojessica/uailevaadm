import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/core/user/user';
import { UserService } from 'src/core/user/user.service';
import { MotoristaLiberacao } from 'src/interface/motoristaLiberacao';
import { MotoristaLiberacaoService } from 'src/servico/motorista_liberacao';

@Component({
  selector: 'app-login-home',
  templateUrl: './login-home.page.html',
  styleUrls: ['./login-home.page.scss'],
})
export class LoginHomePage implements OnInit {

  user$: Observable<User>;
  usuarioLogado: User;
  user_id: number;
  apelido: string;
  nome:string;
  m : MotoristaLiberacao[];
  adm: string;

  constructor(
    private router: Router,
    private motoristaService: UserService,
    private mortoristaLiberacaoService: MotoristaLiberacaoService,
    private userService: UserService,
    private menuCtrl: MenuController
  ) { 
    this.user$ = motoristaService.getUser();

    this.user$.subscribe( usuario => {
      this.usuarioLogado = usuario;
      this.user_id = this.usuarioLogado.id;
      var a = this.usuarioLogado.full_name.split(' ');

      if(this.usuarioLogado.apelido == null){
        this.apelido = '';
      }else {
        this.apelido = '(' + this.usuarioLogado.apelido + ')';
      }
      this.nome = a[0] + ' ' + this.apelido;
      this.adm = this.usuarioLogado.adm;

    });

    this.mortoristaLiberacaoService.liberar().subscribe(motoristaLiberado =>{
      this.m = motoristaLiberado;

   });


  }

  ngOnInit() {
  }

  liberacao(){

    this.router.navigate(['/liberacao']);
  }

  motorista(){
    this.router.navigate(['/bloqueio']);
  }

  cadastro(){
    this.router.navigate(['/cadastro']);
  }

  bloqueados() {
    this.router.navigate(['motorista-bloqueado']);
  }

  carteira(){
    this.router.navigate(['/carteira']);
  }

  banco(){
    this.router.navigate(['/banco']);
  }

  gerarBoleto(){
    this.router.navigate(['/boleto']);
  }

  bloqueioCliente(){
    this.router.navigate(['/bloqueio-cliente']);
  }

  editarCliente(){
    this.router.navigate(['/editar-passageiro']);
  }

  corridas(){
    this.router.navigate(['/corridas']);
  }

  viagens(){
    this.router.navigate(['/viagens']);
  }

  cobranca(){
    this.router.navigate(['/cobranca']);
  }

  listaMotoristas(){
    this.router.navigate(['/lista-motoristas']);
  }
  
  logout(){
    this.userService.logout();
    localStorage.removeItem('currentUser');
    this.router.navigate(['']);
    this.menuCtrl.close();
    navigator['app'].exitApp();
  }

}

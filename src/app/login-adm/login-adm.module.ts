import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginAdmPageRoutingModule } from './login-adm-routing.module';

import { LoginAdmPage } from './login-adm.page';
import { BrMaskerModule } from 'br-mask';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BrMaskerModule,
    LoginAdmPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [LoginAdmPage]
})
export class LoginAdmPageModule {}

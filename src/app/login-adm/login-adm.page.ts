import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { AuthService } from 'src/core/auth/auth.service';
import { PlatformDectorService } from 'src/core/plataform-dector/plataform-dector.service';

@Component({
  selector: 'app-login-adm',
  templateUrl: './login-adm.page.html',
  styleUrls: ['./login-adm.page.scss'],
})
export class LoginAdmPage implements OnInit {

  tipo: boolean;
  public onLoginForm: FormGroup;
  
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private motoristaService: AuthService,
    private navCtrl: NavController,
    private platformDetectorService: PlatformDectorService,
    private toastCtrl: ToastController,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {

    this.onLoginForm = this.formBuilder.group({
      'userName': [null, Validators.compose([Validators.required])],
      'password': [null, Validators.compose([Validators.required])]
    });
  }

  mostrarSenha(){
    this.tipo = !this.tipo;
  }

  toLoginHome(){

    const userName = this.onLoginForm.get('userName').value;
    const password = this.onLoginForm.get('password').value;

    var a = userName.replace('-','').replace(' ','').replace('(','').replace(')','');

    this.motoristaService.authenticate(a, password).subscribe(
      () => this.navCtrl.navigateRoot('/login-home'),
      async err => {
          console.log('entrou aqui' + err.message);               
          this.onLoginForm.reset();
          this.platformDetectorService.isPlataformBrowser() ;
          const toast = await this.toastCtrl.create({
                  message:'Usuário não liberado, por favor fale com o administrativo.', 
                  duration:4000, position:'top',
                  color:'danger'
          });               
         toast.present();

      }
      
  ); 
  }

  async forgotPass() {
    const alert = await this.alertCtrl.create({
      header: 'Esqueceu sua senha?',
      message: 'Informe seu e-mail para a recuperação.',
      inputs: [
        {
          name: 'email',
          type: 'email',
          placeholder: 'E-mail'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirmar Cancelamento?');
          }
        }/*, {
          text: 'Confirmar',
          handler: async data => {
            console.log(data.email)
            this.recuperarSenha.sendemail(data.email).subscribe(
                async () =>{ 
                  console.log('entrou aqui')
                  this.navCtrl.navigateRoot('')
                  const toast = await this.toastCtrl.create({
                            
                    message:'Email enviado com sucesso!', 
                    duration:4000, position:'top',
                    color:'success'
                    });
            
                  
                  toast.present();
                },
                async err => {
             
                   const toast = await this.toastCtrl.create({
                            
                            message:'Usuário não cadastrado, por favor cadastre-se no APP!', 
                            duration:4000, position:'top',
                            color:'danger'
                    });
            
                   
                   toast.present();
    
                }
                
            );
            
          }
        }*/
      ]
    });

    await alert.present();
}

}

import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { MotoristaService } from 'src/servico/motorista/motorista';
import { UsuarioService } from 'src/servico/usuario/usuario';
import { ViagemService } from 'src/servico/viagem/viagem';
import { CorridaAbertaPage } from '../corrida-aberta/corrida-aberta.page';

@Component({
  selector: 'app-viagens',
  templateUrl: './viagens.page.html',
  styleUrls: ['./viagens.page.scss'],
})
export class ViagensPage implements OnInit {

    viagem: any[];
    nome: any;
    motorista:any;
    status:any;
  constructor(
    private modalCtrl: ModalController,
    private viagemService: ViagemService,
    private usuarioService: UsuarioService,
    private motoristaService: MotoristaService
  ) { 
    this.viagemService.viagemAberta().subscribe(viagem => {
      this.viagem = viagem;
        for(var v of this.viagem){

          if(v.status == 'ACEITO'){
            this.status = 'MOTORISTA A CAMINHO DO PASSAGEIRO';
          }
    
          if(v.status == 'INICIANDO'){
            this.status = 'MOTORISTA A CAMINHO DO DESTINO';
          }
    
          if(v.status == 'CHEGUEI'){
            this.status = 'MOTORISTA AGUARDANDO PASSAGEIRO';
          }
       
        }
    });

  }

  ngOnInit() {
  }

  async corridaAberta(id_viagem:any){
    const modal = await this.modalCtrl.create({
      component: CorridaAbertaPage,
      componentProps: {
        viagem: id_viagem
      },
    });

    return await modal.present();
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MotoristasBloqueadosPage } from './motoristas_bloqueados.page';

const routes: Routes = [
  {
    path: '',
    component: MotoristasBloqueadosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MotoristasBloqueadosPageRoutingModule {}

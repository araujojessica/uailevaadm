import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MotoristasBloqueadosPage } from './motoristas_bloqueados.page';

describe('MotoristasBloqueadosPage', () => {
  let component: MotoristasBloqueadosPage;
  let fixture: ComponentFixture<MotoristasBloqueadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MotoristasBloqueadosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MotoristasBloqueadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

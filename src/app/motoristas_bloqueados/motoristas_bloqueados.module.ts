import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MotoristasBloqueadosPageRoutingModule } from './motoristas_bloqueados-routing.module';

import { MotoristasBloqueadosPage } from './motoristas_bloqueados.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MotoristasBloqueadosPageRoutingModule
  ],
  declarations: [MotoristasBloqueadosPage]
})
export class MotoristasBloqueadosPageModule {}

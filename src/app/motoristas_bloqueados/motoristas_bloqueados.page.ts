import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/core/user/user';
import { UserService } from 'src/core/user/user.service';
import { MotoristaLiberacao } from 'src/interface/motoristaLiberacao';
import { MotoristaLiberacaoService } from 'src/servico/motorista_liberacao';

@Component({
  selector: 'app-motoristas_bloqueados',
  templateUrl: './motoristas_bloqueados.page.html',
  styleUrls: ['./motoristas_bloqueados.page.scss'],
})
export class MotoristasBloqueadosPage implements OnInit {

  user$: Observable<User>;
  usuarioLogado: User;
  user_id: number;
  apelido: string;
  nome:string;
  m : MotoristaLiberacao[];
  

  constructor(
    private router: Router,
    private motoristaService: UserService,
    private mortoristaLiberacaoService: MotoristaLiberacaoService,
    private toastCtrl: ToastController
    ) { 
    this.user$ = motoristaService.getUser();

    this.user$.subscribe( usuario => {
      this.usuarioLogado = usuario;
      this.user_id = this.usuarioLogado.id;
      var a = this.usuarioLogado.full_name.split(' ');

      if(this.usuarioLogado.apelido == null){
        this.apelido = '';
      }else {
        this.apelido = '(' + this.usuarioLogado.apelido + ')';
      }
      this.nome = a[0] + ' ' + this.apelido;
    });
    this.mortoristaLiberacaoService.motbloqueado().subscribe(motoristaLiberado =>{
      this.m = motoristaLiberado;
   });

  }

  ngOnInit(){
    
  }

  desbloquear(email, phone){
   this.mortoristaLiberacaoService.desbloqueio(phone,email).subscribe(
    async () => {
      const toast = await (await this.toastCtrl.create({
        message: 'Motorista Desbloqueado',
        duration: 4000, position: 'middle',
        color: 'success'
      })).present();

     
    },  
      async err => {
        const toast = await (await this.toastCtrl.create({
          message: 'Erro ao desbloquear Motorista',
          duration: 4000, position: 'middle',
          color: 'danger'
        })).present();

      }
   )
  }


}
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditarPassageiroPage } from './editar-passageiro.page';

describe('EditarPassageiroPage', () => {
  let component: EditarPassageiroPage;
  let fixture: ComponentFixture<EditarPassageiroPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarPassageiroPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditarPassageiroPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

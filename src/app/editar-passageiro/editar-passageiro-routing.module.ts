import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditarPassageiroPage } from './editar-passageiro.page';

const routes: Routes = [
  {
    path: '',
    component: EditarPassageiroPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditarPassageiroPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditarPassageiroPageRoutingModule } from './editar-passageiro-routing.module';

import { EditarPassageiroPage } from './editar-passageiro.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditarPassageiroPageRoutingModule
  ],
  declarations: [EditarPassageiroPage]
})
export class EditarPassageiroPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BloqueioPage } from './bloqueio.page';

const routes: Routes = [
  {
    path: '',
    component: BloqueioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BloqueioPageRoutingModule {}

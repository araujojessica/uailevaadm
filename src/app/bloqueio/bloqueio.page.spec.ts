import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BloqueioPage } from './bloqueio.page';

describe('BloqueioPage', () => {
  let component: BloqueioPage;
  let fixture: ComponentFixture<BloqueioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BloqueioPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BloqueioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

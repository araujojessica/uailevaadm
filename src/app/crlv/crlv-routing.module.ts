import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrlvPage } from './crlv.page';

const routes: Routes = [
  {
    path: '',
    component: CrlvPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrlvPageRoutingModule {}

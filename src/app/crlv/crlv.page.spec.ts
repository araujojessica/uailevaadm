import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrlvPage } from './crlv.page';

describe('CrlvPage', () => {
  let component: CrlvPage;
  let fixture: ComponentFixture<CrlvPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrlvPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrlvPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

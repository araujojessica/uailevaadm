import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrlvPageRoutingModule } from './crlv-routing.module';

import { CrlvPage } from './crlv.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrlvPageRoutingModule
  ],
  declarations: [CrlvPage]
})
export class CrlvPageModule {}

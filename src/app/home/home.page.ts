import { Component } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.css'],
})
export class HomePage {

  constructor(
    private router: Router,
  
   
  ) {}

  toLogin(){
    this.router.navigate(['/login-adm']);
  }

  toMotoristas(){
    this.router.navigate(['/motoristas']);
  }

  baixar(){
    this.router.navigate(['/download']);
  }

  toadm(){
    
  }

  async download(){
    const anchor = window.document.createElement('a');
    anchor.href = '/assets/img/app-debug.apk'
    anchor.download = 'uaileva.apk';
    anchor.click();
    window.URL.revokeObjectURL(anchor.href);
  } 

}

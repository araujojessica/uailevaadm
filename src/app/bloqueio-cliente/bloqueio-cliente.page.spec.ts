import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BloqueioClientePage } from './bloqueio-cliente.page';

describe('BloqueioClientePage', () => {
  let component: BloqueioClientePage;
  let fixture: ComponentFixture<BloqueioClientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BloqueioClientePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BloqueioClientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { MotoristaLiberacaoService } from 'src/servico/motorista_liberacao';

@Component({
  selector: 'app-bloqueio-cliente',
  templateUrl: './bloqueio-cliente.page.html',
  styleUrls: ['./bloqueio-cliente.page.scss'],
})
export class BloqueioClientePage implements OnInit {


  phone:string;

  constructor(
    private usuarioService: MotoristaLiberacaoService,
    private toastCtrl: ToastController
  ) { }

  ngOnInit() {
  }
  
  bloquear(){
    this.usuarioService.bloqueioPassageiro(this.phone).subscribe(
      async () => {
        const toast = await (await this.toastCtrl.create({
          message: 'Passageiro '+this.phone+' bloqueado com sucesso!',
          duration: 4000, position: 'top',
          color: 'success'
        })).present();
      },  
      async err => {
        console.log(err)
        const toast = await (await this.toastCtrl.create({
          message: 'Por favor confira se a sua internet está funcionando e tente novamente! UaiLeva agradece!',
          duration: 4000, position: 'top',
          color: 'danger'
        })).present();
      }
    );

  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BloqueioClientePage } from './bloqueio-cliente.page';

const routes: Routes = [
  {
    path: '',
    component: BloqueioClientePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BloqueioClientePageRoutingModule {}

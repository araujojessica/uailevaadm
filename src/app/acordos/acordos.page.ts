import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-acordos',
  templateUrl: './acordos.page.html',
  styleUrls: ['./acordos.page.scss'],
})
export class AcordosPage implements OnInit {

  checkedButton: boolean = true;
  constructor() { }

  ngOnInit() {
  }

  enableBtn(event: { checked: any; }){
    if (event.checked) {
      this.checkedButton = !this.checkedButton;
     
    }

    else{
      this.checkedButton = !this.checkedButton;
    } 
}

}

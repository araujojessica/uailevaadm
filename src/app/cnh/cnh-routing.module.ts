import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CnhPage } from './cnh.page';

const routes: Routes = [
  {
    path: '',
    component: CnhPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CnhPageRoutingModule {}

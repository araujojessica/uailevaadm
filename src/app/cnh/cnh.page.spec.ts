import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CnhPage } from './cnh.page';

describe('CnhPage', () => {
  let component: CnhPage;
  let fixture: ComponentFixture<CnhPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CnhPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CnhPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

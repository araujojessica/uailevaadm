import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { MotoristaLiberacaoService } from 'src/servico/motorista_liberacao';

@Component({
  selector: 'app-corridas',
  templateUrl: './corridas.page.html',
  styleUrls: ['./corridas.page.scss'],
})
export class CorridasPage implements OnInit {

  totalCm : any;
  canceladoMotorista:any;
  totalCp : any;
  canceladoPassageiro:any;
  totalf:any
  finalizadas:any;
  constructor(
    private corridasService : MotoristaLiberacaoService,
    private modalCtrl: ModalController

  ) {

    var d = new Date();
    var data = d.getDate();
    var month = parseInt(d.getMonth()+'')+1;

   
    if(month ==1){
      var m = '01';
    } else
    if(month == 2){
      var m = '02';
    }else
    if(month == 3){
      var m = '03';
    }else
    if(month == 4){
      var m = '04';
    }else
    if(month == 5){
      var m = '05';
    }else
    if(month == 6){
      var m = '06';
    }else
    if(month == 7){
      var m = '07';
    }else
    if(month == 8){
      var m = '08';
    }else
    if(month == 9){
      var m = '09';
    }else{
      var m = ''+month;
    }

    if(d.getDate() ==1){
      var h = '01';
    } else
    if(d.getDate() == 2){
      var h = '02';
    }else
    if(d.getDate() == 3){
      var h = '03';
    }else
    if(d.getDate() == 4){
      var h = '04';
    }else
    if(d.getDate() == 5){
      var h = '05';
    }else
    if(d.getDate() == 6){
      var h = '06';
    }else
    if(d.getDate() == 7){
      var h = '07';
    }else
    if(d.getDate() == 8){
      var h = '08';
    }else
    if(d.getDate() == 9){
      var h = '09';
    }else{
      var h = ''+d.getDate();
    }
    
    var date =  d.getFullYear() + "-" + m + '-' + h;

    this.corridasService.corridaCM(date).subscribe(totalcm =>{
        this.totalCm = totalcm;
        this.canceladoMotorista = this.totalCm.total_viagem_cm;
    });

    this.corridasService.corridaCP(date).subscribe(totalcp =>{
      this.totalCp = totalcp;
      this.canceladoPassageiro = this.totalCp.total_viagem_cp;
  });

  this.corridasService.corridaF(date).subscribe(totalf =>{
      this.totalf = totalf;
      this.finalizadas = this.totalf.total_viagem_f;
  });

  
   }

  ngOnInit() {
  }

}

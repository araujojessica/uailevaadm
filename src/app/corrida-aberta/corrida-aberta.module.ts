import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CorridaAbertaPageRoutingModule } from './corrida-aberta-routing.module';

import { CorridaAbertaPage } from './corrida-aberta.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CorridaAbertaPageRoutingModule
  ],
  declarations: [CorridaAbertaPage]
})
export class CorridaAbertaPageModule {}

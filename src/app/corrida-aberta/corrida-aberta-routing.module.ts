import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CorridaAbertaPage } from './corrida-aberta.page';

const routes: Routes = [
  {
    path: '',
    component: CorridaAbertaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CorridaAbertaPageRoutingModule {}

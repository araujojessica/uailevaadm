import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, ToastController } from '@ionic/angular';
import { MotoristaService } from 'src/servico/motorista/motorista';
import { UsuarioService } from 'src/servico/usuario/usuario';
import { ViagemService } from 'src/servico/viagem/viagem';

@Component({
  selector: 'app-corrida-aberta',
  templateUrl: './corrida-aberta.page.html',
  styleUrls: ['./corrida-aberta.page.scss'],
})
export class CorridaAbertaPage implements OnInit {
id_viagem:any;
status:any;
nome:any;
motorista:any;
valor:any;
origem:any;
destino:any;
mottel:any;
usertel:any;
id_motorista:any;

  constructor(
    private modalCtrl: ModalController,
    private params: NavParams,
    private viagemService:ViagemService,
    private usuarioService: UsuarioService,
    private motoristaService: MotoristaService,
    private toastCtrl: ToastController
  ) { 

    this.id_viagem = this.params.get('viagem');
    
    this.viagemService.viagem(this.id_viagem).subscribe( viagem => {

      if(viagem.viagem_status == 'ACEITO'){
        this.status = 'MOTORISTA  A CAMINHO DO PASSAGEIRO';
      }

      if(viagem.viagem_status == 'INICIANDO'){
        this.status = 'MOTORISTA A CAMINHO DO DESTINO';
      }

      if(viagem.viagem_status == 'CHEGUEI'){
        this.status = 'MOTORISTA AGUARDANDO PASSAGEIRO';
      }
      
      this.mottel = viagem.phone;
      this.usertel = viagem.user_phone;
      this.origem = viagem.origem;
      this.destino = viagem.destino;
      this.valor = viagem.valor_corrida;
      this.id_motorista = viagem.id_motorista;
      this.usuarioService.usuario(viagem.id_usuario).subscribe(usuario => {
        this.nome = usuario.full_name;
      });
        this.motoristaService.motorista(viagem.id_motorista).subscribe(motorista => {
        this.motorista = motorista.full_name;
        
      })

    });
  }

  ngOnInit() {
  }

  dismiss(){
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  cancelar(id_viagem){
    this.viagemService.cancelar(id_viagem).subscribe(
      async () => {
       
        const toast = await (await this.toastCtrl.create({
          message: 'Viagem cancelada com sucesso !',
          duration: 4000, position: 'middle',
          color: 'success'
        })).present();
               
      },  
        async err => {
          console.log(err)
          const toast = await (await this.toastCtrl.create({
            message: 'Ocorreu um erro!',
            duration: 4000, position: 'middle',
            color: 'danger'
          })).present();
        
    }
    )
    this.updateOnline();
   
  }

  finalizar(id_viagem){
    this.viagemService.finalizar(id_viagem).subscribe(
      async () => {
       
        const toast = await (await this.toastCtrl.create({
          message: 'Viagem finalizada com sucesso !',
          duration: 4000, position: 'middle',
          color: 'success'
        })).present();
               
      },  
        async err => {
          console.log(err)
          const toast = await (await this.toastCtrl.create({
            message: 'Ocorreu um erro!',
            duration: 4000, position: 'middle',
            color: 'danger'
          })).present();
        
    }
    )
    this.updateOnline();
  }
  updateOnline() {
    this.viagemService.updateViagemVT(this.id_motorista).subscribe();
  }

}

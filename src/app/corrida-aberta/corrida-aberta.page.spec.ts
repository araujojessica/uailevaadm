import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CorridaAbertaPage } from './corrida-aberta.page';

describe('CorridaAbertaPage', () => {
  let component: CorridaAbertaPage;
  let fixture: ComponentFixture<CorridaAbertaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorridaAbertaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CorridaAbertaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, MenuController, ModalController, NavController, ToastController } from '@ionic/angular';
import { RegisterService } from 'src/services/user/registerService';
import { newUser } from 'src/interface/newUser';
import { UserNotTakenValidatorService } from './user-not-taken.validator.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  tipo: boolean;
  public onRegisterForm: FormGroup;
  nome: string;
  phone: string;

  constructor(
    private router: Router,
    private navCtrl: NavController,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private menuCtrl: MenuController,
    private formBuilder: FormBuilder,
    private signupService: RegisterService,
    private userNotTakenValidatorService: UserNotTakenValidatorService
  ) { }

  ngOnInit() {

    this.onRegisterForm = this.formBuilder.group({

      name: ['',
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(40)
          ]
      ],
      secondName: ['',
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(40)
          ]
      ],           
      phone: ['',
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(40)
          ],this.userNotTakenValidatorService.checkUserNameTaken()
      ],
      email: ['',
          [
            Validators.required,
            Validators.email
          ],this.userNotTakenValidatorService.checkEmailTaken()
      ],

      cidade: ['',
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(40)
          ]
      ],
      password: ['',
          [
              Validators.required,
              Validators.minLength(6),
              Validators.maxLength(14)
          ]
      ]
    });

  }

  mostrarSenha(){
    this.tipo = !this.tipo;
  }

  signup() {    
    const newUser  = this.onRegisterForm.getRawValue() as newUser;
    this.nome = newUser.name;
    this.phone = newUser.phone;

    newUser.phone = newUser.phone.replace('-','').replace(' ','').replace('(','').replace(')','');

    this.signupService.signup(newUser).subscribe(
        
      async () => {
        
        this.router.navigate(['/etapas/'+this.nome+'/'+newUser.phone+'']);   
        const toast = (await this.toastCtrl.create({
          message: 'Usuário cadastrado com sucesso! Finalize as etapas do cadastro para entrar em análise!',
          duration: 4000, position: 'middle',
          color: 'success'
        })).present();               
      
      },  
      async err => {
        console.log(err)
        const toast = await (await this.toastCtrl.create({
          message: 'Por favor confira se a sua internet está funcionando e tente cadastrar novamente! UaiLeva agradece!',
          duration: 4000, position: 'top',
          color: 'danger'
        })).present();
      }
    );
  }

 /* toEtapas(){
    this.router.navigate(['/etapas']);
  }*/

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QuitarPageRoutingModule } from './quitar-routing.module';

import { QuitarPage } from './quitar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QuitarPageRoutingModule
  ],
  declarations: [QuitarPage]
})
export class QuitarPageModule {}

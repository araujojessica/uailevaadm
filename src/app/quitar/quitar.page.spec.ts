import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QuitarPage } from './quitar.page';

describe('QuitarPage', () => {
  let component: QuitarPage;
  let fixture: ComponentFixture<QuitarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuitarPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QuitarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

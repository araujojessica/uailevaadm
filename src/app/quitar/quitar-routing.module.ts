import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuitarPage } from './quitar.page';

const routes: Routes = [
  {
    path: '',
    component: QuitarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QuitarPageRoutingModule {}

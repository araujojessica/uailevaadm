import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ModalController, NavParams, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/core/user/user';
import { UserService } from 'src/core/user/user.service';
import { MotoristaLiberacao } from 'src/interface/motoristaLiberacao';
import { MotoristaLiberacaoService } from 'src/servico/motorista_liberacao';

@Component({
  selector: 'app-quitar',
  templateUrl: './quitar.page.html',
  styleUrls: ['./quitar.page.scss'],
})
export class QuitarPage implements OnInit {

  user$: Observable<User>;
  usuarioLogado: User;
  user_id: number;
  motorista_id: number;
  apelido: string = ' - ';
  nome: string;
  m: MotoristaLiberacao[];
  mot: any;
  photo: any;
  url_foto: string = 'assets/img/no.png';
  photo_crlv: any;
  url_crlv: string = 'assets/img/no.png';
  photo_cnh: any;
  url_cnh: string = 'assets/img/no.png';
  nomeCompleto:string;
  placa: string;
  modelo: string;
  secondNome:string;
  phone:string;
  email:string;
  password:string;
  cidade:string;
  totalArrecadado:any;
  totalUai:any;
  
  teste: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private motoristaService: UserService,
    private motoristaLiberacaoService: MotoristaLiberacaoService,
    private toastCtrl: ToastController,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private params: NavParams
  ) { 

    this.route.params.subscribe( parametros => {
      //this.motorista_id = parametros['user_id'];
      this.motorista_id = this.params.get('motorista');
      this.motoristaLiberacaoService.motoristaUnico(this.motorista_id).subscribe( motorista => {
        this.mot = motorista;
        console.log(this.mot);
        this.nome = motorista.full_name;
        //this.phone = this.mot.phone;
        this.email = this.mot.email;
        this.password = this.mot.password;
        this.cidade = this.mot.cidade;
        this.apelido = this.mot.apelido;
        this.motoristaLiberacaoService.fotoMotorista(this.mot.phone).subscribe( foto_motorista => {
          this.photo = foto_motorista;
          this.url_foto = `https://uaileva.com.br/api/imgs/` + this.photo.url;
        });

        this.motoristaLiberacaoService.fotoCrlv(this.mot.phone).subscribe( foto_crlv => {
          this.photo_crlv = foto_crlv;
          this.url_crlv = `https://uaileva.com.br/api/imgs/` + this.photo_crlv.url;
        });

        this.motoristaLiberacaoService.fotoCnh(this.mot.phone).subscribe( foto_cnh => {
          this.photo_cnh = foto_cnh;
          this.url_cnh = `https://uaileva.com.br/api/imgs/` + this.photo_cnh.url;
        });
      });  
      this.motoristaLiberacaoService.arrecadacao(this.motorista_id).subscribe(arrecadacao => {
          this.totalArrecadado = arrecadacao.total;
          this.totalUai = arrecadacao.uaileva;

      })
    });
    

  }

  ngOnInit() {
  }

  dismiss(){
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { Downloader, DownloadRequest, NotificationVisibility } from '@ionic-native/downloader';

@Component({
  selector: 'app-download',
  templateUrl: './download.page.html',
  styleUrls: ['./download.page.scss'],
})
export class DownloadPage implements OnInit {

  constructor(private downloader: Downloader) { }

  ngOnInit() {
  }

  download(){
    var request: DownloadRequest = {
      uri: '/assets/img/bg',
      title: 'MyDownload',
      description: '',
      mimeType: '',
      visibleInDownloadsUi: true,
      notificationVisibility: NotificationVisibility.VisibleNotifyCompleted,
      destinationInExternalFilesDir: {
          dirType: 'Downloads',
          subPath: 'UaiLeva.apk'
      }
  };


this.downloader.download(request)
          .then((location: string) => console.log('File downloaded at:'+location))
          .catch((error: any) => console.error(error));
  } 

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditarMotoristaPage } from './editar-motorista.page';

const routes: Routes = [
  {
    path: '',
    component: EditarMotoristaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditarMotoristaPageRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/core/user/user';
import { UserService } from 'src/core/user/user.service';
import { MotoristaLiberacao } from 'src/interface/motoristaLiberacao';
import { EditarPerfilService } from 'src/servico/editar-motorista';
import { MotoristaLiberacaoService } from 'src/servico/motorista_liberacao';

@Component({
  selector: 'app-editar-motorista',
  templateUrl: './editar-motorista.page.html',
  styleUrls: ['./editar-motorista.page.scss'],
})
export class EditarMotoristaPage implements OnInit {

  user$: Observable<User>;
  usuarioLogado: User;
  user_id: number;
  motorista_id: number;
  apelido: string = ' - ';
  nome: string;
  m: MotoristaLiberacao[];
  mot: any;
  photo: any;
  url_foto: string = 'assets/img/no.png';
  photo_crlv: any;
  url_crlv: string = 'assets/img/no.png';
  photo_cnh: any;
  url_cnh: string = 'assets/img/no.png';
  nomeCompleto:string;
  placa: string;
  modelo: string;
  secondNome:string;
  phone:string;
  email:string;
  password:string;
  cidade:string;
  motId: number;
  nomeNovo:string = '';
  apelidoNovo:string = '';
  phoneNovo:string = '';
  emailNovo:string = '';
  senhaNovo:string = '';
  cidadeNovo:string = '';
  placaNovo:string = '';
  modeloNovo:string = '';
  file: File;
  fileCNH: File;
  fileCRLV: File;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private motoristaService: UserService,
    private motoristaLiberacaoService: MotoristaLiberacaoService,
    private toastCtrl: ToastController,
    private editarService: EditarPerfilService,
  ) { 

    this.user$ = motoristaService.getUser();

    this.user$.subscribe( usuario => {
      this.usuarioLogado = usuario;
      this.user_id = this.usuarioLogado.id;

      var a = this.usuarioLogado.full_name.split(' ');
      if(this.usuarioLogado.apelido == null){
        this.apelido = '';
      } else {
        this.apelido = '(' + this.usuarioLogado.apelido + ')';
      }
      this.nome = a[0] + ' ' + this.apelido;    
    });
  
    
    this.route.params.subscribe( parametros => {
      this.motorista_id = parametros['user_id'];
      
      this.motoristaLiberacaoService.motoristaLiberadoUnico(this.motorista_id).subscribe( motorista => {

        this.mot = motorista;
        this.nome = motorista.full_name;
        this.phone = this.mot.phone;
        this.email = this.mot.email;
        this.password = this.mot.password;
        this.cidade = this.mot.cidade;
        this.apelido = this.mot.apelido;
        this.placa = this.mot.placa;
        this.modelo = this.mot.modelo;
        this.motId = this.mot.id;

        this.motoristaLiberacaoService.fotoMotorista(this.mot.phone).subscribe( foto_motorista => {
          this.photo = foto_motorista;
          this.url_foto = `https://uaileva.com.br/api/imgs/` + this.photo.url;
        });

        this.motoristaLiberacaoService.fotoCrlv(this.mot.phone).subscribe( foto_crlv => {
          this.photo_crlv = foto_crlv;
          this.url_crlv = `https://uaileva.com.br/api/imgs/` + this.photo_crlv.url;
        });

        this.motoristaLiberacaoService.fotoCnh(this.mot.phone).subscribe( foto_cnh => {
          this.photo_cnh = foto_cnh;
          this.url_cnh = `https://uaileva.com.br/api/imgs/` + this.photo_cnh.url;
        });
      });  
    });

  }

  ngOnInit() {
  }

  salvar() {

    if(this.nomeNovo != ''){
      console.log("" + this.nomeNovo) 

      this.editarService.updateNome( this.motorista_id, this.nomeNovo, '').subscribe( async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Nome editado com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();
       
     
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });

    }
    if(this.apelidoNovo != ''){
      console.log("" + this.apelidoNovo) 

      this.editarService.apelido( this.motorista_id, this.apelidoNovo).subscribe( async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Apelido editado com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();
       
     
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });
    }
  
    if(this.phoneNovo != ''){
      console.log("" + this.phoneNovo) 
      this.editarService.phone( this.motorista_id, this.phoneNovo).subscribe( async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Phone editada com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();
       
     
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });
    }
    if(this.emailNovo != ''){
      console.log("" + this.emailNovo) 

      this.editarService.email( this.motorista_id, this.emailNovo).subscribe( async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Email editado com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();
       
     
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });
    }
    if(this.senhaNovo != ''){
      console.log("" + this.senhaNovo) 

      this.editarService.updatePassword( this.motorista_id, this.senhaNovo).subscribe( async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Senha editado com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();       
     
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });
       
    }
    if(this.cidadeNovo != ''){
      console.log("" + this.cidadeNovo) 

      this.editarService.cidade( this.motorista_id, this.cidadeNovo).subscribe( async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Cidade editada com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();
       
     
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });
       
    }
    if(this.placaNovo != '' || this.modeloNovo != '' || this.placaNovo != undefined || this.placaNovo != null || this.modeloNovo != null || this.modeloNovo != undefined){
      console.log("" + this.placaNovo) 

      if(this.modeloNovo == '') {
        this.modeloNovo = this.modelo;
      }
      if(this.placaNovo == '' ) {
        this.placaNovo = this.placa;
      }
      this.editarService.placa( this.motorista_id, this.placaNovo, this.modeloNovo).subscribe( async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Placa do carro editado com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();
       
     
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });
    }
    

    if(this.file != undefined) {

      console.log(this.file)
      let allowComments = true;
      let description = this.phone + '/ USER / PEFIL';
  
      this.editarService.updateFoto(description,allowComments, this.file).subscribe(async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Foto editada com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();       
     
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });

    }
console.log(this.phone)
    if(this.fileCNH != undefined){
      console.log(this.fileCNH)
      let allowComments = true;
      let description = this.phone + '/ MOTORISTA / CNH';
  
      this.editarService.updateFoto(description,allowComments, this.file).subscribe(async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Foto editada com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();       
     
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });
  
    }

    if(this.fileCRLV != undefined){
      console.log(this.fileCRLV)
      let allowComments = true;
      let description = this.phone + '/ MOTORISTA / CRLV';
  
      this.editarService.updateFoto(description,allowComments, this.file).subscribe(async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Foto editada com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();       
     
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });
    }
  }  
}

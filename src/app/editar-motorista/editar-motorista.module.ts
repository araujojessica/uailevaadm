import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditarMotoristaPageRoutingModule } from './editar-motorista-routing.module';

import { EditarMotoristaPage } from './editar-motorista.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditarMotoristaPageRoutingModule
  ],
  declarations: [EditarMotoristaPage]
})
export class EditarMotoristaPageModule {}

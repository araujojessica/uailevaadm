import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditarMotoristaPage } from './editar-motorista.page';

describe('EditarMotoristaPage', () => {
  let component: EditarMotoristaPage;
  let fixture: ComponentFixture<EditarMotoristaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarMotoristaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditarMotoristaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

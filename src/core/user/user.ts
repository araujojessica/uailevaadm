export interface User {
    id: number;
    phone: string,
    email: string,
    password: string,
    full_name: string,
    cidade:string,
    photo: string,
    photo_cnh: string,
    photo_crlv: string,
    logado: string,
    status: string,
    acordo: string,
    adm:string,
    apelido:string,
    sexo:string
    
    
}
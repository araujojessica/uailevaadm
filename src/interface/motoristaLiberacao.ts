export interface MotoristaLiberacao {
    id: number,
    email: string,
    password: string,
    full_name: string,
    cidade: string,
    phone: string,
    photo: string,
    photo_cnh: string,
    photo_crlv: string,
    logado: string,
    apelido: string,
    sexo:string,
    status: string,
    acordo: string,
    data_logado:string,
   
}
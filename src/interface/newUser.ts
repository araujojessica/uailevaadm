export interface newUser {

    id: string;
    name: string;
    secondName: string;
    phone: string;
    email: string;
    cidade: string;
    password: string;
}
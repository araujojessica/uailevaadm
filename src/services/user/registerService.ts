import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { newUser } from "src/interface/newUser";
import { Foto } from "src/interface/foto";
import { User } from "src/core/user/user";


const API_URL = "https://uaileva.com.br/api"; 
//const API_URL = "http://191.252.3.12:3000"; 

@Injectable({providedIn: 'root'})
export class RegisterService{

    constructor(private http: HttpClient){}
    
    checkUserNameTaken(phone: string) {
        return this.http.get(API_URL + '/motorista/exists/' + phone);
    } 
    checkEmailTaken(email: string) {
        return this.http.get(API_URL + '/motorista/exists/email/' + email);
    } 

    signup(newUser : newUser){
        return this.http.post(API_URL +  '/motorista/signup', newUser);
    }

    getAll() {
        return this.http.get<newUser[]>(API_URL +  `/motorista`);
    }

    getById(id: number) {
        return this.http.get( API_URL + `/motorista/` + id);
    }

    register(user: newUser) {
        return this.http.post(API_URL + `/motorista/register`, user);
    }

    update(user: newUser) {
        return this.http.put(API_URL + `/motorista/` + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(API_URL + `/motorista/` + id);
    }

    upload(description: string, allowComments: boolean, file: File) {
        {
            const formData = new FormData();
            formData.append('description', description);
            formData.append('allowComments', allowComments ? 'true' : 'false');
            formData.append('imageFile', file);           
            
        return this.http.post(API_URL + '/photos/upload', formData);
        }
    }    

    updateFoto(description: string, allowComments: boolean, file: File) {
        {
            const formData = new FormData();
            formData.append('description', description);
            formData.append('allowComments', allowComments ? 'true' : 'false');
            formData.append('imageFile', file);           
            
            return this.http.post(API_URL + '/photos/update', formData);
        }
      }    


    
    acordo(phone:string){
        return this.http.get(API_URL + `/motorista/acordo/` + phone);
    }

    findFotoMotorista(user_id: string) {     
        return this.http.get<Foto>(API_URL + '/photos/motorista/'+user_id+'');
     } 

    findMotorista(phone: string) {     
    return this.http.get<User>(API_URL + '/motorista/taxa/'+phone+'');
    } 



}
(function () {
  function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["viagens-viagens-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/viagens/viagens.page.html":
    /*!*********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/viagens/viagens.page.html ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppViagensViagensPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"login-home\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <br>\r\n  <ion-grid style=\"border: none;\">\r\n    <ion-item-divider mode=\"md\">Viagens Abertas</ion-item-divider>\r\n    <ion-list *ngFor= \"let viagem of viagem\">\r\n      <ion-item (click)=\"corridaAberta(viagem.id_viagem)\">\r\n        <ion-label> Corrida Aberta : {{status}}\r\n            <p style=\"line-height: 14px; font-size: small;\">Motorista: {{motorista}}</p>\r\n            <p style=\"line-height: 14px; font-size: small;\">Passageiro: {{nome}}</p>\r\n        </ion-label>\r\n        <ion-icon name=\"chevron-forward-outline\" slot=\"end\" size=\"small\" style=\"color: gray;\"></ion-icon>\r\n      </ion-item>\r\n    </ion-list>\r\n  </ion-grid>\r\n  \r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "./src/app/viagens/viagens-routing.module.ts":
    /*!***************************************************!*\
      !*** ./src/app/viagens/viagens-routing.module.ts ***!
      \***************************************************/

    /*! exports provided: ViagensPageRoutingModule */

    /***/
    function srcAppViagensViagensRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ViagensPageRoutingModule", function () {
        return ViagensPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _viagens_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./viagens.page */
      "./src/app/viagens/viagens.page.ts");

      var routes = [{
        path: '',
        component: _viagens_page__WEBPACK_IMPORTED_MODULE_3__["ViagensPage"]
      }];

      var ViagensPageRoutingModule = function ViagensPageRoutingModule() {
        _classCallCheck(this, ViagensPageRoutingModule);
      };

      ViagensPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ViagensPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/viagens/viagens.module.ts":
    /*!*******************************************!*\
      !*** ./src/app/viagens/viagens.module.ts ***!
      \*******************************************/

    /*! exports provided: ViagensPageModule */

    /***/
    function srcAppViagensViagensModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ViagensPageModule", function () {
        return ViagensPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _viagens_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./viagens-routing.module */
      "./src/app/viagens/viagens-routing.module.ts");
      /* harmony import */


      var _viagens_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./viagens.page */
      "./src/app/viagens/viagens.page.ts");

      var ViagensPageModule = function ViagensPageModule() {
        _classCallCheck(this, ViagensPageModule);
      };

      ViagensPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _viagens_routing_module__WEBPACK_IMPORTED_MODULE_5__["ViagensPageRoutingModule"]],
        declarations: [_viagens_page__WEBPACK_IMPORTED_MODULE_6__["ViagensPage"]]
      })], ViagensPageModule);
      /***/
    },

    /***/
    "./src/app/viagens/viagens.page.scss":
    /*!*******************************************!*\
      !*** ./src/app/viagens/viagens.page.scss ***!
      \*******************************************/

    /*! exports provided: default */

    /***/
    function srcAppViagensViagensPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-card, img {\n  border-radius: 5%;\n  border: #001c4e 0.5px solid;\n}\n\nion-toolbar {\n  --background: #001c4e;\n}\n\nion-grid {\n  margin: auto;\n  display: block;\n  width: 90%;\n  border: 1px solid;\n  border-left: none;\n  border-right: none;\n}\n\nion-title {\n  text-align: center;\n  color: #001c4e;\n  font-size: 1.5em;\n  display: block;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlhZ2Vucy92aWFnZW5zLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFBO0VBQ0EsMkJBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0FBQ0o7O0FBRUE7RUFDSSxZQUFBO0VBQ0EsY0FBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQUNKIiwiZmlsZSI6InNyYy9hcHAvdmlhZ2Vucy92aWFnZW5zLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkLCBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNSU7XHJcbiAgICBib3JkZXI6ICMwMDFjNGUgMC41cHggc29saWQ7XHJcbn1cclxuXHJcbmlvbi10b29sYmFyIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzAwMWM0ZTtcclxufSBcclxuXHJcbmlvbi1ncmlkIHtcclxuICAgIG1hcmdpbjogYXV0bzsgXHJcbiAgICBkaXNwbGF5OiBibG9jazsgXHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICBib3JkZXItbGVmdDogbm9uZTtcclxuICAgIGJvcmRlci1yaWdodDogbm9uZTtcclxufVxyXG5cclxuaW9uLXRpdGxlIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjsgXHJcbiAgICBjb2xvcjogIzAwMWM0ZTtcclxuICAgIGZvbnQtc2l6ZTogMS41ZW07XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/viagens/viagens.page.ts":
    /*!*****************************************!*\
      !*** ./src/app/viagens/viagens.page.ts ***!
      \*****************************************/

    /*! exports provided: ViagensPage */

    /***/
    function srcAppViagensViagensPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ViagensPage", function () {
        return ViagensPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_servico_motorista_motorista__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/servico/motorista/motorista */
      "./src/servico/motorista/motorista.ts");
      /* harmony import */


      var src_servico_usuario_usuario__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/servico/usuario/usuario */
      "./src/servico/usuario/usuario.ts");
      /* harmony import */


      var src_servico_viagem_viagem__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/servico/viagem/viagem */
      "./src/servico/viagem/viagem.ts");
      /* harmony import */


      var _corrida_aberta_corrida_aberta_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ../corrida-aberta/corrida-aberta.page */
      "./src/app/corrida-aberta/corrida-aberta.page.ts");

      var ViagensPage = /*#__PURE__*/function () {
        function ViagensPage(modalCtrl, viagemService, usuarioService, motoristaService) {
          var _this = this;

          _classCallCheck(this, ViagensPage);

          this.modalCtrl = modalCtrl;
          this.viagemService = viagemService;
          this.usuarioService = usuarioService;
          this.motoristaService = motoristaService;
          this.viagemService.viagemAberta().subscribe(function (viagem) {
            _this.viagem = viagem;

            var _iterator = _createForOfIteratorHelper(_this.viagem),
                _step;

            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var v = _step.value;

                if (v.status == 'ACEITO') {
                  _this.status = 'MOTORISTA A CAMINHO DO PASSAGEIRO';
                }

                if (v.status == 'INICIANDO') {
                  _this.status = 'MOTORISTA A CAMINHO DO DESTINO';
                }

                if (v.status == 'CHEGUEI') {
                  _this.status = 'MOTORISTA AGUARDANDO PASSAGEIRO';
                }

                _this.usuarioService.usuario(v.id_usuario).subscribe(function (usuario) {
                  _this.nome = usuario.full_name;
                });

                _this.motoristaService.motorista(v.id_motorista).subscribe(function (motorista) {
                  _this.motorista = motorista.full_name;
                });
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }
          });
        }

        _createClass(ViagensPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "corridaAberta",
          value: function corridaAberta(id_viagem) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var modal;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.modalCtrl.create({
                        component: _corrida_aberta_corrida_aberta_page__WEBPACK_IMPORTED_MODULE_6__["CorridaAbertaPage"],
                        componentProps: {
                          viagem: id_viagem
                        }
                      });

                    case 2:
                      modal = _context.sent;
                      _context.next = 5;
                      return modal.present();

                    case 5:
                      return _context.abrupt("return", _context.sent);

                    case 6:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }]);

        return ViagensPage;
      }();

      ViagensPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"]
        }, {
          type: src_servico_viagem_viagem__WEBPACK_IMPORTED_MODULE_5__["ViagemService"]
        }, {
          type: src_servico_usuario_usuario__WEBPACK_IMPORTED_MODULE_4__["UsuarioService"]
        }, {
          type: src_servico_motorista_motorista__WEBPACK_IMPORTED_MODULE_3__["MotoristaService"]
        }];
      };

      ViagensPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-viagens',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./viagens.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/viagens/viagens.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./viagens.page.scss */
        "./src/app/viagens/viagens.page.scss"))["default"]]
      })], ViagensPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=viagens-viagens-module-es5.js.map
(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["editar-passageiro-editar-passageiro-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/editar-passageiro/editar-passageiro.page.html":
    /*!*****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/editar-passageiro/editar-passageiro.page.html ***!
      \*****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppEditarPassageiroEditarPassageiroPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"login-home\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <br><br>\r\n  <ion-title>Editar Perfil</ion-title><br>\r\n  <form style=\"padding-left: 25px; padding-right: 25px;\">\r\n    <ion-item>\r\n      <ion-label position=\"stacked\">Nome Completo</ion-label>\r\n      <ion-input [(ngModel)]=\"nomeNovo\" placeholder=\"{{nome}}\" name = \"nomeNovo\" type=\"text\"></ion-input>\r\n    </ion-item>\r\n    \r\n    <ion-item>\r\n      <ion-label position=\"stacked\">Apelido</ion-label>\r\n      <ion-input [(ngModel)]=\"apelidoNovo\" placeholder=\"{{apelido}}\" name = \"apelidoNovo\" type=\"text\"></ion-input>\r\n    </ion-item>\r\n    \r\n    <ion-item>\r\n      <ion-label position=\"stacked\">Telefone</ion-label>\r\n      <ion-input [(ngModel)]=\"phoneNovo\" placeholder=\"{{phone}}\" name = \"phoneNovo\" type=\"tel\"></ion-input>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label position=\"stacked\">E-mail</ion-label>\r\n      <ion-input [(ngModel)]=\"emailNovo\" placeholder=\"{{email}}\" name = \"emailNovo\" type=\"text\"></ion-input>\r\n    </ion-item>\r\n    \r\n    <!--<ion-item>\r\n      <ion-label position=\"stacked\">Senha</ion-label>\r\n      <ion-input name = \"senhaNovo\" placeholder=\"********\" type=\"password\"></ion-input>\r\n    </ion-item>-->\r\n\r\n    <ion-item>\r\n      <ion-label position=\"stacked\">Cidade</ion-label>\r\n      <ion-input [(ngModel)]=\"cidadeNovo\" placeholder=\"{{cidade}}\"  name = \"cidadeNovo\" type=\"text\"></ion-input>\r\n    </ion-item><br>\r\n\r\n    <ion-grid>\r\n      <ion-col class=\"col-md-12\"><img src=\"{{url_foto}}\"/></ion-col>&nbsp;\r\n    </ion-grid>\r\n\r\n    <ion-grid>\r\n      <ion-col class=\"col-md-12\">\r\n        <ion-label>Foto perfil : </ion-label>&nbsp;&nbsp;\r\n        <input type=\"file\" (change)= \"file = $event.target.files[0]\" accept=\"image/*\">\r\n      </ion-col>&nbsp;\r\n    </ion-grid>\r\n  </form><br>\r\n\r\n  <ion-button class=\"botao\" tappable>\r\n    Salvar Alterações\r\n    <ion-icon name=\"checkmark-outline\" style=\"color: #001c4e;\"></ion-icon>\r\n  </ion-button>\r\n<br>\r\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/editar-passageiro/editar-passageiro-routing.module.ts":
    /*!***********************************************************************!*\
      !*** ./src/app/editar-passageiro/editar-passageiro-routing.module.ts ***!
      \***********************************************************************/

    /*! exports provided: EditarPassageiroPageRoutingModule */

    /***/
    function srcAppEditarPassageiroEditarPassageiroRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditarPassageiroPageRoutingModule", function () {
        return EditarPassageiroPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _editar_passageiro_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./editar-passageiro.page */
      "./src/app/editar-passageiro/editar-passageiro.page.ts");

      var routes = [{
        path: '',
        component: _editar_passageiro_page__WEBPACK_IMPORTED_MODULE_3__["EditarPassageiroPage"]
      }];

      var EditarPassageiroPageRoutingModule = function EditarPassageiroPageRoutingModule() {
        _classCallCheck(this, EditarPassageiroPageRoutingModule);
      };

      EditarPassageiroPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], EditarPassageiroPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/editar-passageiro/editar-passageiro.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/editar-passageiro/editar-passageiro.module.ts ***!
      \***************************************************************/

    /*! exports provided: EditarPassageiroPageModule */

    /***/
    function srcAppEditarPassageiroEditarPassageiroModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditarPassageiroPageModule", function () {
        return EditarPassageiroPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _editar_passageiro_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./editar-passageiro-routing.module */
      "./src/app/editar-passageiro/editar-passageiro-routing.module.ts");
      /* harmony import */


      var _editar_passageiro_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./editar-passageiro.page */
      "./src/app/editar-passageiro/editar-passageiro.page.ts");

      var EditarPassageiroPageModule = function EditarPassageiroPageModule() {
        _classCallCheck(this, EditarPassageiroPageModule);
      };

      EditarPassageiroPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _editar_passageiro_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditarPassageiroPageRoutingModule"]],
        declarations: [_editar_passageiro_page__WEBPACK_IMPORTED_MODULE_6__["EditarPassageiroPage"]]
      })], EditarPassageiroPageModule);
      /***/
    },

    /***/
    "./src/app/editar-passageiro/editar-passageiro.page.scss":
    /*!***************************************************************!*\
      !*** ./src/app/editar-passageiro/editar-passageiro.page.scss ***!
      \***************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppEditarPassageiroEditarPassageiroPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-card, img {\n  border-radius: 5%;\n  border: #001c4e 0.5px solid;\n}\n\nion-toolbar {\n  --background: #001c4e;\n}\n\nion-grid {\n  margin: auto;\n  display: block;\n  width: 95%;\n  border: 1px solid;\n  border-left: none;\n  border-right: none;\n  padding: 0px;\n}\n\nion-input {\n  --highlight-color-focused: var(--ion-color-primary, #001c4e);\n  --highlight-color-valid: var(--ion-color-success, #2dd36f);\n  --highlight-color-invalid: var(--ion-color-danger, #eb445a);\n  --item-highlight: var(--ion-color-primary, #001c4e);\n}\n\nion-title {\n  text-align: center;\n  color: #001c4e;\n  font-size: 1.5em;\n  display: block;\n}\n\n.botao {\n  --background: #b1ff49;\n  --color: #001c4e;\n  font-weight: bold;\n  display: block;\n  height: 48px;\n  width: 45%;\n  margin: auto;\n  text-transform: uppercase;\n  --border-radius: 50px;\n  --background-activated: #001c4e;\n  --color-activated: white;\n  --background-hover: #001c4e;\n  --color-hover: white;\n  --box-shadow: 0 0 0 0;\n  outline: none !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZWRpdGFyLXBhc3NhZ2Vpcm8vZWRpdGFyLXBhc3NhZ2Vpcm8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSwyQkFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7QUFDSjs7QUFHQTtFQUNJLFlBQUE7RUFDQSxjQUFBO0VBQ0EsVUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUFBSjs7QUFJQTtFQUNJLDREQUFBO0VBQ0EsMERBQUE7RUFDQSwyREFBQTtFQUNBLG1EQUFBO0FBREo7O0FBSUE7RUFDSSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUFESjs7QUFJQTtFQUNJLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSx5QkFBQTtFQUNBLHFCQUFBO0VBQ0EsK0JBQUE7RUFDQSx3QkFBQTtFQUNBLDJCQUFBO0VBQ0Esb0JBQUE7RUFDRCxxQkFBQTtFQUNDLHdCQUFBO0FBREoiLCJmaWxlIjoic3JjL2FwcC9lZGl0YXItcGFzc2FnZWlyby9lZGl0YXItcGFzc2FnZWlyby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZCwgaW1nIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUlO1xyXG4gICAgYm9yZGVyOiAjMDAxYzRlIDAuNXB4IHNvbGlkO1xyXG59XHJcblxyXG5pb24tdG9vbGJhciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGU7XHJcbn1cclxuXHJcblxyXG5pb24tZ3JpZCB7XHJcbiAgICBtYXJnaW46IGF1dG87IFxyXG4gICAgZGlzcGxheTogYmxvY2s7IFxyXG4gICAgd2lkdGg6IDk1JTtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkO1xyXG4gICAgYm9yZGVyLWxlZnQ6IG5vbmU7XHJcbiAgICBib3JkZXItcmlnaHQ6IG5vbmU7XHJcbiAgICBwYWRkaW5nOiAwcHg7XHJcbn1cclxuXHJcblxyXG5pb24taW5wdXQge1xyXG4gICAgLS1oaWdobGlnaHQtY29sb3ItZm9jdXNlZDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnksICMwMDFjNGUpO1xyXG4gICAgLS1oaWdobGlnaHQtY29sb3ItdmFsaWQ6IHZhcigtLWlvbi1jb2xvci1zdWNjZXNzLCAjMmRkMzZmKTtcclxuICAgIC0taGlnaGxpZ2h0LWNvbG9yLWludmFsaWQ6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIsICNlYjQ0NWEpO1xyXG4gICAgLS1pdGVtLWhpZ2hsaWdodDogdmFyKC0taW9uLWNvbG9yLXByaW1hcnksICMwMDFjNGUpO1xyXG59XHJcblxyXG5pb24tdGl0bGUge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyOyBcclxuICAgIGNvbG9yOiAjMDAxYzRlO1xyXG4gICAgZm9udC1zaXplOiAxLjVlbTtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4uYm90YW8ge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjYjFmZjQ5OyAgLy8jMDAxYzRlO1xyXG4gICAgLS1jb2xvcjogIzAwMWM0ZTsgLy93aGl0ZTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBoZWlnaHQ6IDQ4cHg7XHJcbiAgICB3aWR0aDogNDUlO1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIC0tYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6ICMwMDFjNGU7XHJcbiAgICAtLWNvbG9yLWFjdGl2YXRlZDogd2hpdGU7XHJcbiAgICAtLWJhY2tncm91bmQtaG92ZXI6ICMwMDFjNGU7XHJcbiAgICAtLWNvbG9yLWhvdmVyOiB3aGl0ZTtcclxuICAgLS1ib3gtc2hhZG93OiAwIDAgMCAwO1xyXG4gICAgb3V0bGluZTogbm9uZSAhaW1wb3J0YW50O1xyXG59Il19 */";
      /***/
    },

    /***/
    "./src/app/editar-passageiro/editar-passageiro.page.ts":
    /*!*************************************************************!*\
      !*** ./src/app/editar-passageiro/editar-passageiro.page.ts ***!
      \*************************************************************/

    /*! exports provided: EditarPassageiroPage */

    /***/
    function srcAppEditarPassageiroEditarPassageiroPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EditarPassageiroPage", function () {
        return EditarPassageiroPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var EditarPassageiroPage = /*#__PURE__*/function () {
        function EditarPassageiroPage() {
          _classCallCheck(this, EditarPassageiroPage);
        }

        _createClass(EditarPassageiroPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return EditarPassageiroPage;
      }();

      EditarPassageiroPage.ctorParameters = function () {
        return [];
      };

      EditarPassageiroPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-editar-passageiro',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./editar-passageiro.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/editar-passageiro/editar-passageiro.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./editar-passageiro.page.scss */
        "./src/app/editar-passageiro/editar-passageiro.page.scss"))["default"]]
      })], EditarPassageiroPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=editar-passageiro-editar-passageiro-module-es5.js.map
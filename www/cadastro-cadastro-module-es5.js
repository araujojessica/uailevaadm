(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cadastro-cadastro-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/cadastro/cadastro.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cadastro/cadastro.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppCadastroCadastroPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"login-home\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <br><br>\r\n  <ion-title>Cadastro de Motorista</ion-title><br>\r\n    <ion-grid>\r\n      <ion-row>\r\n        <ion-col>\r\n        <form [formGroup]=\"onRegisterForm\" style=\"padding: 10px;\">\r\n\r\n          <ion-item>\r\n            <ion-input type=\"text\" name=\"nome\" placeholder=\"Nome\"  id=\"inputNome\" formControlName=\"name\">\r\n              <ion-icon name=\"person-outline\" size=\"small\" slot=\"start\" item-start></ion-icon>&nbsp;\r\n            </ion-input>\r\n          </ion-item>\r\n         <p ion-text class=\"text08\" *ngIf=\"onRegisterForm.get('name').touched && onRegisterForm.get('name').hasError('required')\">\r\n            <ion-text color=\"danger\">\r\n              Campo obrigatório!\r\n            </ion-text>\r\n          </p>\r\n\r\n          <p></p>\r\n\r\n          <ion-item>\r\n            <ion-input type=\"text\" name=\"nome\" placeholder=\"Sobrenome\"  id=\"inputNome\" formControlName=\"secondName\">\r\n              <ion-icon name=\"person-outline\" size=\"small\" slot=\"start\" item-start></ion-icon>&nbsp;\r\n            </ion-input>\r\n          </ion-item>\r\n          <p ion-text class=\"text08\" *ngIf=\"onRegisterForm.get('secondName').touched && onRegisterForm.get('secondName').hasError('required')\">\r\n            <ion-text color=\"danger\">\r\n              Campo obrigatório!\r\n            </ion-text>\r\n          </p>\r\n\r\n          <p></p>\r\n\r\n          <ion-item> \r\n            <ion-input type=\"tel\" name=\"telefone\" placeholder=\"Celular com (DDD)\"  id=\"inputTelefone\" [brmasker]=\"{phone: true}\" formControlName=\"phone\">\r\n              <ion-icon name=\"phone-portrait-outline\" size=\"small\" slot=\"start\"></ion-icon>&nbsp;\r\n            </ion-input>\r\n          </ion-item>\r\n          <p ion-text class=\"text08\" *ngIf=\"onRegisterForm.get('phone').touched && onRegisterForm.get('phone').hasError('required')\">\r\n            <ion-text color=\"danger\">\r\n              Campo obrigatório!\r\n            </ion-text>\r\n          </p>\r\n          \r\n          <div class= \"texto-email\">\r\n            <br>\r\n            <small *ngIf = \"onRegisterForm.get('phone').errors?.userNameTaken\" class= \"text-danger\" >Phone Existente !</small>\r\n            <small *ngIf = \"onRegisterForm.get('phone').valid\" class= \"text-success\" >Phone Válido !</small>\r\n          </div>\r\n\r\n          <p></p>\r\n\r\n          <ion-item>\r\n            <ion-input type=\"text\" name=\"email\" placeholder=\"E-mail\"  id=\"inputEmail\" formControlName=\"email\">\r\n              <ion-icon name=\"mail-outline\" size=\"small\" slot=\"start\"></ion-icon>&nbsp;\r\n            </ion-input>\r\n          </ion-item>\r\n          \r\n          <div class= \"texto-email\">\r\n            <small *ngIf = \"onRegisterForm.get('email').errors?.emailTaken\" class= \"text-danger\" >Email Existente !</small>\r\n            <small *ngIf = \"onRegisterForm.get('email').valid\" class= \"text-success\" >Email Válido !</small>\r\n          </div>\r\n          <p ion-text class=\"text08\" *ngIf=\"onRegisterForm.get('email').touched && onRegisterForm.get('email').hasError('required')\">\r\n            <ion-text color=\"danger\">\r\n              Campo obrigatório!\r\n            </ion-text>\r\n          </p>\r\n\r\n          <p></p>\r\n\r\n          <ion-item>\r\n            <ion-input [type]=\"tipo ? 'text' : 'password'\" name=\"senha\" placeholder=\"Senha\"  id=\"senha\" formControlName=\"password\">\r\n              <ion-icon name=\"lock-closed-outline\" size=\"small\" slot=\"start\"></ion-icon>&nbsp;\r\n            </ion-input>\r\n            <ion-icon [name]=\"tipo ? 'eye-outline' : 'eye-off-outline'\" slot=\"end\" size=\"small\" (click)=\"mostrarSenha()\" style=\"padding-top: 5px;\"></ion-icon>\r\n          </ion-item>\r\n          <p ion-text class=\"text08\" *ngIf=\"onRegisterForm.get('password').touched && onRegisterForm.get('password').hasError('required')\">\r\n            <ion-text color=\"danger\">\r\n              Campo obrigatório! Mínimo 6 caracteres máximo 14\r\n            </ion-text>\r\n          </p>\r\n\r\n          <p></p>\r\n\r\n          <ion-item>\r\n            <ion-input type=\"text\" name=\"cidade\" placeholder=\"Cidade\" id=\"cidade\" formControlName=\"cidade\">\r\n              <ion-icon name=\"business-outline\" size=\"small\" slot=\"start\"></ion-icon>&nbsp;\r\n            </ion-input>\r\n          </ion-item>\r\n          <p ion-text class=\"text08\" *ngIf=\"onRegisterForm.get('cidade').touched && onRegisterForm.get('cidade').hasError('required')\">\r\n            <ion-text color=\"danger\">\r\n              Campo obrigatório!\r\n            </ion-text>\r\n          </p>\r\n\r\n          <p></p>\r\n\r\n          <ion-button class=\"botaoCadastro\" (click)=\"signup()\">\r\n            <ion-icon name=\"checkmark-outline\" slot=\"end\" style=\"color: #001c4e;\"></ion-icon>Cadastrar\r\n          </ion-button>\r\n    </form>\r\n  </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/cadastro/cadastro-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/cadastro/cadastro-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: CadastroPageRoutingModule */

    /***/
    function srcAppCadastroCadastroRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CadastroPageRoutingModule", function () {
        return CadastroPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _cadastro_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./cadastro.page */
      "./src/app/cadastro/cadastro.page.ts");

      var routes = [{
        path: '',
        component: _cadastro_page__WEBPACK_IMPORTED_MODULE_3__["CadastroPage"]
      }];

      var CadastroPageRoutingModule = function CadastroPageRoutingModule() {
        _classCallCheck(this, CadastroPageRoutingModule);
      };

      CadastroPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], CadastroPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/cadastro/cadastro.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/cadastro/cadastro.module.ts ***!
      \*********************************************/

    /*! exports provided: CadastroPageModule */

    /***/
    function srcAppCadastroCadastroModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CadastroPageModule", function () {
        return CadastroPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _cadastro_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./cadastro-routing.module */
      "./src/app/cadastro/cadastro-routing.module.ts");
      /* harmony import */


      var _cadastro_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./cadastro.page */
      "./src/app/cadastro/cadastro.page.ts");
      /* harmony import */


      var br_mask__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! br-mask */
      "./node_modules/br-mask/__ivy_ngcc__/dist/index.js");

      var CadastroPageModule = function CadastroPageModule() {
        _classCallCheck(this, CadastroPageModule);
      };

      CadastroPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], br_mask__WEBPACK_IMPORTED_MODULE_7__["BrMaskerModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _cadastro_routing_module__WEBPACK_IMPORTED_MODULE_5__["CadastroPageRoutingModule"]],
        declarations: [_cadastro_page__WEBPACK_IMPORTED_MODULE_6__["CadastroPage"]]
      })], CadastroPageModule);
      /***/
    },

    /***/
    "./src/app/cadastro/cadastro.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/cadastro/cadastro.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppCadastroCadastroPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-card, img {\n  border-radius: 5%;\n  border: #001c4e 0.5px solid;\n}\n\nion-toolbar {\n  --background: #001c4e;\n}\n\nion-title {\n  text-align: center;\n  color: #001c4e;\n  font-size: 1.5em;\n  display: block;\n}\n\nion-grid {\n  margin: auto;\n  display: block;\n  width: 90%;\n  border: 1px solid;\n  border-left: none;\n  border-right: none;\n}\n\n.botaoCadastro {\n  --background: #b1ff49;\n  --color: #001c4e;\n  font-weight: bold;\n  display: block;\n  height: 48px;\n  width: 95%;\n  margin: auto;\n  --border-radius: 50px;\n  --background-activated: #001c4e;\n  --color-activated: white;\n  --background-hover: #001c4e;\n  --color-hover: #001c4e;\n  --box-shadow: 0 0 0 0;\n  outline: 0;\n}\n\nion-item {\n  border: 1px solid lightgray;\n  border-radius: 50px;\n  --highlight-height: 0px;\n  /*--highlight-color-focused: var(--ion-color-primary, #001c4e);\n  --highlight-color-valid: var(--ion-color-success, #2dd36f);\n  --highlight-color-invalid: var(--ion-color-danger, #eb445a);*/\n}\n\nion-icon {\n  color: gray;\n}\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 15px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY2FkYXN0cm8vY2FkYXN0cm8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSwyQkFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLGNBQUE7RUFDQSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7RUFDQSwrQkFBQTtFQUNBLHdCQUFBO0VBQ0EsMkJBQUE7RUFDQSxzQkFBQTtFQUNBLHFCQUFBO0VBQ0EsVUFBQTtBQUNKOztBQUNBO0VBQ0ksMkJBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0E7OytEQUFBO0FBSUo7O0FBQ0E7RUFDSSxXQUFBO0FBRUo7O0FBQ0E7RUFDSSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBRUoiLCJmaWxlIjoic3JjL2FwcC9jYWRhc3Ryby9jYWRhc3Ryby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZCwgaW1nIHtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUlO1xyXG4gICAgYm9yZGVyOiAjMDAxYzRlIDAuNXB4IHNvbGlkO1xyXG59XHJcblxyXG5pb24tdG9vbGJhciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGU7XHJcbn1cclxuXHJcbmlvbi10aXRsZSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7IFxyXG4gICAgY29sb3I6ICMwMDFjNGU7XHJcbiAgICBmb250LXNpemU6IDEuNWVtO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbmlvbi1ncmlkIHtcclxuICAgIG1hcmdpbjogYXV0bzsgXHJcbiAgICBkaXNwbGF5OiBibG9jazsgXHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICBib3JkZXItbGVmdDogbm9uZTtcclxuICAgIGJvcmRlci1yaWdodDogbm9uZTtcclxufVxyXG5cclxuLmJvdGFvQ2FkYXN0cm8ge1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjYjFmZjQ5OyAgLy8jMDAxYzRlO1xyXG4gICAgLS1jb2xvcjogIzAwMWM0ZTsgLy93aGl0ZTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBoZWlnaHQ6IDQ4cHg7XHJcbiAgICB3aWR0aDogOTUlO1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgLS1ib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogIzAwMWM0ZTtcclxuICAgIC0tY29sb3ItYWN0aXZhdGVkOiB3aGl0ZTtcclxuICAgIC0tYmFja2dyb3VuZC1ob3ZlcjogIzAwMWM0ZTtcclxuICAgIC0tY29sb3ItaG92ZXI6ICMwMDFjNGU7XHJcbiAgICAtLWJveC1zaGFkb3c6IDAgMCAwIDA7IFxyXG4gICAgb3V0bGluZTogMDtcclxufVxyXG5pb24taXRlbSB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGdyYXk7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgLS1oaWdobGlnaHQtaGVpZ2h0OiAwcHg7XHJcbiAgICAvKi0taGlnaGxpZ2h0LWNvbG9yLWZvY3VzZWQ6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LCAjMDAxYzRlKTtcclxuICAgIC0taGlnaGxpZ2h0LWNvbG9yLXZhbGlkOiB2YXIoLS1pb24tY29sb3Itc3VjY2VzcywgIzJkZDM2Zik7XHJcbiAgICAtLWhpZ2hsaWdodC1jb2xvci1pbnZhbGlkOiB2YXIoLS1pb24tY29sb3ItZGFuZ2VyLCAjZWI0NDVhKTsqL1xyXG59XHJcblxyXG5pb24taWNvbiB7XHJcbiAgICBjb2xvcjogZ3JheTtcclxufSBcclxuXHJcbmgxIHtcclxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIG1hcmdpbjogMTVweCAwO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBjb2xvcjogIzEyM2I3ZDtcclxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/cadastro/cadastro.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/cadastro/cadastro.page.ts ***!
      \*******************************************/

    /*! exports provided: CadastroPage */

    /***/
    function srcAppCadastroCadastroPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CadastroPage", function () {
        return CadastroPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_services_user_registerService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/services/user/registerService */
      "./src/services/user/registerService.ts");
      /* harmony import */


      var _user_not_taken_validator_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./user-not-taken.validator.service */
      "./src/app/cadastro/user-not-taken.validator.service.ts");

      var CadastroPage = /*#__PURE__*/function () {
        function CadastroPage(router, navCtrl, toastCtrl, loadingCtrl, menuCtrl, formBuilder, signupService, userNotTakenValidatorService) {
          _classCallCheck(this, CadastroPage);

          this.router = router;
          this.navCtrl = navCtrl;
          this.toastCtrl = toastCtrl;
          this.loadingCtrl = loadingCtrl;
          this.menuCtrl = menuCtrl;
          this.formBuilder = formBuilder;
          this.signupService = signupService;
          this.userNotTakenValidatorService = userNotTakenValidatorService;
        }

        _createClass(CadastroPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.onRegisterForm = this.formBuilder.group({
              name: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(40)]],
              secondName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(40)]],
              phone: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(40)], this.userNotTakenValidatorService.checkUserNameTaken()],
              email: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email], this.userNotTakenValidatorService.checkEmailTaken()],
              cidade: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(40)]],
              password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(14)]]
            });
          }
        }, {
          key: "mostrarSenha",
          value: function mostrarSenha() {
            this.tipo = !this.tipo;
          }
        }, {
          key: "signup",
          value: function signup() {
            var _this = this;

            var newUser = this.onRegisterForm.getRawValue();
            this.nome = newUser.name;
            this.phone = newUser.phone;
            newUser.phone = newUser.phone.replace('-', '').replace(' ', '').replace('(', '').replace(')', '');
            this.signupService.signup(newUser).subscribe(function () {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                var toast;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        this.router.navigate(['/etapas/' + this.nome + '/' + newUser.phone + '']);
                        _context.next = 3;
                        return this.toastCtrl.create({
                          message: 'Usuário cadastrado com sucesso! Finalize as etapas do cadastro para entrar em análise!',
                          duration: 4000,
                          position: 'middle',
                          color: 'success'
                        });

                      case 3:
                        toast = _context.sent.present();

                      case 4:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));
            }, function (err) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                var toast;
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        console.log(err);
                        _context2.next = 3;
                        return this.toastCtrl.create({
                          message: 'Por favor confira se a sua internet está funcionando e tente cadastrar novamente! UaiLeva agradece!',
                          duration: 4000,
                          position: 'top',
                          color: 'danger'
                        });

                      case 3:
                        _context2.next = 5;
                        return _context2.sent.present();

                      case 5:
                        toast = _context2.sent;

                      case 6:
                      case "end":
                        return _context2.stop();
                    }
                  }
                }, _callee2, this);
              }));
            });
          }
        }]);

        return CadastroPage;
      }();

      CadastroPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
        }, {
          type: src_services_user_registerService__WEBPACK_IMPORTED_MODULE_5__["RegisterService"]
        }, {
          type: _user_not_taken_validator_service__WEBPACK_IMPORTED_MODULE_6__["UserNotTakenValidatorService"]
        }];
      };

      CadastroPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-cadastro',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./cadastro.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/cadastro/cadastro.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./cadastro.page.scss */
        "./src/app/cadastro/cadastro.page.scss"))["default"]]
      })], CadastroPage);
      /***/
    },

    /***/
    "./src/app/cadastro/user-not-taken.validator.service.ts":
    /*!**************************************************************!*\
      !*** ./src/app/cadastro/user-not-taken.validator.service.ts ***!
      \**************************************************************/

    /*! exports provided: UserNotTakenValidatorService */

    /***/
    function srcAppCadastroUserNotTakenValidatorServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserNotTakenValidatorService", function () {
        return UserNotTakenValidatorService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var src_services_user_registerService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/services/user/registerService */
      "./src/services/user/registerService.ts");

      var UserNotTakenValidatorService = /*#__PURE__*/function () {
        function UserNotTakenValidatorService(signUpService) {
          _classCallCheck(this, UserNotTakenValidatorService);

          this.signUpService = signUpService;
        }

        _createClass(UserNotTakenValidatorService, [{
          key: "checkUserNameTaken",
          value: function checkUserNameTaken() {
            var _this2 = this;

            return function (control) {
              return control.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["debounceTime"])(300)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(function (phone) {
                return _this2.signUpService.checkUserNameTaken(phone.replace('-', '').replace(' ', '').replace('(', '').replace(')', ''));
              })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (isTaken) {
                return isTaken ? {
                  userNameTaken: true
                } : null;
              })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["first"])());
            };
          }
        }, {
          key: "checkEmailTaken",
          value: function checkEmailTaken() {
            var _this3 = this;

            console.log('entrou no taken');
            return function (control) {
              return control.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["debounceTime"])(300)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(function (email) {
                return _this3.signUpService.checkEmailTaken(email);
              })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (isTaken) {
                return isTaken ? {
                  emailTaken: true
                } : null;
              })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["first"])());
            };
          }
        }]);

        return UserNotTakenValidatorService;
      }();

      UserNotTakenValidatorService.ctorParameters = function () {
        return [{
          type: src_services_user_registerService__WEBPACK_IMPORTED_MODULE_3__["RegisterService"]
        }];
      };

      UserNotTakenValidatorService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], UserNotTakenValidatorService);
      /***/
    },

    /***/
    "./src/services/user/registerService.ts":
    /*!**********************************************!*\
      !*** ./src/services/user/registerService.ts ***!
      \**********************************************/

    /*! exports provided: RegisterService */

    /***/
    function srcServicesUserRegisterServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RegisterService", function () {
        return RegisterService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var API_URL = "https://uaileva.com.br/api"; //const API_URL = "http://191.252.3.12:3000"; 

      var RegisterService = /*#__PURE__*/function () {
        function RegisterService(http) {
          _classCallCheck(this, RegisterService);

          this.http = http;
        }

        _createClass(RegisterService, [{
          key: "checkUserNameTaken",
          value: function checkUserNameTaken(phone) {
            return this.http.get(API_URL + '/motorista/exists/' + phone);
          }
        }, {
          key: "checkEmailTaken",
          value: function checkEmailTaken(email) {
            return this.http.get(API_URL + '/motorista/exists/email/' + email);
          }
        }, {
          key: "signup",
          value: function signup(newUser) {
            return this.http.post(API_URL + '/motorista/signup', newUser);
          }
        }, {
          key: "getAll",
          value: function getAll() {
            return this.http.get(API_URL + "/motorista");
          }
        }, {
          key: "getById",
          value: function getById(id) {
            return this.http.get(API_URL + "/motorista/" + id);
          }
        }, {
          key: "register",
          value: function register(user) {
            return this.http.post(API_URL + "/motorista/register", user);
          }
        }, {
          key: "update",
          value: function update(user) {
            return this.http.put(API_URL + "/motorista/" + user.id, user);
          }
        }, {
          key: "delete",
          value: function _delete(id) {
            return this.http["delete"](API_URL + "/motorista/" + id);
          }
        }, {
          key: "upload",
          value: function upload(description, allowComments, file) {
            {
              var formData = new FormData();
              formData.append('description', description);
              formData.append('allowComments', allowComments ? 'true' : 'false');
              formData.append('imageFile', file);
              return this.http.post(API_URL + '/photos/upload', formData);
            }
          }
        }, {
          key: "updateFoto",
          value: function updateFoto(description, allowComments, file) {
            {
              var formData = new FormData();
              formData.append('description', description);
              formData.append('allowComments', allowComments ? 'true' : 'false');
              formData.append('imageFile', file);
              return this.http.post(API_URL + '/photos/update', formData);
            }
          }
        }, {
          key: "acordo",
          value: function acordo(phone) {
            return this.http.get(API_URL + "/motorista/acordo/" + phone);
          }
        }, {
          key: "findFotoMotorista",
          value: function findFotoMotorista(user_id) {
            return this.http.get(API_URL + '/photos/motorista/' + user_id + '');
          }
        }, {
          key: "findMotorista",
          value: function findMotorista(phone) {
            return this.http.get(API_URL + '/motorista/taxa/' + phone + '');
          }
        }]);

        return RegisterService;
      }();

      RegisterService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      RegisterService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], RegisterService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=cadastro-cadastro-module-es5.js.map
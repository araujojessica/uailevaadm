(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["banco-banco-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/banco/banco.page.html":
    /*!*****************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/banco/banco.page.html ***!
      \*****************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppBancoBancoPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"login-home\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "./src/app/banco/banco-routing.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/banco/banco-routing.module.ts ***!
      \***********************************************/

    /*! exports provided: BancoPageRoutingModule */

    /***/
    function srcAppBancoBancoRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BancoPageRoutingModule", function () {
        return BancoPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _banco_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./banco.page */
      "./src/app/banco/banco.page.ts");

      var routes = [{
        path: '',
        component: _banco_page__WEBPACK_IMPORTED_MODULE_3__["BancoPage"]
      }];

      var BancoPageRoutingModule = function BancoPageRoutingModule() {
        _classCallCheck(this, BancoPageRoutingModule);
      };

      BancoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], BancoPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/banco/banco.module.ts":
    /*!***************************************!*\
      !*** ./src/app/banco/banco.module.ts ***!
      \***************************************/

    /*! exports provided: BancoPageModule */

    /***/
    function srcAppBancoBancoModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BancoPageModule", function () {
        return BancoPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _banco_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./banco-routing.module */
      "./src/app/banco/banco-routing.module.ts");
      /* harmony import */


      var _banco_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./banco.page */
      "./src/app/banco/banco.page.ts");

      var BancoPageModule = function BancoPageModule() {
        _classCallCheck(this, BancoPageModule);
      };

      BancoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _banco_routing_module__WEBPACK_IMPORTED_MODULE_5__["BancoPageRoutingModule"]],
        declarations: [_banco_page__WEBPACK_IMPORTED_MODULE_6__["BancoPage"]]
      })], BancoPageModule);
      /***/
    },

    /***/
    "./src/app/banco/banco.page.scss":
    /*!***************************************!*\
      !*** ./src/app/banco/banco.page.scss ***!
      \***************************************/

    /*! exports provided: default */

    /***/
    function srcAppBancoBancoPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-card, img {\n  border-radius: 5%;\n  border: #001c4e 0.5px solid;\n}\n\nion-toolbar {\n  --background: #001c4e;\n}\n\nion-title {\n  color: #fff;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYmFuY28vYmFuY28ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksaUJBQUE7RUFDQSwyQkFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7QUFDSiIsImZpbGUiOiJzcmMvYXBwL2JhbmNvL2JhbmNvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkLCBpbWcge1xyXG4gICAgYm9yZGVyLXJhZGl1czogNSU7XHJcbiAgICBib3JkZXI6ICMwMDFjNGUgMC41cHggc29saWQ7XHJcbn1cclxuXHJcbmlvbi10b29sYmFyIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzAwMWM0ZTtcclxufVxyXG5cclxuaW9uLXRpdGxlIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG59XHJcbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/banco/banco.page.ts":
    /*!*************************************!*\
      !*** ./src/app/banco/banco.page.ts ***!
      \*************************************/

    /*! exports provided: BancoPage */

    /***/
    function srcAppBancoBancoPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BancoPage", function () {
        return BancoPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var BancoPage = /*#__PURE__*/function () {
        function BancoPage() {
          _classCallCheck(this, BancoPage);
        }

        _createClass(BancoPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return BancoPage;
      }();

      BancoPage.ctorParameters = function () {
        return [];
      };

      BancoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-banco',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./banco.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/banco/banco.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./banco.page.scss */
        "./src/app/banco/banco.page.scss"))["default"]]
      })], BancoPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=banco-banco-module-es5.js.map
(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["cnh-cnh-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/cnh/cnh.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/cnh/cnh.page.html ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button text=\"\" defaultHref=\"etapas\" color=\"light\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <h1>Tire uma foto do(a) Carteira Nacional de Habilitação com EAR - CNH</h1>\r\n  <br>\r\n  <div>\r\n    <p class=\"termo\">\r\n      Lembre-se, é preciso que o documento: 1. Seja como na imagem abaixo. 2. Esteja aberto.\r\n      3. Tenha todos os campos visíveis. 4. Tenha a observação \"Exerce Atividade Remunerada\"\r\n      (EAR). Caso não tenha, consulte o Detran local e resolva no mesmo dia. Obs: A Permissão\r\n      para Dirigir não é válida\r\n    </p>\r\n  </div>\r\n\r\n  <img src=\"/assets/img/exemplo-cnh.png\"><br>\r\n\r\n  <br><br>\r\n  <input type=\"file\" (change)= \"file = $event.target.files[0]\" accept=\"image/*\"><br>\r\n  <ion-button class=\"botao\" (click)= \"salvarFoto();\">Enviar foto</ion-button>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/cnh/cnh-routing.module.ts":
/*!*******************************************!*\
  !*** ./src/app/cnh/cnh-routing.module.ts ***!
  \*******************************************/
/*! exports provided: CnhPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CnhPageRoutingModule", function() { return CnhPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _cnh_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./cnh.page */ "./src/app/cnh/cnh.page.ts");




const routes = [
    {
        path: '',
        component: _cnh_page__WEBPACK_IMPORTED_MODULE_3__["CnhPage"]
    }
];
let CnhPageRoutingModule = class CnhPageRoutingModule {
};
CnhPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], CnhPageRoutingModule);



/***/ }),

/***/ "./src/app/cnh/cnh.module.ts":
/*!***********************************!*\
  !*** ./src/app/cnh/cnh.module.ts ***!
  \***********************************/
/*! exports provided: CnhPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CnhPageModule", function() { return CnhPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _cnh_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./cnh-routing.module */ "./src/app/cnh/cnh-routing.module.ts");
/* harmony import */ var _cnh_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./cnh.page */ "./src/app/cnh/cnh.page.ts");







let CnhPageModule = class CnhPageModule {
};
CnhPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _cnh_routing_module__WEBPACK_IMPORTED_MODULE_5__["CnhPageRoutingModule"]
        ],
        declarations: [_cnh_page__WEBPACK_IMPORTED_MODULE_6__["CnhPage"]]
    })
], CnhPageModule);



/***/ }),

/***/ "./src/app/cnh/cnh.page.scss":
/*!***********************************!*\
  !*** ./src/app/cnh/cnh.page.scss ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-content {\n  --background: var(--ion-color-light);\n  --background: url(\"/assets/img/bg-uai-leva2.png\") #eaeaea no-repeat bottom 75px left 45px;\n}\n\nion-toolbar {\n  --background: #001c4e !important;\n}\n\nion-header {\n  --background: #001c4e !important;\n}\n\nion-item {\n  --ion-item-background: transparent;\n  --border-style: var(--border-style);\n  --border: 0 none;\n  --box-shadow: 0 0 0 0;\n  --outline: 0;\n}\n\nion-icon {\n  color: gray;\n}\n\n/*input[type=\"file\"]{\n    display: none;\n}\n\nlabel {\n    background: #b1ff49;  //#001c4e;\n    color: #001c4e; //white;\n    font-weight: bold;\n    display: block;\n    height: 48px;\n    width: 95%;\n    margin: auto;\n    border-radius: 50px;\n    text-align: center;\n    padding: 16px;\n    --box-shadow: 0 0 0 0; \n    outline: 0;\n    text-transform: uppercase;\n    font-size: .9em;\n    cursor: pointer;\n}*/\n\nimg {\n  height: 300px;\n  width: 300px;\n  display: block;\n  margin: auto;\n}\n\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  margin: 15px 0;\n  font-weight: bold;\n  color: #123b7d;\n}\n\n.declaracao {\n  font-size: 1.1rem;\n  padding: 10px;\n  color: #123b7d;\n  text-align: justify;\n}\n\n.termo {\n  padding: 10px;\n  padding-top: 0px;\n  color: #123b7d;\n  margin-top: -20px;\n  text-align: justify;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY25oL2NuaC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQ0FBQTtFQUNBLHlGQUFBO0FBQ0o7O0FBRUE7RUFDSSxnQ0FBQTtBQUNKOztBQUVBO0VBQ0ksZ0NBQUE7QUFDSjs7QUFFQTtFQUNJLGtDQUFBO0VBQ0EsbUNBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtBQUNKOztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQUFBOztBQXNCQTtFQUNJLGFBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7QUFBSjs7QUFHQTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFFQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBREo7O0FBSUE7RUFDSSxpQkFBQTtFQUVBLGFBQUE7RUFFQSxjQUFBO0VBQ0EsbUJBQUE7QUFISjs7QUFNQTtFQUNJLGFBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBSEoiLCJmaWxlIjoic3JjL2FwcC9jbmgvY25oLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcclxuICAgIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxuICAgIC0tYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWcvYmctdWFpLWxldmEyLnBuZ1wiKSAjZWFlYWVhIG5vLXJlcGVhdCBib3R0b20gNzVweCBsZWZ0IDQ1cHg7XHJcbn1cclxuXHJcbmlvbi10b29sYmFyIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzAwMWM0ZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5pb24taGVhZGVyIHtcclxuICAgIC0tYmFja2dyb3VuZDogIzAwMWM0ZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5pb24taXRlbSB7XHJcbiAgICAtLWlvbi1pdGVtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgLS1ib3JkZXItc3R5bGU6IHZhcigtLWJvcmRlci1zdHlsZSk7XHJcbiAgICAtLWJvcmRlcjogMCBub25lOyBcclxuICAgIC0tYm94LXNoYWRvdzogMCAwIDAgMDsgXHJcbiAgICAtLW91dGxpbmU6IDA7XHJcbn1cclxuXHJcbmlvbi1pY29uIHtcclxuICAgIGNvbG9yOiBncmF5O1xyXG59XHJcblxyXG4vKmlucHV0W3R5cGU9XCJmaWxlXCJde1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxubGFiZWwge1xyXG4gICAgYmFja2dyb3VuZDogI2IxZmY0OTsgIC8vIzAwMWM0ZTtcclxuICAgIGNvbG9yOiAjMDAxYzRlOyAvL3doaXRlO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIGhlaWdodDogNDhweDtcclxuICAgIHdpZHRoOiA5NSU7XHJcbiAgICBtYXJnaW46IGF1dG87XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgcGFkZGluZzogMTZweDtcclxuICAgIC0tYm94LXNoYWRvdzogMCAwIDAgMDsgXHJcbiAgICBvdXRsaW5lOiAwO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIGZvbnQtc2l6ZTogLjllbTtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufSovXHJcblxyXG5pbWcge1xyXG4gICAgaGVpZ2h0OiAzMDBweDtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG59XHJcblxyXG5oMSB7XHJcbiAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIC8vdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIG1hcmdpbjogMTVweCAwO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBjb2xvcjogIzEyM2I3ZDtcclxufVxyXG5cclxuLmRlY2xhcmFjYW8ge1xyXG4gICAgZm9udC1zaXplOiAxLjFyZW07XHJcbiAgICAvL2ZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIC8vcGFkZGluZy1ib3R0b206IDBweDtcclxuICAgIGNvbG9yOiAjMTIzYjdkO1xyXG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcclxufVxyXG5cclxuLnRlcm1vIHtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbiAgICBtYXJnaW4tdG9wOiAtMjBweDtcclxuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/cnh/cnh.page.ts":
/*!*********************************!*\
  !*** ./src/app/cnh/cnh.page.ts ***!
  \*********************************/
/*! exports provided: CnhPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CnhPage", function() { return CnhPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let CnhPage = class CnhPage {
    constructor() { }
    ngOnInit() {
    }
};
CnhPage.ctorParameters = () => [];
CnhPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-cnh',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./cnh.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/cnh/cnh.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./cnh.page.scss */ "./src/app/cnh/cnh.page.scss")).default]
    })
], CnhPage);



/***/ })

}]);
//# sourceMappingURL=cnh-cnh-module-es2015.js.map
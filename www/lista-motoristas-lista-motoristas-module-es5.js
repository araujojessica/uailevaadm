(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["lista-motoristas-lista-motoristas-module"], {
    /***/
    "./node_modules/jwt-decode/build/jwt-decode.esm.js":
    /*!*********************************************************!*\
      !*** ./node_modules/jwt-decode/build/jwt-decode.esm.js ***!
      \*********************************************************/

    /*! exports provided: default, InvalidTokenError */

    /***/
    function node_modulesJwtDecodeBuildJwtDecodeEsmJs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "InvalidTokenError", function () {
        return n;
      });

      function e(e) {
        this.message = e;
      }

      e.prototype = new Error(), e.prototype.name = "InvalidCharacterError";

      var r = "undefined" != typeof window && window.atob && window.atob.bind(window) || function (r) {
        var t = String(r).replace(/=+$/, "");
        if (t.length % 4 == 1) throw new e("'atob' failed: The string to be decoded is not correctly encoded.");

        for (var n, o, a = 0, i = 0, c = ""; o = t.charAt(i++); ~o && (n = a % 4 ? 64 * n + o : o, a++ % 4) ? c += String.fromCharCode(255 & n >> (-2 * a & 6)) : 0) {
          o = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(o);
        }

        return c;
      };

      function t(e) {
        var t = e.replace(/-/g, "+").replace(/_/g, "/");

        switch (t.length % 4) {
          case 0:
            break;

          case 2:
            t += "==";
            break;

          case 3:
            t += "=";
            break;

          default:
            throw "Illegal base64url string!";
        }

        try {
          return function (e) {
            return decodeURIComponent(r(e).replace(/(.)/g, function (e, r) {
              var t = r.charCodeAt(0).toString(16).toUpperCase();
              return t.length < 2 && (t = "0" + t), "%" + t;
            }));
          }(t);
        } catch (e) {
          return r(t);
        }
      }

      function n(e) {
        this.message = e;
      }

      function o(e, r) {
        if ("string" != typeof e) throw new n("Invalid token specified");
        var o = !0 === (r = r || {}).header ? 0 : 1;

        try {
          return JSON.parse(t(e.split(".")[o]));
        } catch (e) {
          throw new n("Invalid token specified: " + e.message);
        }
      }

      n.prototype = new Error(), n.prototype.name = "InvalidTokenError";
      /* harmony default export */

      __webpack_exports__["default"] = o; //# sourceMappingURL=jwt-decode.esm.js.map

      /***/
    },

    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/lista-motoristas/lista-motoristas.page.html":
    /*!***************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/lista-motoristas/lista-motoristas.page.html ***!
      \***************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppListaMotoristasListaMotoristasPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"login-home\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <br>\r\n  <ion-grid>\r\n    <ion-list>\r\n      <ion-item-divider mode=\"md\"><ion-label>Motoristas não liberados</ion-label></ion-item-divider>\r\n      <ion-item *ngFor= \"let m of m\">\r\n        <ion-label>{{m.full_name}}</ion-label>\r\n        <ion-button class=\"btn-bloquear\" slot=\"end\" (click) = \"liberacao(m.id)\">LIBERAR</ion-button>\r\n      </ion-item>\r\n    </ion-list>\r\n  </ion-grid>\r\n  <br>\r\n</ion-content>\r\n";
      /***/
    },

    /***/
    "./src/app/lista-motoristas/lista-motoristas-routing.module.ts":
    /*!*********************************************************************!*\
      !*** ./src/app/lista-motoristas/lista-motoristas-routing.module.ts ***!
      \*********************************************************************/

    /*! exports provided: ListaMotoristasPageRoutingModule */

    /***/
    function srcAppListaMotoristasListaMotoristasRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ListaMotoristasPageRoutingModule", function () {
        return ListaMotoristasPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _lista_motoristas_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./lista-motoristas.page */
      "./src/app/lista-motoristas/lista-motoristas.page.ts");

      var routes = [{
        path: '',
        component: _lista_motoristas_page__WEBPACK_IMPORTED_MODULE_3__["ListaMotoristasPage"]
      }];

      var ListaMotoristasPageRoutingModule = function ListaMotoristasPageRoutingModule() {
        _classCallCheck(this, ListaMotoristasPageRoutingModule);
      };

      ListaMotoristasPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ListaMotoristasPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/lista-motoristas/lista-motoristas.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/lista-motoristas/lista-motoristas.module.ts ***!
      \*************************************************************/

    /*! exports provided: ListaMotoristasPageModule */

    /***/
    function srcAppListaMotoristasListaMotoristasModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ListaMotoristasPageModule", function () {
        return ListaMotoristasPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _lista_motoristas_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./lista-motoristas-routing.module */
      "./src/app/lista-motoristas/lista-motoristas-routing.module.ts");
      /* harmony import */


      var _lista_motoristas_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./lista-motoristas.page */
      "./src/app/lista-motoristas/lista-motoristas.page.ts");

      var ListaMotoristasPageModule = function ListaMotoristasPageModule() {
        _classCallCheck(this, ListaMotoristasPageModule);
      };

      ListaMotoristasPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _lista_motoristas_routing_module__WEBPACK_IMPORTED_MODULE_5__["ListaMotoristasPageRoutingModule"]],
        declarations: [_lista_motoristas_page__WEBPACK_IMPORTED_MODULE_6__["ListaMotoristasPage"]]
      })], ListaMotoristasPageModule);
      /***/
    },

    /***/
    "./src/app/lista-motoristas/lista-motoristas.page.scss":
    /*!*************************************************************!*\
      !*** ./src/app/lista-motoristas/lista-motoristas.page.scss ***!
      \*************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppListaMotoristasListaMotoristasPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-toolbar {\n  --background: #001c4e;\n}\n\nion-grid {\n  margin: auto;\n  display: block;\n  width: 95%;\n  border: 1px solid;\n  border-left: none;\n  border-right: none;\n}\n\nion-button {\n  --background: #b1ff49;\n  color: #001c4e;\n  font-weight: bold;\n  border-radius: 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbGlzdGEtbW90b3Jpc3Rhcy9saXN0YS1tb3RvcmlzdGFzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHFCQUFBO0FBQ0o7O0FBRUE7RUFDSSxZQUFBO0VBQ0EsY0FBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUFDSiIsImZpbGUiOiJzcmMvYXBwL2xpc3RhLW1vdG9yaXN0YXMvbGlzdGEtbW90b3Jpc3Rhcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tdG9vbGJhciB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICMwMDFjNGU7XHJcbn1cclxuXHJcbmlvbi1ncmlkIHtcclxuICAgIG1hcmdpbjogYXV0bzsgXHJcbiAgICBkaXNwbGF5OiBibG9jazsgXHJcbiAgICB3aWR0aDogOTUlO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQ7XHJcbiAgICBib3JkZXItbGVmdDogbm9uZTtcclxuICAgIGJvcmRlci1yaWdodDogbm9uZTtcclxufVxyXG5cclxuaW9uLWJ1dHRvbiB7XHJcbiAgICAtLWJhY2tncm91bmQ6ICNiMWZmNDk7XHJcbiAgICBjb2xvcjogIzAwMWM0ZTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkOyBcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/lista-motoristas/lista-motoristas.page.ts":
    /*!***********************************************************!*\
      !*** ./src/app/lista-motoristas/lista-motoristas.page.ts ***!
      \***********************************************************/

    /*! exports provided: ListaMotoristasPage */

    /***/
    function srcAppListaMotoristasListaMotoristasPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ListaMotoristasPage", function () {
        return ListaMotoristasPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var src_core_user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/core/user/user.service */
      "./src/core/user/user.service.ts");
      /* harmony import */


      var src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/servico/motorista_liberacao */
      "./src/servico/motorista_liberacao.ts");

      var ListaMotoristasPage = /*#__PURE__*/function () {
        function ListaMotoristasPage(router, motoristaService, mortoristaLiberacaoService) {
          var _this = this;

          _classCallCheck(this, ListaMotoristasPage);

          this.router = router;
          this.motoristaService = motoristaService;
          this.mortoristaLiberacaoService = mortoristaLiberacaoService;
          this.user$ = motoristaService.getUser();
          this.user$.subscribe(function (usuario) {
            _this.usuarioLogado = usuario;
            _this.user_id = _this.usuarioLogado.id;

            var a = _this.usuarioLogado.full_name.split(' ');

            if (_this.usuarioLogado.apelido == null) {
              _this.apelido = '';
            } else {
              _this.apelido = '(' + _this.usuarioLogado.apelido + ')';
            }

            _this.nome = a[0] + ' ' + _this.apelido;
          });
          this.mortoristaLiberacaoService.liberar().subscribe(function (motoristaLiberado) {
            _this.m = motoristaLiberado;
          });
        }

        _createClass(ListaMotoristasPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "liberacao",
          value: function liberacao(user_id) {
            this.router.navigate(['/liberacao/' + user_id + '']);
          }
        }]);

        return ListaMotoristasPage;
      }();

      ListaMotoristasPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: src_core_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]
        }, {
          type: src_servico_motorista_liberacao__WEBPACK_IMPORTED_MODULE_4__["MotoristaLiberacaoService"]
        }];
      };

      ListaMotoristasPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-lista-motoristas',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./lista-motoristas.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/lista-motoristas/lista-motoristas.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./lista-motoristas.page.scss */
        "./src/app/lista-motoristas/lista-motoristas.page.scss"))["default"]]
      })], ListaMotoristasPage);
      /***/
    },

    /***/
    "./src/core/token/token.service.ts":
    /*!*****************************************!*\
      !*** ./src/core/token/token.service.ts ***!
      \*****************************************/

    /*! exports provided: TokenService */

    /***/
    function srcCoreTokenTokenServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TokenService", function () {
        return TokenService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var KEY = 'authToken';

      var TokenService = /*#__PURE__*/function () {
        function TokenService() {
          _classCallCheck(this, TokenService);
        }

        _createClass(TokenService, [{
          key: "hasToken",
          value: function hasToken() {
            return !!this.getToken();
          }
        }, {
          key: "setToken",
          value: function setToken(token) {
            window.localStorage.setItem(KEY, token);
          }
        }, {
          key: "getToken",
          value: function getToken() {
            return window.localStorage.getItem(KEY);
          }
        }, {
          key: "removeToken",
          value: function removeToken() {
            window.localStorage.removeItem(KEY);
          }
        }]);

        return TokenService;
      }();

      TokenService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], TokenService);
      /***/
    },

    /***/
    "./src/core/user/user.service.ts":
    /*!***************************************!*\
      !*** ./src/core/user/user.service.ts ***!
      \***************************************/

    /*! exports provided: UserService */

    /***/
    function srcCoreUserUserServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserService", function () {
        return UserService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _token_token_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../token/token.service */
      "./src/core/token/token.service.ts");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var jwt_decode__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! jwt-decode */
      "./node_modules/jwt-decode/build/jwt-decode.esm.js"); //import * as jwt_decode from 'jwt-decode';


      var UserService = /*#__PURE__*/function () {
        function UserService(tokenService) {
          _classCallCheck(this, UserService);

          this.tokenService = tokenService;
          this.userSubject = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](null);
          this.tokenService.hasToken() && this.decodeAndNotify();
        }

        _createClass(UserService, [{
          key: "setToken",
          value: function setToken(token) {
            this.tokenService.setToken(token);
            this.decodeAndNotify();
          }
        }, {
          key: "getUser",
          value: function getUser() {
            return this.userSubject.asObservable();
          }
        }, {
          key: "decodeAndNotify",
          value: function decodeAndNotify() {
            var token = this.tokenService.getToken();
            var user = Object(jwt_decode__WEBPACK_IMPORTED_MODULE_4__["default"])(token);
            this.fullName = user.full_name;
            this.userSubject.next(user);
          }
        }, {
          key: "logout",
          value: function logout() {
            this.tokenService.removeToken();
            this.userSubject.next(null);
          }
        }, {
          key: "isLogged",
          value: function isLogged() {
            return this.tokenService.hasToken();
          }
        }, {
          key: "getUserName",
          value: function getUserName() {
            return this.fullName;
          }
        }]);

        return UserService;
      }();

      UserService.ctorParameters = function () {
        return [{
          type: _token_token_service__WEBPACK_IMPORTED_MODULE_2__["TokenService"]
        }];
      };

      UserService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], UserService);
      /***/
    },

    /***/
    "./src/servico/motorista_liberacao.ts":
    /*!********************************************!*\
      !*** ./src/servico/motorista_liberacao.ts ***!
      \********************************************/

    /*! exports provided: MotoristaLiberacaoService */

    /***/
    function srcServicoMotorista_liberacaoTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MotoristaLiberacaoService", function () {
        return MotoristaLiberacaoService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

      var API_URL = "https://uaileva.com.br/api";

      var MotoristaLiberacaoService = /*#__PURE__*/function () {
        function MotoristaLiberacaoService(http) {
          _classCallCheck(this, MotoristaLiberacaoService);

          this.http = http;
        }

        _createClass(MotoristaLiberacaoService, [{
          key: "liberar",
          value: function liberar() {
            return this.http.get(API_URL + "/motorista/liberacao");
          }
        }, {
          key: "motoristaUnico",
          value: function motoristaUnico(motorista_id) {
            return this.http.get(API_URL + "/motorista/unico/" + motorista_id);
          }
        }, {
          key: "fotoMotorista",
          value: function fotoMotorista(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/" + mot_phone);
          }
        }, {
          key: "fotoCrlv",
          value: function fotoCrlv(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/crlv/" + mot_phone);
          }
        }, {
          key: "fotoCnh",
          value: function fotoCnh(mot_phone) {
            return this.http.get(API_URL + "/photos/motorista/cnh/" + mot_phone);
          }
        }, {
          key: "carro",
          value: function carro(id_motorista, placa, modelo) {
            return this.http.get(API_URL + "/carro/signup/" + id_motorista + "/" + placa + "/" + modelo + "");
          }
        }, {
          key: "updateNome",
          value: function updateNome(user_id, nome, secondName) {
            return this.http.post("".concat(API_URL, "/motorista/updateNome"), {
              user_id: user_id,
              nome: nome,
              secondName: secondName
            });
          }
        }, {
          key: "liberarMotorista",
          value: function liberarMotorista(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/LIBERADO");
          }
        }, {
          key: "motoristaLiberado",
          value: function motoristaLiberado() {
            return this.http.get(API_URL + "/motoristaLiberado");
          }
        }, {
          key: "bloqueio",
          value: function bloqueio(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/BLOQUEADO");
          }
        }, {
          key: "bloqueioPassageiro",
          value: function bloqueioPassageiro(phone) {
            return this.http.get(API_URL + "/user/bloqueio/" + phone + "/BLOQUEADO");
          }
        }, {
          key: "corridaCM",
          value: function corridaCM(data) {
            console.log(data);
            return this.http.get(API_URL + "/corrida/diariacm/" + data + "");
          }
        }, {
          key: "corridaCP",
          value: function corridaCP(data) {
            return this.http.get(API_URL + "/corrida/diariacp/" + data + "");
          }
        }, {
          key: "corridaF",
          value: function corridaF(data) {
            return this.http.get(API_URL + "/corrida/diariaf/" + data + "");
          }
        }, {
          key: "corridaTotal",
          value: function corridaTotal() {
            return this.http.get(API_URL + "/totalCorridas");
          }
        }, {
          key: "motoristaLiberadoUnico",
          value: function motoristaLiberadoUnico(motorista_id) {
            return this.http.get(API_URL + "/motorista/liberadoUnico/" + motorista_id);
          }
        }, {
          key: "arrecadacao",
          value: function arrecadacao(motorista_id) {
            return this.http.get(API_URL + "/totalarrecadado/" + motorista_id);
          }
        }, {
          key: "desbloqueio",
          value: function desbloqueio(phone, email) {
            return this.http.get(API_URL + "/motorista/status/" + phone + "/" + email + "/LIBERADO");
          }
        }, {
          key: "motbloqueado",
          value: function motbloqueado() {
            return this.http.get(API_URL + "/motorista/bloqueado");
          }
        }]);

        return MotoristaLiberacaoService;
      }();

      MotoristaLiberacaoService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      MotoristaLiberacaoService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], MotoristaLiberacaoService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=lista-motoristas-lista-motoristas-module-es5.js.map